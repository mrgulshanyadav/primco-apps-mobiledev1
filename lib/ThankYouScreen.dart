import 'package:flutter/material.dart';
import 'package:primcoca/SalesRepDashboardScreen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class ThankYouScreen extends StatefulWidget{
  final String pdfLink;
  final String qrCodeLink;

  ThankYouScreen({ Key key, this.pdfLink, this.qrCodeLink }): super(key: key);

  @override
  State<StatefulWidget> createState() => new _ThankYouScreenState( pdfLink, qrCodeLink );

}

class _ThankYouScreenState extends State<ThankYouScreen>{
  final String pdfLink;
  final String qrCodeLink;

  _ThankYouScreenState(
    this.pdfLink,
    this.qrCodeLink
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: globals.secondaryColor,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              Text(
                'Thank you for being with Primco',
                style: globals.h1White,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 30,
              ),
              Image.network(qrCodeLink),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    child: Text('Download PDF'),
                    color: Colors.white,
                    textColor: globals.secondaryColor,
                    onPressed: () async{
                      if ( await canLaunch(pdfLink) ) {
                        await launch(pdfLink);
                      }
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  RaisedButton(
                    child: Text('Back to Home'),
                    color: Colors.white,
                    textColor: globals.secondaryColor,
                    onPressed: () {
                      Navigator.pushReplacement(context, 
                        MaterialPageRoute( builder: (context) => SalesRepDashboardScreen() )
                      );
                    },
                  ),

                ],
              )
              
              
            ],
          )  
        )
        
      ),
    );
  }
}