import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:expandable/expandable.dart';

import 'package:http/http.dart' as http;
import 'package:primcoca/GlobalVariables.dart' as globals;

class Customers {
  final String customerId;
  final String id;
  final String title;
  final String body;
  final String name;
  final String address;
  final String city;
  final String province;
  final String postalCode;
  final String sin;
  final String autocompleteterm;
  
  Customers({
    this.id, 
    this.customerId, 
    this.title, 
    this.body, 
    this.name,
    this.address,
    this.city,
    this.postalCode,
    this.province,
    this.sin,
    this.autocompleteterm,
    
  });

  factory Customers.fromJson(Map<String, dynamic> parsedJson) {
    return Customers(
      id: parsedJson['id'].toString(),
      customerId: parsedJson['customerid'].toString(),
      name: parsedJson['name'].toString(),
      address: parsedJson['address'].toString(),
      city: parsedJson['city'].toString(),
      province: parsedJson['province'].toString(),
      postalCode: parsedJson['postal_code'].toString(),
      sin: parsedJson['sin'].toString(),
      autocompleteterm: parsedJson['name'].toString(),
        
    );
  }
}

class CustomerViewModel {
  static List<Customers> players;

  static Future loadPlayers() async {
    
    try {
      players = new List<Customers>();
      //String jsonString = await rootBundle.loadString('assets/players.json');
      // final response = await http.get(globals.baseURL+'customers/index.json');
      await http.post( globals.baseURL+'customers/index.json', headers: {
        HttpHeaders.contentTypeHeader: 'application/json'
      } ).then((response){
        print(response.body);
        if (response.statusCode == 200) {
          Map parsedJson = json.decode(response.body);
          var categoryJson = parsedJson['customers'] as List;
          for (int i = 0; i < categoryJson.length; i++) {
            players.add(new Customers.fromJson(categoryJson[i]));
          }
        } else {
          // If that call was not successful, throw an error.
          throw Exception('Failed to load customer.');
        }
      });

      

      
    } catch (e) {
      
      print(e);
      
    }
  }
}

class Salesmen {
  final String id;
  final String name;
  final String salesRep;
  final String sdRep;
  final String repEmail;
  final String spec;
  final String autocompleteterm; 
  
  Salesmen({
    this.id, 
    this.name, 
    this.salesRep, 
    this.sdRep, 
    this.repEmail,
    this.spec,
    this.autocompleteterm
  });

  factory Salesmen.fromJson(Map<String, dynamic> parsedJson) {
    return Salesmen(
      id: parsedJson['id'].toString(),
      name: parsedJson['name'].toString(),
      salesRep: parsedJson['sales_rep'].toString(),
      sdRep: parsedJson['sd_rep'].toString(),
      repEmail: parsedJson['rep_email'].toString(),
      spec: parsedJson['spec'].toString(),
      autocompleteterm: parsedJson['name'].toString(),
        
    );
  }
}

class SalesmenViewModel {
  static List<Salesmen> salesmen;

  static Future loadPlayers() async {
    
    try {
      salesmen = new List<Salesmen>();
      //String jsonString = await rootBundle.loadString('assets/players.json');
      // final response = await http.get( globals.baseURL+'salesmen/index.json');
      await http.post( globals.baseURL+'salesmen/index.json', headers: {
        HttpHeaders.contentTypeHeader: 'application/json'
      }).then((response){
        if (response.statusCode == 200) {
          Map parsedJson = json.decode(response.body);
          var categoryJson = parsedJson['salesmen'] as List;
          
          for (int i = 0; i < categoryJson.length; i++) {
            salesmen.add(new Salesmen.fromJson(categoryJson[i]));
          }
        } else {
          // If that call was not successful, throw an error.
          throw Exception('Failed to load sales rep.');
        }
      });

      

      
    } catch (e) {
      
      print(e);
      
    }
  }
}

class ProductItem {
  // final String id;
  final String productName;
  final String autocompleteterm; 
  
  ProductItem({
    // this.id, 
    this.productName, 
    this.autocompleteterm
  });

  factory ProductItem.fromJson(Map<String, dynamic> parsedJson) {
    return ProductItem(
      // id: parsedJson['id'].toString(),
      productName: parsedJson['product_name'].toString(),
      autocompleteterm: parsedJson['product_name'].toString(),
        
    );
  }
}

class ProductItemViewModel {
  static List<ProductItem> product;

  static Future loadProduct() async {
    
    try {
      product = new List<ProductItem>();
      
      var headers = {
        HttpHeaders.contentTypeHeader: 'application/json'
      };
      
      await http.post( globals.baseURL+'products/getproductname.json', headers: headers ).then((response){
        
        if (response.statusCode == 200) {
          
          Map parsedJson = json.decode(response.body);
          var categoryJson = parsedJson['json_records'] as List;
          
          for (int i = 0; i < categoryJson.length; i++) {
            product.add(new ProductItem.fromJson(categoryJson[i]));
          }
        } else {
          // If that call was not successful, throw an error.
          
          throw Exception('Failed to load products');
        }
      });

    } catch (e) {
      
      print(e);
      
    }
  }
}

class ItemName {
  // final String id;
  final String itemName;
  final String autocompleteterm; 
  
  ItemName({
    // this.id, 
    this.itemName, 
    this.autocompleteterm
  });

  factory ItemName.fromJson(Map<String, dynamic> parsedJson) {
    return ItemName(
      // id: parsedJson['id'].toString(),
      itemName: parsedJson['item_name'].toString(),
      autocompleteterm: parsedJson['item_name'].toString(),
        
    );
  }
}

class ItemNameViewModel {
  static List<ItemName> items;

  static Future loadItemName( productName ) async {
    
    try {
      items = new List<ItemName>();
      //String jsonString = await rootBundle.loadString('assets/players.json');
      // final response = await http.get( globals.baseURL+'products/getitemname'+productName);
      
      await http.post( globals.baseURL+'products/getitemname.json', 
        body: { 
          'product_name': productName 
        }, 
        headers: {
          HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
        }
      ).then((response){
        
        if (response.statusCode == 200) {
          Map parsedJson = json.decode(response.body);
          var categoryJson = parsedJson['json_records'] as List;

          // items.add( new ItemName( autocompleteterm: '', itemName: '' ) );
          for (int i = 0; i < categoryJson.length; i++) {
            items.add(new ItemName.fromJson(categoryJson[i]));
          }
        } else {
          // If that call was not successful, throw an error.
          throw Exception('Failed to load items.');
        }
      });
      
    } catch (e) {
      print(e);
      
    }
  }
}

class ItemNumber {
  // final String id;
  final String id;
  final String productName;
  final String itemName;
  final String itemNumber;
  final String itemType;
  final String colorName;
  final String singleFranklin;
  final String bulkFranklin;
  final String perItem;
  final String singleItem;
  final String bulkItem;
  final String franklinType;
  final String bulkCount;
  final String autocompleteterm; 
  
  ItemNumber({
    // this.id, 
    this.id,
    this.productName,
    this.itemName,
    this.itemNumber,
    this.itemType,
    this.colorName,
    this.singleFranklin,
    this.bulkFranklin,
    this.perItem,
    this.singleItem,
    this.bulkItem,
    this.franklinType,
    this.bulkCount,
    this.autocompleteterm
  });

  factory ItemNumber.fromJson(Map<String, dynamic> parsedJson) {
    return ItemNumber(
      // id: parsedJson['id'].toString(),
      id: parsedJson['id'].toString(),
      productName: parsedJson['product_name'].toString(),
      itemName: parsedJson['item_name'].toString(),
      itemNumber: parsedJson['item_number'].toString(),
      itemType: parsedJson['item_type'].toString(),
      colorName: parsedJson['color_name'].toString(),
      singleFranklin: parsedJson['single_franklin'].toString(),
      bulkFranklin: parsedJson['bulk_franklin'].toString(),
      perItem: parsedJson['per_item'].toString(),
      singleItem: parsedJson['single_item'].toString(),
      bulkItem: parsedJson['bulk_item'].toString(),
      franklinType : parsedJson['franklin_type'].toString(),
      bulkCount: parsedJson['bulk_count'].toString(),
      autocompleteterm: parsedJson['item_number'].toString(),
        
    );
  }

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'productName': productName,
      'itemName': itemName,
      'itemNumber': itemNumber,
      'itemType': itemType,
      'colorName': colorName,
      'singleFranklin': singleFranklin,
      'bulkFranklin': bulkFranklin,
      'perItem': perItem,
      'singleItem': singleItem,
      'bulkItem': bulkItem,
      'franklinType' : franklinType,
      'bulkCount':bulkCount,
      'autocompleteterm': autocompleteterm,
    };
}

class ItemNumberViewModel {
  static List<ItemNumber> itemNumber;

  static Future loadItemNumber( productName, itemName ) async {
    
    try {
      itemNumber = new List<ItemNumber>();
      
      await http.post( globals.baseURL+'products/getitemnumber.json', 
        body: {
          'product_name': productName,
          'item_name': itemName
        },
        headers: {
          HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
        }
      ).then((response){
        
        print( 'Status code: '+ response.statusCode.toString() );

        if (response.statusCode == 200) {
          Map parsedJson = json.decode(response.body);
          var categoryJson = parsedJson['json_records'] as List;
          // itemNumber.add(
          //   new ItemNumber(
          //     autocompleteterm: '',
          //     bulkCount: '',
          //     bulkFranklin: '',
          //     bulkItem: '',
          //     colorName: '',
          //     franklinType: '',
          //     id: '',
          //     itemName: '',
          //     itemNumber: '',
          //     itemType: '',
          //     perItem: '',
          //     productName: '',
          //     singleFranklin: '',
          //     singleItem: ''
          //   )
          // );
          for (int i = 0; i < categoryJson.length; i++) {
            itemNumber.add(new ItemNumber.fromJson(categoryJson[i]));
          }
          
        } else {
          // If that call was not successful, throw an error.
          throw Exception('Failed to load item number.');
        }
      });
    } catch (e) {
      
      print(e);
      
    }
  }
}


class CANCForm1Data{
  String companyLegalName, 
  companyTradeName,
  companyStreetAddress,
  companyPhone,
  companyFax,
  companyCity,
  companyProvince,
  companyPostalCode,
  companyEmailAddress;

  String ownerName,
  ownerHomeAddress,
  ownerHomePhone,
  ownerCellPhone;

  String managerName,
  managerPhone,
  financialInstitutionIncludingFinancialAddress,
  bankAccountInformation,
  managerCity,
  managerProvince,
  managerPosalCode,
  managerPhone2,
  managerGST,
  provincialSalesTax,
  amountOfCreditRequired,
  expectedSalesVolumePerYear,
  ficalYearEnd, 
  isPartOfBuyGroup,
  buyGroupName;

  CANCForm1Data(

    this.companyLegalName,
    this.companyTradeName,
    this.companyStreetAddress,
    this.companyPhone,
    this.amountOfCreditRequired,
    this.bankAccountInformation,
    this.buyGroupName,
    this.companyCity,
    this.companyEmailAddress,
    this.companyFax,
    this.companyPostalCode,
    this.companyProvince,
    this.expectedSalesVolumePerYear,
    this.ficalYearEnd,
    this.financialInstitutionIncludingFinancialAddress,
    this.isPartOfBuyGroup,
    this.managerCity,
    this.managerGST,
    this.managerName,
    this.managerPhone,
    this.managerPhone2,
    this.managerPosalCode,
    this.managerProvince,
    this.ownerCellPhone,
    this.ownerHomeAddress,
    this.ownerHomePhone,
    this.ownerName,
    this.provincialSalesTax

  );

  Map<String, dynamic> toJson() =>
  {
    'company_legal_name': companyLegalName,
    'company_trade_name': companyTradeName,
    'street_address': companyStreetAddress,
    'phone': companyPhone,
    'fax': companyFax,
    'city': companyCity,
    'province': companyProvince,
    'postal_code': companyPostalCode,
    'email_address': companyEmailAddress,
    'owner_name': ownerName,
    'owner_home_address': ownerHomeAddress,
    'owner_home_phone': ownerHomePhone,
    'manager_name': managerName,
    'manager_phone': managerPhone,
    'manager_finacial_address': financialInstitutionIncludingFinancialAddress,
    'manager_bank_account': bankAccountInformation,
    'manager_city': managerCity,
    'manager_province': managerProvince,
    'manager_postal_code': managerPosalCode,
    'manager_phone_2': managerPhone2,
    'manager_gst': managerGST,
    'province_sales_tax': provincialSalesTax,
    'amount_credit_required': amountOfCreditRequired,
    'yearly_sales_volume': expectedSalesVolumePerYear,
    'fiscal_year_end': ficalYearEnd,
    'is_group': isPartOfBuyGroup,
    'group_name': buyGroupName
    
  };

}

class CustomerContactList{

  String name;
  String email;
  int itemIndex;

  CustomerContactList(
    this.name,
    this.email,
    this.itemIndex
  );

  Map<String, dynamic> toJson() =>
  {
    'name': name,
    'email': email,
    'itemIndex': itemIndex
  };

}


class ContactList extends StatefulWidget{

  final int count;
  final String title;
  List customerContactList = new List();

  ContactList({Key key, this.count, this.title, this.customerContactList }) : super(key: key);
  
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ContactListState( count, title, customerContactList );
  }
}

class _ContactListState extends State<ContactList>{
  final String title;
  List customerContactList = new List();
  //final MainBloc mainBloc;
  bool isVisible = true;
  int count;
  bool saved = false;
  String tabTitle;


  TextEditingController _name = new TextEditingController();
  TextEditingController _email = new TextEditingController();

  _ContactListState(this.count, this.title, this.customerContactList);

  @override
  void initState() {
    super.initState();
    tabTitle = '  '+title;
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Remove Item"),
          content: new Text("Are you sure you wanted to remove the Product item?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Yes"),
              onPressed: () {
                setState(() {
                  isVisible = false;
                  if( this.saved ){
                    customerContactList.removeWhere((contact){
                      if(contact.itemIndex == count ) {
                        return true;
                      } else {
                        return false;
                      }
                    });
                  }  
                });
                
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }

  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Visibility(
      visible: isVisible??true,
      child: Card(
        child: ExpandablePanel(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
          header: Text( tabTitle, style: globals.inputTextBold, ),
          tapHeaderToExpand: true,
          hasIcon: true,
          expanded: Card(
            
            margin: const EdgeInsets.only(bottom: 5),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _name,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Name',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Name',
                              labelStyle: globals.inputTextLight,
                          ),
                          // onSaved: (val) => this._name = val,
                          onChanged: ( value ){
                            setState(() {
                              customerContactList.removeWhere((contact){
                                if( contact.itemIndex == count ){
                                  return true;
                                } else {
                                  return false;
                                }
                              });
                              saved = true;
                              customerContactList.add( new CustomerContactList(_name.text??'', _email.text??'', count ) );
                            });
                          },
                          // enabled: false
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _email,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Email',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Email',
                              labelStyle: globals.inputTextLight,
                          ),
                          onChanged: ( value ){
                            setState(() {
                              customerContactList.removeWhere((contact){
                                if( contact.itemIndex == count ){
                                  return true;
                                } else {
                                  return false;
                                }
                              });
                              saved = true;
                              customerContactList.add( new CustomerContactList(_name.text??'', _email.text??'', count ) );
                            });
                            
                          },
                          // enabled: false
                        ),
                      ),
                    ),
                    
                  ],
                ),
                
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          this.saved ? 
                          IconButton( icon: Icon(Icons.check_circle), color: Colors.greenAccent, onPressed: (){} ) : 
                          IconButton( icon: Icon(Icons.close), color: Colors.redAccent, onPressed: (){},),

                          IconButton(
                            icon: Icon(Icons.delete),
                            color: Colors.redAccent,
                            onPressed: () {
                              _showDialog();
                            },
                          ),
                        ],
                      )
                      
                    ),
                    
                  ],
                )
              ],
            ),
          ),
          
          
        ),
      )
 
    );
  }
}
