import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import "package:autocomplete_textfield/autocomplete_textfield.dart";

import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/AddOrderScreenSalesmenSection.dart';
import 'package:primcoca/customer_list.dart';
import 'package:primcoca/blocs/product_bloc.dart';

class AddOrderScreen extends StatefulWidget {
  
  @override
  State<StatefulWidget> createState() => new _AddOrderScreenState();

}

class _AddOrderScreenState extends State<AddOrderScreen> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  // Auto complete
  GlobalKey<AutoCompleteTextFieldState<Customers>> key = new GlobalKey();
  AutoCompleteTextField searchTextField;
  TextEditingController controller = new TextEditingController();

  TextEditingController address = new TextEditingController();
  TextEditingController city = new TextEditingController();
  TextEditingController province = new TextEditingController();
  TextEditingController postalCode = new TextEditingController();
  TextEditingController sin = new TextEditingController();
  // TextEditingController poNumber = new TextEditingController();
  TextEditingController shipTo = new TextEditingController();
  TextEditingController additionalComments = new TextEditingController();
  TextEditingController shipVia = new TextEditingController();
  TextEditingController shipDate = new TextEditingController();
  bool isTestOrder = false;
  
  _AddOrderScreenState();

  String dealerName = "";
  final format = DateFormat("yyyy-MM-dd");

  Future<Customers> customer;
  bool dataLoaded = false;

  @override
  void initState() {
    super.initState();
    _loadData();
    mainBloc = MainBloc();
    
  }

  @override
  void dispose() {
    // TODO: implement dispose
    //mainBloc.dispose();
    super.dispose();
  }

  void _loadData() async {
    await CustomerViewModel.loadPlayers().then((onValue){
      
      setState(() {
        dataLoaded = true;  
      });
    });
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
      ));
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Order: Dealer Section',),
        backgroundColor: globals.secondaryColor,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: (){
              setState(() {
                
              });
            },
          )
        ],
      ),
      key: scaffoldKey,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: dataLoaded == false ? 
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(

                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    color: Color.fromRGBO(255, 255, 255, 0.5),
                    child: Column(
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 30,
                        ),
                        Text('Loading Data')
                      ],
                    ),
                  ),
                
                
              ]
            ),
          ) :
        
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Dealer Section', style: globals.h1Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,

            ),
            Form(
              key: formKey,
              autovalidate: true,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: 
                        searchTextField = AutoCompleteTextField<Customers>(
                          //style: new TextStyle(color: Colors.black, fontSize: 16.0),
                          controller: controller,
                          decoration: new InputDecoration(
                            contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            focusedBorder:  OutlineInputBorder(
                              borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            //icon: Icon(Icons.lock,color: Colors.black38,),
                            hintText: 'Dealer Name',
                            hintStyle: globals.inputTextLight,
                            labelText: 'Dealer Name',
                            labelStyle: globals.inputTextLight,
                          ),
                          
                          itemSubmitted: (item) {
                            mainBloc.setCustomerId(this, item.id);
                            mainBloc.setAddress(this, item.address);
                            mainBloc.setCity(this, item.city);
                            mainBloc.setProvince(this, item.province);
                            mainBloc.setPostalCode(this, item.postalCode);
                            mainBloc.setSin(this, item.sin);
                            setState(() {
                              address.text = item.address;
                              province.text = item.province;
                              city.text = item.city;
                              postalCode.text = item.postalCode;
                              if( item.sin != "" ) {
                                sin.text = "In File";
                              } else {
                                sin.text = "Required";
                              }
                              
                              
                              searchTextField.textField.controller.text = item.autocompleteterm;
                            });
                            print(mainBloc.address);
                            
                          },
                          clearOnSubmit: false,
                          key: key,
                          suggestions: CustomerViewModel.players,
                          itemBuilder: (context, item) {
                            //print(item.autocompleteterm);
                            var _name = item.autocompleteterm??'';
                            var _id = item.customerId??'';
                            var customerName = _name +':'+ _id;
                            return Text(
                                  '  '+customerName, 
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    height: 2.5
                                  ),
                                );
                          },
                          itemSorter: (a, b) {
                            return a.autocompleteterm.compareTo(b.autocompleteterm);
                          },
                          itemFilter: (item, query) {
                            return item.autocompleteterm
                                .toLowerCase()
                                .contains(query.toLowerCase());
                          }),
                        
                        ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: address,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Address *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Address *',
                                labelStyle: globals.inputTextLight,
                            ),
                            enabled: false,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: city,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'City *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'City *',
                                labelStyle: globals.inputTextLight,
                            ),
                            enabled: false,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: province,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Province *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Province *',
                                labelStyle: globals.inputTextLight,
                            ),
                            enabled: false,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: postalCode,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Postal Code *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Postal Code *',
                                labelStyle: globals.inputTextLight,
                            ),
                            enabled: false,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: sin,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'SIN',
                                hintStyle: globals.inputTextLight,
                                labelText: 'SIN',
                                labelStyle: globals.inputTextLight,
                            ),
                            enabled: false,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      
                      
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: shipTo,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Ship To',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Ship To',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setShipTo(this, val);
                            },
                            
                          ),
                        ),
                      ),

                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: DateTimeField(
                            format: format,
                            //controller: shipDate,
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2100));
                            },
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Ship Date',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Ship Date',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setShipDate(this, val);
                            },
                          ),
                        ),
                      ),
                      
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: additionalComments,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Additional Comments',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Additional Comments',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setAdditionalComments(this, val);
                            }
                          ),
                        ),
                      ),
                      
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: shipVia,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Ship Via',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Ship Via',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setShipVia(this, val);
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Checkbox( 
                        value: isTestOrder,
                        onChanged: (value) {

                          setState(() {
                            isTestOrder = value;
                            mainBloc.setOrderTestStatus(this, value);
                          });

                        },
                      ),
                      Text('Is it a test order?')
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: ButtonTheme(
                          // minWidth: MediaQuery.of(context).size * .90,
                          minWidth: MediaQuery.of(context).size.width * .90,
                          height: 45.0,
                          child: RaisedButton( 
                            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
                            color: globals.secondaryColor,
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                            onPressed: (){
                              print('customerid' + mainBloc.customerId.toString() );

                              if( mainBloc.customerId == "" || mainBloc.customerId == null ) {
                                
                                showInSnackBar("Can't proceed without any customer. Please select a Customer from the list.");
                                
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    // return object of type Dialog
                                    return AlertDialog(
                                      title: new Text("Error"),
                                      content: new Text("Can't proceed without any customer. Please select a Customer from the list."),
                                      actions: <Widget>[
                                        // usually buttons at the bottom of the dialog
                                        new FlatButton(
                                          child: new Text("Close"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              } else {
                                Navigator.push(
                                  context,  
                                  MaterialPageRoute( builder: (context) => AddOrderScreenSalesmenSection( mainBloc: mainBloc ) )
                                );
                              }
                              
                            },

                          ),
                        ),
                      )
                    ],
                  )
                  
                ],
              ),
            ),
            
          ],
        )
      )
    );
  }
}






//