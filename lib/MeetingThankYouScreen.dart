import 'package:flutter/material.dart';
import 'package:primcoca/LoginSignupScreen.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class MeetingThankYouScreen extends StatefulWidget{
  // final String pdfLink;
  // final String qrCodeLink;

  MeetingThankYouScreen({ Key key }): super(key: key);

  @override
  State<StatefulWidget> createState() => new _MeetingThankYouScreenState();

}

class _MeetingThankYouScreenState extends State<MeetingThankYouScreen>{
  // final String pdfLink;
  // final String qrCodeLink;

  _MeetingThankYouScreenState();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      
      backgroundColor: globals.secondaryColor,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              Text(
                'Thank you for being with Primco.',
                style: globals.h1White,
                textAlign: TextAlign.center,
              ),
              RaisedButton(
                child: Text('Back to Home'),
                color: Colors.white,
                textColor: globals.secondaryColor,
                onPressed: () {
                  Navigator.pushReplacement(context, 
                    MaterialPageRoute( builder: (context) => LoginSignupScreen() )
                  );
                }
              ),
              
              
              
            ],
          )  
        )
        
      ),
    );
  }
}