import 'dart:io';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:primcoca/ClientMeetingScreen.dart';
import 'package:primcoca/EventListScreen.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/SalesRepDashboardScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginSignupScreen extends StatefulWidget {


  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  
  const LoginSignupScreen({Key key, this.fieldKey, this.hintText, this.labelText, this.helperText, this.onSaved, this.validator, this.onFieldSubmitted }) : super(key: key);

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
      hintColor: Colors.red,
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(
            color: Colors.yellow,
            fontSize: 24.0
        ),
      ),
    );
  }
  @override
  State<StatefulWidget> createState() => login();
}

class login extends State<LoginSignupScreen> {

  ShapeBorder shape;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  final LocalStorage userDataStorage = new LocalStorage('userData');
  // final dbHelper = DatabaseHelper.instance;

  int _counter = 0;

  String _email;
  String _password;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _autovalidate = false;
  bool _formWasEdited = false;
  bool isLoggedIn = false;

  
  String _contactText;

  @override
  void initState() {
    super.initState();
    // mainBloc = MainBloc();
    // mainBloc.customerId = "";
    
  }

  

  @override
  void dispose() {
    // TODO: implement dispose
    // mainBloc.dispose();
    super.dispose();
  }

  
  @override
  Widget build(BuildContext context) {
    
    //checkLoginStatus();

    // TODO: implement build
    bool _obscureText = true;
    return new Stack(
      children: <Widget>[
        
        Scaffold(
        key: scaffoldKey,
        backgroundColor: globals.primaryColorOpac,
        body: Center(
          
          child: new SingleChildScrollView(
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                
                //new Image.asset('images/logo.png'),
                SizedBox(height: 20.0,),
                new SafeArea(
                  top: false,
                  bottom: false,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    // elevation: 5.0,
                    child: Form(
                      key: formKey,
                      autovalidate: _autovalidate,
                      child: SingleChildScrollView(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text('Login', style: globals.h1White, textAlign: TextAlign.center,),
                            const SizedBox(height: 20.0),
                            TextFormField(
                              // initialValue: 'ram@yopmail.com',
                              style: globals.inputTextWhite,
                              
                              decoration: InputDecoration(
                                  
                                  contentPadding: const EdgeInsets.symmetric( vertical: 20, horizontal: 25),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white, style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(33.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white,style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(33.0),
                                  ),
                                  focusedBorder:  OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white, style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(33.0),
                                  ),
                                  //icon: Icon(Icons.email,color: Colors.black38,),
                                  hintText: 'Your email address',
                                  //labelText: 'E-mail',
                                  //fillColor: Colors.white,
                                  labelStyle: TextStyle(color: Colors.white),
                                  hintStyle: TextStyle(
                                    color: globals.darkBackground
                                  ),
                                  
  
                              ),
                              //keyboardType: TextInputType.emailAddress,
                              validator: (val) =>
                              !val.contains('@') ? 'Not a valid email.' : null,
                              onSaved: (val) => _email = val,
                            ),

                            const SizedBox(height: 20.0),
                            TextFormField(
                              // initialValue: 'admin@123',
                              obscureText: true,
                              style: globals.inputTextWhite,
                              decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.symmetric( vertical: 20, horizontal: 25),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(33.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white,style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(33.0),
                                  ),
                                  focusedBorder:  OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white,style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(33.0),
                                  ),
                                  //icon: Icon(Icons.lock,color: Colors.black38,),
                                  hintText: 'Your password',
                                  hintStyle: TextStyle(
                                    color: globals.darkBackground
                                  ),
                                  //labelText: 'Password',
                                  labelStyle: TextStyle(color: Colors.white)
                              ),

                              validator: (val) =>
                              val.length < 6 ? 'Password too short.' : null,
                              onSaved: (val) => _password = val,
                            ),

                            SizedBox(height: 20.0,),
                            new Container(
                              child: Column(
                                //mainAxisSize: MainAxisSize.max,
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  ButtonTheme(
                                    // minWidth: MediaQuery.of(context).size * .90,
                                    minWidth: MediaQuery.of(context).size.width * .90,
                                    height: 65.0,
                                    child: RaisedButton( 
                                      //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                                      child: Text( 'Login', style: globals.bigButtonWhite ),
                                      color: globals.secondaryColor,
                                      textColor: Colors.white,
                                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                                      onPressed: (){
                                        _submit();
                                      },
 
                                    ),
                                  ),
                                  SizedBox( height: 20, ),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                  SizedBox( height: 20, ),
                                  Column(children: <Widget>[
                                    
                                    SizedBox( height: 20, ),
                                    ButtonTheme(
                                      // minWidth: MediaQuery.of(context).size * .90,
                                      minWidth: MediaQuery.of(context).size.width * .90,
                                      height: 65.0,
                                      child: RaisedButton( 
                                        //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                                        child: Text( 'Schedule Meeting', style: globals.bigButtonWhite ),
                                        color: globals.secondaryColor,
                                        textColor: Colors.white,
                                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                                        onPressed: (){
                                          Navigator.push(
                                            context, 
                                            MaterialPageRoute( builder: (context) => ClientMeetingScreen() )
                                            // CANCForm3()
                                          );
                                        },
  
                                      ),
                                    ),
                                    SizedBox( height: 20, ),
                                    ButtonTheme(
                                      // minWidth: MediaQuery.of(context).size * .90,
                                      minWidth: MediaQuery.of(context).size.width * .90,
                                      height: 65.0,
                                      child: RaisedButton( 
                                        //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                                        child: Text( 'Events', style: globals.bigButtonWhite ),
                                        color: globals.secondaryColor,
                                        textColor: Colors.white,
                                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                                        onPressed: (){
                                          Navigator.push(
                                            context, 
                                            MaterialPageRoute( builder: (context) => EventListScreen() )
                                            // CANCForm3()
                                          );
                                        },
  
                                      ),
                                    ),
                                    
                                  ],)
                                ],
                              ),
                            ),
                                                                     ]
                          ),
                        )
                      )        //login,
                    )
                )
              ],
            ),
          )
        ))
      ],
      
    );
    
      
  }

  void _submit() async{
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      var body = { 'username': _email, 'password': _password }; 
      var encodedEmail = utf8.encode(_email);
      var encodedPassword = utf8.encode(_password);
      var encodedColan = utf8.encode(":");
      var authorizationHeader = base64.encode( encodedEmail +encodedColan+ encodedPassword ); 
      var httpHeader = {
        HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
        HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
      };
      http.post( globals.baseURL+'users/login.json' , body: body, headers: httpHeader).then((http.Response response) async {
        
        final int statusCode = response.statusCode;
        if (statusCode < 200 || statusCode > 400 || json == null) {
          showInSnackBar("Please check username and password.");
          return;
        }
        
        var responsData = json.decode(response.body);
        if( responsData['status'].toString() == "1" ) {
          showInSnackBar( 'Welcome!' );
          userDataStorage.setItem('userData', responsData['user'] );
          userDataStorage.setItem('authorizationHeader',authorizationHeader );

          print('userData: '+responsData['user'].toString());

          UserData userData = UserData.fromJson(responsData['user']);

          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('currentUserId', userData.id.toString());
          prefs.setString('role', userData.role.toString());

          print('currentUserId: '+userData.id.toString());

          Navigator.pushReplacement(
            context, 
            MaterialPageRoute( builder: (context) => SalesRepDashboardScreen() )
          );
          
        } else {
          showInSnackBar( responsData[0]['message'] );
        }
        
      });

      showInSnackBar( 'Please wait! Logging in..' );
      
    }
    else{
      showInSnackBar('Please fix the errors in red before submitting.');

    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value)
      ));
  }
  
  _verticalD() => Container(
      margin: EdgeInsets.only(left: 10.0, right: 0.0, top: 0.0, bottom: 0.0),
  );

}

class User{
  String userId;
  String fullName;
  String firstName;
  String lastName;
  String userEmail;
  String status;

  User({
    this.userId,
    this.fullName,
    this.firstName,
    this.lastName,
    this.userEmail,
    this.status
  });

  factory User.fromJson(Map<String, dynamic> parsedJson){
    return User(
        userId: parsedJson['id'],
        userEmail: parsedJson['username'],
        fullName: parsedJson['full_name'],
        firstName: parsedJson['first_name'],
        lastName: parsedJson['last_name'],
        status: parsedJson['status']
    );
  }

}

class UserData{
  int id;
  String username;
  String role;
  String created;
  String modified;

  UserData({
    this.id,
    this.username,
    this.role,
    this.created,
    this.modified,
  });

  factory UserData.fromJson(Map<String, dynamic> parsedJson){
    return UserData(
        id: parsedJson['id'],
        username: parsedJson['username'],
        role: parsedJson['role'],
        created: parsedJson['created'],
        modified: parsedJson['modified']
    );
  }

}

