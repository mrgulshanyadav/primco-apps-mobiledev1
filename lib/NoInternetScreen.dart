import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:primcoca/main.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class NoInternetScreen extends StatefulWidget {
  NoInternetScreen({Key key}) : super(key: key);

  @override
  _NoInternetScreenState createState() => _NoInternetScreenState();
}

class _NoInternetScreenState extends State<NoInternetScreen> {
  
  String internetConnectionStatus = "Server is not reachable."; 

  Future<Null> initConnectivity() async {
    
    final response = await http.get( globals.baseURL );
    try {
      
      final response = await http.get( globals.baseURL );  
      
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => MyHomePage()), ModalRoute.withName('/') );
    } catch (e) {
      Navigator.push(context, MaterialPageRoute(builder: (context)=>NoInternetScreen()));
    }


  }

  int _selectedIndex = 0; 

  void _onItemTapped(int index) {
    
    if( index == 0 ){
      initConnectivity();
    } else {
      exit(0);
    }
    
    setState(() {
      _selectedIndex = index;
    });

  }

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return new Scaffold(
      backgroundColor: Color.fromRGBO(240, 240, 240, 1),
      body: Container(
        child: Center(
          child: Text(
            internetConnectionStatus,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 25.0,
              color: Colors.blueGrey
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.refresh),
            title: new Text('Retry'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.exit_to_app),
            title: new Text('Close'),
            
          ),
        ],
        fixedColor: Colors.deepPurple,
        onTap: _onItemTapped,
      ),

    );
  }
}
