import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class Event{
  
  int id;
  String name;
  String description;
  String from;
  String link;
  String to;

  Event({
    this.id,
    this.description,
    this.name,
    this.from,
    this.link,
    this.to
  });
}

class EventList{
  String date;
  List<Event> event;
  
  EventList({
    this.date,
    this.event
  });
}

class EventDetailScreen extends StatefulWidget{
  final List subEvent;
  final String tagLine;
  final String description;
  EventDetailScreen({ Key key, this.subEvent, this.tagLine, this.description }): super(key: key);
  
  @override
  State<StatefulWidget> createState() => new _EventDetailScreenState(subEvent, tagLine, description);

}

class _EventDetailScreenState extends State<EventDetailScreen>{
  // final String pdfLink;
  // final String qrCodeLink;
  final List subEvent;
  final String tagLine;
  final String description;
  _EventDetailScreenState( this.subEvent, this.tagLine, this.description );

  List dateList = new List();
  List<EventList> eventList = new List<EventList>();
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // print(subEvent);
    processData();
  }

  processData(){
    
    subEvent.map((f){
      List<Event> event = new List<Event>();
      
      DateFormat dateFormat = DateFormat('dd');
      var dateTime = DateTime.parse( f['event_date'].toString() );
      var eventDates = dateFormat.format(dateTime);
      
      int eventIndex;
      eventIndex = eventList.indexWhere((test){
        if( test.date == eventDates ) {
          return true;
        }
        return false;
      });
      if( eventIndex >= 0 ) {
        
        event.add( new Event(id: f['event_id'], name: f['name'], description: f['description'], from: f['from_time'], to: f['to_time'], link:  f['link'] ) );

        for (var i = 0; i < eventList[eventIndex].event.length ; i++) {
          event.add( new Event(id: eventList[eventIndex].event[i].id , name: eventList[eventIndex].event[i].name, description: eventList[eventIndex].event[i].description, from: eventList[eventIndex].event[i].from, to: eventList[eventIndex].event[i].to, link: eventList[eventIndex].event[i].link ) );
        }

        eventList.removeWhere((test){
          if( test.date == eventDates ) {
            return true;
          }
          return false;
        });

        eventList.insert(
          eventIndex,
          new EventList(
            date: eventDates,
            event: event
          )
        );
      } else {
        
        event.add( new Event(id: f['event_id'], name: f['name'], description: f['description'], from: f['from_time'], to: f['to_time'], link:  f['link'] ) );
        eventList.add(
          new EventList(
            date: eventDates,
            event: event
          )
        );
      }
      
    }).toList();
    eventList = eventList.reversed.toList();


    
    
    
  }

  @override
  Widget build(BuildContext context) {
    
    // print(eventList.length);
    
    // TODO: implement build
    return DefaultTabController(
        length: eventList.length,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              
              indicatorColor: Colors.redAccent,
              tabs: eventList.map((f){
                return Tab( child: Text( f.date, style: globals.h2White, ), );
              }).toList()
            ),
            title: Text('Event Details'),
          ),
          
          body: TabBarView(

            children: eventList.map((f){
              // print(f.date);
              
              Widget eventWidget;



              eventWidget = Column(
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    children: f.event.map((_f){

                      print(_f.id );
                      print(_f.to.toString());
                      print( "length: "+_f.link.toString() ); 
                      
                      DateFormat fromDateFormat = DateFormat().add_jm();
                      var fromDateTime = DateTime.parse( _f.from.toString() );
                      var fromDate = fromDateFormat.format(fromDateTime);

                      var toDateTime = DateTime.parse( _f.to.toString() );
                      var toDate = fromDateFormat.format(toDateTime);
                      

                      return Card(

                          margin: const EdgeInsets.symmetric( horizontal: 10, vertical: 5 ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Padding(

                            padding: const EdgeInsets.symmetric( horizontal: 20, vertical: 20 ),
                            child: Row(
                              
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 3.0,
                                      color: globals.secondaryColor
                                    ),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(15.0) //         <--- border radius here
                                    ),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Text( fromDate.toString()??'' ),
                                      // SizedBox( height: 5,),
                                      Container(
                                        height: 4,
                                        width: 30,
                                        margin: const EdgeInsets.symmetric(vertical: 3),
                                        child: SizedBox(height: 2,),
                                        decoration: BoxDecoration(
                                          color: globals.secondaryColor,
                                          border: Border.all(
                                            width: 2.0,
                                            color: globals.secondaryColor
                                          ),
                                        ),
                                      ),
                                      // SizedBox( height: 5,),
                                      Text( toDate.toString()??'' )
                                    ],
                                  ),
                                ),
                                SizedBox(width: 30,),
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        _f.name??'',
                                        style: globals.h2Secondary,
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        _f.description??'',
                                      ),
                                      _f.link.toString().length > 5 ? RaisedButton(
                                        onPressed: () async{
                                          if ( await canLaunch(_f.link) ) {
                                            await launch(_f.link);
                                          }
                                        },
                                        color: globals.secondaryColor,
                                        textColor: Colors.white,
                                        child: Text('Download PDF'),
                                      ): SizedBox(height: 0,)
                                      
                                      
                                      
                                    ],
                                  ),
                                ),
                                
                              ],
                            ),
                          ) 
                        )
                      ;
                    }).toList()
                  ),
                  Text( tagLine??'', style: globals.h2Secondary, ),
                  RichText(
                    overflow: TextOverflow.fade,
                    text: TextSpan(
                      text: "Booth # MRNRS\nMariner A&B\n3950 S Las Vegas Blvd\nLas Vegas, NV 89119\nUnited States",
                      style: globals.pSecondary
                    ),
                  ),
                  //Text( description??'', style: globals.pSecondary, ),

                  
                ],
              );
              
              return Tab(child: eventWidget, );
            }).toList()
          ),
        
        ),
      );
  }
}