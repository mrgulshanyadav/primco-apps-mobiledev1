import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:primcoca/EventDetailScreen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class EventListScreen extends StatefulWidget{
  EventListScreen({ Key key }): super(key: key);

  @override
  State<StatefulWidget> createState() => new _EventListScreenState();

}

class _EventListScreenState extends State<EventListScreen>{
  // final String pdfLink;
  // final String qrCodeLink;

  _EventListScreenState();
  List<Widget> children;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // backgroundColor: globals.secondaryColor,
      appBar: AppBar(
        title: Text('Future Events',),
        backgroundColor: globals.secondaryColor,
      ),  
      body: SingleChildScrollView(

        child: Padding(
          padding: const EdgeInsets.all(20),

          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              
              SizedBox(
                height: 20,
              ),
              // Text(
              //   'Set Meeting', style: globals.h1Secondary, textAlign: TextAlign.center,
              // ),
              FutureBuilder<http.Response>(
                future: http.post( globals.baseURL+'events/getallevents.json' ) ,
                builder: (context, snapshot){
                  
                  
                  if (snapshot.hasData) {
                    children.clear();
                    var responseData = json.decode(snapshot.data.body);
                    //print(responseData);

                    if ( responseData['status'].toString() == "1" ) {
                      
                      if( responseData['events'].length > 0 ) {
                        for (var i = 0; i < responseData['events'].length ; i++) {
                          
                          DateFormat dateFormat = DateFormat('dd-MM-yyy hh:mm a');
                          var dateTime = DateTime.parse( responseData['events'][i]['dated_and_time'].toString() );
                          var enentDates = dateFormat.format(dateTime);
                          children.add(
                            SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(
                                        width: MediaQuery.of(context).size.width,
                                        child: 
                                           
                                          Container(
                                            decoration: BoxDecoration(
                                              color: globals.secondaryColor,
                                              borderRadius: new BorderRadius.only( topLeft: new Radius.circular(15.0), topRight: new Radius.circular(15.0) ),
                                              
                                            ),
                                            child: responseData['events'][i]['image'] != null ?
                                            ClipRRect(
                                                borderRadius: new BorderRadius.circular(15.0),
                                                child: Image( 
                                                  image: NetworkImage( responseData['events'][i]['image'] ),  
                                                ),
                                            ) :
                                            Image.asset('images/logo.png')
                                          ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all( 20 ),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            
                                            Text( 
                                              responseData['events'][i]['name'], 
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: globals.h3FontSize,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text( 
                                              responseData['events'][i]['description'], 
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: globals.pFontSize,
                                              ),
                                              maxLines: 10,
                                              overflow: TextOverflow.visible,
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: RichText(
                                                    overflow: TextOverflow.fade,
                                                    text: TextSpan(
                                                      children: [
                                                        WidgetSpan(
                                                          child: GestureDetector(
                                                            onTap: ()async{
                                                              if ( await canLaunch(responseData['events'][i]['map_link']) ) {
                                                                await launch(responseData['events'][i]['map_link']);
                                                              }
                                                            },
                                                            child: Column(
                                                              children: <Widget>[
                                                                Icon(
                                                                  Icons.location_on,
                                                                  color: Colors.grey,
                                                                  size: globals.pFontSize,
                                                                ),
                                                                Text( responseData['events'][i]['location'].toString() )
                                                              ],
                                                            ),
                                                          )
                                                          
                                                        ),
                                                        
                                                      ]
                                                    ),
                                                  ),
                                                ),
                                                
                                                Expanded(
                                                  
                                                  child: RaisedButton(
                                                    onPressed: (){
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                          EventDetailScreen( subEvent: responseData['events'][i]['sub_events'] , tagLine: responseData['events'][i]['tagline'], description: responseData['events'][i]['description'] )
                                                        )
                                                      );
                                                    },
                                                    child: Text( 'Details' ),
                                                    color: globals.secondaryColor,
                                                    textColor: Colors.white,
                                                  ),
                                                )
                                              ],
                                            ),
                                            
                                          ],
                                        )
                                      )
                                    ],
                                  )
                                  
                                  
                                )
                              )
                            );
                        }
                      } else {
                        
                      }
                      

                      return Column(
                        children: children,
                      );
                      
                    } else {
                      children = <Widget>[
                        Icon(
                          Icons.close,
                          color: Colors.redAccent,
                          size: 60,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Text('Unable to fetch the data.'),
                        )
                      ];
                    }
                    
                    
                    
                  } else if (snapshot.hasError) {
                    print(snapshot.error);
                    children = <Widget>[
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ];
                  } else {
                    
                    children = <Widget>[
                      SizedBox(
                        child: CircularProgressIndicator(),
                        width: 60,
                        height: 60,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Text('Awaiting result...'),
                      )
                    ];
                  }
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: children,
                    ),
                  );
                },
              )
              
              
            ],
          )  
        )
        
      ),
    );
  }
}