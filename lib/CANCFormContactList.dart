import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

import 'package:primcoca/CANCForm3.dart';
import 'package:primcoca/customer_list.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class CANCFormContactList extends StatefulWidget {
  final String contactId;
  CANCFormContactList({ Key key, this.contactId }): super(key: key);
  @override
  State<StatefulWidget> createState() => new _CANCFormContactListState( contactId );

}

class _CANCFormContactListState extends State<CANCFormContactList>{

  final String contactId;
  _CANCFormContactListState(this.contactId);

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final LocalStorage userDataStorage = new LocalStorage('userData');

  var statementsContact = List<Widget>();
  var invoiceContact = List<Widget>();
  var pricePagesContact = List<Widget>();
  var orderConfirmationContact = List<Widget>();
  var backOrderArrivalContact = List<Widget>();
  var purchasingListContact = List<Widget>();
  
  int statementCount = 0;
  int invoiceCount = 0;
  int pricePagesCount = 0;
  int orderConfirmationCount = 0;
  int backOrderCount = 0;
  int purchaseListCount = 0;

  List statementsList = new List();
  List invoiceList = new List();
  List pricePagesList = new List();
  List orderConfirmationList = new List();
  List backOrderList = new List();
  List purchaseListList = new List();

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
      ));
    
  }
  
  

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Contact List',),
        backgroundColor: globals.secondaryColor,
      ),
      key: scaffoldKey,
      persistentFooterButtons: <Widget>[
        ButtonTheme(
          // minWidth: MediaQuery.of(context).size * .90,
          minWidth: MediaQuery.of(context).size.width * .90,
          height: 45.0,
          child: RaisedButton( 
            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
            color: globals.secondaryColor,
            textColor: Colors.white,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
            onPressed: (){
              //saveData();
              var postData = {
                'contact_id': contactId,
                'statements': jsonEncode(statementsList),
                'invoices': jsonEncode( invoiceList ),
                'price_pages': jsonEncode( pricePagesList ),
                'order_confirmation': jsonEncode( orderConfirmationList ),
                'back_order': jsonEncode( backOrderList ),
                'purchase_list': jsonEncode( purchaseListList )
              };
              print(postData);

              showInSnackBar('Saving data...');

              var authorizationHeader = userDataStorage.getItem('authorizationHeader');
              // http://dev.rndexperts.in/primco/api/contacts-list/addcontact.json
              http.post( globals.baseURL+'contacts-list/addcontact.json', 
                body: postData, 
                headers: {
                  HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader,
                  HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
                }
              ).then((http.Response response) {
                final int statusCode = response.statusCode;
                debugPrint(response.body);
                if (statusCode < 200 || statusCode > 400 || json == null) {
                  throw new Exception("Error while fetching data");
                }
                
                var responsData = json.decode(response.body);
                
                //print(responsData);
                if( responsData['status'] == 1 ) {
                  showInSnackBar('Data saved. Redirecting...');
                  //print(responsData);
                  Navigator.pushReplacement(context, 
                    MaterialPageRoute( builder: (context) => 
                      CANCForm3( contactId: contactId, )
                      //CANCFormContactList( contactId: responsData['creditApplication']['id'].toString() ) ,
                    )
                  );
                } else {

                  

                  showInSnackBar('Error while saving data!');
                  //print('falied');
                
                  // showInSnackBar('Failed to save data.'+ responseData['errors'] );
                }
                
              });


            },

          ),
        )
      ],
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Statements', style: globals.h2Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: statementsContact
            ),
            SizedBox(
              height: 10,
            ),

            Column(
              children: <Widget>[
                Text(
                  'Tap button to add.',
                ),
                RaisedButton(
                  color: globals.secondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                  onPressed: (){
                    setState(() {
                      statementCount++;
                      statementsContact.add( new ContactList( count: statementCount, title: "Statement", customerContactList: statementsList ) );  
                      for (var i = 0; i < statementsList.length; i++) {
                        print( statementsList[i].name );
                        print( statementsList[i].email );
                        print( statementsList[i].itemIndex );
                      }
                    });
                  },
                  child: Text('Add More Statements Contact'),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Invoices', style: globals.h2Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: invoiceContact,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Text(
                  'Tap button to add.',
                ),
                RaisedButton(
                  color: globals.secondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                  onPressed: (){
                    setState(() {
                      invoiceCount++;
                      invoiceContact.add( new ContactList( count: invoiceCount, title: "Invoice", customerContactList: invoiceList ) );  
                    });
                  },
                  child: Text('Add More Invoice Contact'),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Price Pages', style: globals.h2Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: pricePagesContact,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Text(
                  'Tap button to add.',
                ),
                RaisedButton(
                  color: globals.secondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                  onPressed: (){
                    setState(() {
                      pricePagesCount++;
                      pricePagesContact.add( new ContactList( count: pricePagesCount, title: "Price Pages", customerContactList: pricePagesList ) );  
                    });
                  },
                  child: Text('Add More Price Pages Contact'),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Order Confirmation', style: globals.h2Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: orderConfirmationContact,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Text(
                  'Tap button to add.',
                ),
                RaisedButton(
                  color: globals.secondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                  onPressed: (){
                    setState(() {
                      orderConfirmationCount++;
                      orderConfirmationContact.add( new ContactList( count: orderConfirmationCount, title: "Order Confirmation", customerContactList: orderConfirmationList ) );  
                    });
                  },
                  child: Text('Add More Order Confirmation Contact'),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Notification of Back Order Arrival', style: globals.h2Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: backOrderArrivalContact,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Text(
                  'Tap button to add.',
                ),
                RaisedButton(
                  color: globals.secondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                  onPressed: (){
                    setState(() {
                      backOrderCount++;
                      backOrderArrivalContact.add( new ContactList( count: backOrderCount, title: "Back Order Arrival", customerContactList: backOrderList ) );  
                    });
                  },
                  child: Text('Add More Back Order Arrival Contact'),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Purchasing List', style: globals.h2Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: purchasingListContact,
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Text(
                  'Tap button to add.',
                ),
                RaisedButton(
                  color: globals.secondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                  onPressed: (){
                    setState(() {
                      purchaseListCount++;
                      purchasingListContact.add( new ContactList( count: purchaseListCount, title: "Purchasing List", customerContactList: purchaseListList ) );  
                    });
                  },
                  child: Text('Add More Purchasing List Contact'),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      )
    );
  }
}




