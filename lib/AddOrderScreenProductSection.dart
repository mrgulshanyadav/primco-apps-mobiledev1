import 'dart:io';
import 'dart:convert';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:expandable/expandable.dart';
import "package:autocomplete_textfield/autocomplete_textfield.dart";
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:primcoca/ThankYouScreen.dart';
import 'package:primcoca/customer_list.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'blocs/product_bloc.dart';

class AddOrderScreenProductSection extends StatefulWidget {
  final MainBloc mainBloc;
  AddOrderScreenProductSection({ Key key, this.mainBloc }): super(key: key);
  @override
  State<StatefulWidget> createState() => new _AddOrderScreenProductSectionState( mainBloc );

}

class _AddOrderScreenProductSectionState extends State<AddOrderScreenProductSection> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _sign = GlobalKey<SignatureState>();

  TextEditingController httpError = new TextEditingController();
  TextEditingController franklinTextController = new TextEditingController();
  TextEditingController poNumber = new TextEditingController();
  
  final MainBloc mainBloc;
  _AddOrderScreenProductSectionState( this.mainBloc );
  final _key = GlobalKey<_ProductRowState>();

  var productNameList = new List<ProductItem>();
  var productStateKey = List<GlobalKey<_ProductRowState>>();

  var formFieldsColumn = List<Widget>();
  int count = 0;
  bool dataLoaded = true;
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mainBloc.setGrandTotal(this, 0.0, 0.0, 0.0, 0.0);
    loadProducts();
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message)
    )); 
  }

  void loadProducts() async{
    await ProductItemViewModel.loadProduct().then((onValue){
      
      setState(() {
        productNameList = ProductItemViewModel.product;
        dataLoaded = false;  
      });
    });
    
    
  }

  double franklin;
  double points;
  double grossTotal;
  double grandTotal;

  bottomText(){
    franklin = 0.0;
    points = 0.0;
    grossTotal = 0.0;
    grandTotal = 0.0;
    

    for (var i = 0; i < mainBloc.orderItems.length; i++) {
      var productItem = mainBloc.orderItems[i];
      if( productItem != null) {
        try {
          
          franklin += productItem.franklin != null ? double.tryParse(productItem.franklin??'0') : 0.0;
          points +=  productItem.points != "" ? double.tryParse(productItem.points) : 0.0;
          grossTotal += productItem.total != null ? double.tryParse(productItem.total) + double.tryParse( productItem.discount ) : 0.0;
          grandTotal +=  productItem.total != null ? double.tryParse(productItem.total) : 0.0;
          
        } catch (e) {
          print(e);
          franklin = 0.0;
          points = 0.0;
          grossTotal = 0.0;
          grandTotal = 0.0;
        }
        
      }
    }

    franklinTextController.text = franklin.toStringAsFixed(2);
    
    return BottomNavigationBar(
      currentIndex: 0, // this will be set when a new tab is tapped
      unselectedItemColor: globals.secondaryColor,
      unselectedFontSize: 12,
      unselectedLabelStyle: globals.pSecondary,
      showUnselectedLabels: true,
      items: [
        BottomNavigationBarItem(
          icon: new Icon(Icons.card_giftcard, color: globals.secondaryColor),
          title: new Text('\$'+franklin.toStringAsFixed(2), style: globals.pSecondary,),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.star_border, color: globals.secondaryColor),
          title: new Text(points.toStringAsFixed(2), style: globals.pSecondary,),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.attach_money, color: globals.secondaryColor),
          title: Text('\$'+grossTotal.toString(), style: globals.pSecondary,)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.account_balance_wallet, color: globals.secondaryColor),
          title: Text('\$'+grandTotal.toStringAsFixed(2), style: globals.pSecondary,)
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    //bottomText();
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        
        title: Text('Add Order: Order Section',),
        backgroundColor: globals.secondaryColor,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: (){
              setState(() {
                  productStateKey.map((f){
                    f.currentState.updateWidget();
                  }).toList();
                  
              });
              
            },
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            productStateKey.add( new GlobalKey<_ProductRowState>() );
            formFieldsColumn.add( new ProductRow( key: productStateKey[count] , count: count, mainBloc: mainBloc, productNameList: productNameList ) );  
            count++;
          });
        },
        child: Icon( Icons.add, color: Colors.white, ),
        backgroundColor: globals.primaryColor,
        foregroundColor: Colors.white,
      ),
      bottomNavigationBar: bottomText(),
      //bottomSheet: bottomText(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: Stack(
          children: <Widget>[
            dataLoaded ? Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(

                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Color.fromRGBO(255, 255, 255, 0.5),
                      child: Column(
                        children: <Widget>[
                          CircularProgressIndicator(),
                          SizedBox(
                            height: 30,
                          ),
                          Text('Loading Data')
                        ],
                      ),
                    ),
                  
                  
                ]
              ),
            ) : 
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  'Order Section', style: globals.h1Secondary, textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  key: formKey,
                  autovalidate: true,
                  child: Column(
                    children: formFieldsColumn,
                  ),

                ),
                SizedBox(
                  height: 20,
                ),
                Text('Signature', style: TextStyle(fontWeight: FontWeight.bold) ,),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 200,
                  width: 300,
                  child: Container(
                    
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Colors.black,
                        width: 1,
                      ),
                    ),
                    child: Signature(

                      color: Colors.black,// Color of the drawing path
                      strokeWidth: 2.0,
                      backgroundPainter: null, // Additional custom painter to draw stuff like watermark 
                      onSign: null, // Callback called on user pan drawing
                      key: _sign,
                      // key that allow you to provide a GlobalKey that'll let you retrieve the image once user has signed
                    ),
                  )
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: franklinTextController,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Fanklins',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Franklins',
                              labelStyle: globals.inputTextLight,
                          ),
                          //onSaved: (val) => this.description = val,
                          // onChanged: ( value ){
                          //   this.description = value;
                          // },
                          enabled: true
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: poNumber,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                            contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            focusedBorder:  OutlineInputBorder(
                              borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            //icon: Icon(Icons.lock,color: Colors.black38,),
                            hintText: 'P.O. Number',
                            hintStyle: globals.inputTextLight,
                            labelText: 'P.O. Number',
                            labelStyle: globals.inputTextLight,
                          ),
                          validator: (val){
                            //if( val.isEmpty ) return 'Required.';
                          },
                          onChanged: (val){
                            mainBloc.setPoNumber(this, val);
                          }
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    ButtonBar(
                      alignment: MainAxisAlignment.center,
                      children: <Widget>[

                        RaisedButton(
                          color: globals.secondaryColor,
                          textColor: Colors.white,
                          child: Text(
                            'Clear Signature'
                          ),
                          onPressed: (){
                            _sign.currentState.clear();
                          },
                        ),
                        RaisedButton(
                          onPressed: () async{

                            if( mainBloc.orderItems.length <= 0 ){
                              showInSnackBar('Please select products.');
                              return;  
                            }
                            
                            showInSnackBar('Saving data...');

                            final sign = _sign.currentState;
                            final image = await sign.getData();
                            var data = await image.toByteData(format: ui.ImageByteFormat.png);
                            final encodedSignature = base64.encode(data.buffer.asUint8List());
                            var shipDate = mainBloc.shipDate.toString();
                            if( shipDate == 'null' ) {
                              shipDate = '';
                            }

                            var saleDate = mainBloc.saleDate.toString();
                            if( saleDate == 'null' ) {
                              saleDate = '';
                            }

                            var postData = {
                              'customer_id': mainBloc.customerId,
                              'po_number': mainBloc.poNumber??'',
                              'ship_to': mainBloc.shipTo??'',
                              'is_test': mainBloc.isTest ? '1' : '0',
                              'additional_comments': mainBloc.additionalComments??'',
                              'ship_via': mainBloc.shipVia??'',
                              'ship_date': shipDate.toString(),
                              'salesman_id': mainBloc.salesRepId??'',
                              'events': mainBloc.salesRepEvent??'',
                              'sale_order_number': mainBloc.salesOrderId??'',
                              'incentive_reciept': mainBloc.incentiveAck??'',
                              'purchaser_name': mainBloc.purchserName??'',
                              'sale_date': saleDate.toString(),
                              'orderItems': jsonEncode( mainBloc.orderItems ),
                              'franklin_total': franklinTextController.text,
                              'grand_total': grandTotal.toStringAsFixed(2),
                              'signature': encodedSignature
                            };
                            //print(mainBloc.shipDate);
                            
                            
                            http.post( globals.baseURL+'orders/appAdd.json', 
                              body: postData, 
                              headers: {
                                HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
                              }
                            ).then((http.Response response) {
                              
                              final int statusCode = response.statusCode;
                              
                              if (statusCode < 200 || statusCode > 400 || json == null) {
                                throw new Exception("Error while fetching data");
                              }
                              
                              var responsData = json.decode(response.body);
                              
                              if( responsData['order']['call_status'] == 1 ) {
                                showInSnackBar('Data saved. Generating PDF.');
                                Navigator.pushReplacement(context, 
                                  MaterialPageRoute( builder: (context) => 
                                    ThankYouScreen( 
                                      qrCodeLink: responsData['response']['qrcode_link'], 
                                      pdfLink: responsData['response']['pdf_link'] 
                                    ) 
                                  )
                                );
                              } else {
                                //showInSnackBar(responsData[0]['message']);
                                showInSnackBar('Sorry! We were unable to process data.');
                              }
                              
                            });

                          },
                          child: Text('Save and Submit'),
                          color: globals.secondaryColor,
                          textColor: Colors.white,
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),           
                
                
              ],
            )
          
          ],
        ),
        
      )
    );
  }
}


class ProductRow extends StatefulWidget{

  final int count;
  final MainBloc mainBloc;
  final List<ProductItem> productNameList;

  ProductRow({Key key, this.count, this.mainBloc, this.productNameList }): super(key: key);
  
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductRowState( count, mainBloc, productNameList);
  }
}

class _ProductRowState extends State<ProductRow>{
  
  final MainBloc mainBloc;
  bool isVisible;
  final List<ProductItem> productNameList;

  _ProductRowState( this.count, this.mainBloc, this.productNameList);

  var franklinEnabled = true;
  bool saved = false; 
  bool enableCash = false; 
  bool checkCash = false; 
  int count;
  String productName, 
  productId,
  itemName, 
  itemNumber, 
  description, 
  uom, 
  cash, 
  price,
  total,
  discount, 
  quantity, 
  franklin, 
  points;

  String tabTitle;

  ItemNumber selectItem;

  GlobalKey<AutoCompleteTextFieldState<ProductItem>> suggestionKey = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<ItemName>> itemNameKey = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<ItemNumber>> itemNumberKey = new GlobalKey();

  AutoCompleteTextField searchTextField;
  AutoCompleteTextField itemNameTextField;
  AutoCompleteTextField itemNumberTextField;

  TextEditingController controller = new TextEditingController();
  TextEditingController itemNameController = new TextEditingController();
  TextEditingController itemNumberController = new TextEditingController();

  TextEditingController _description = new TextEditingController();
  TextEditingController _uom = new TextEditingController();
  TextEditingController _cash = new TextEditingController();
  TextEditingController _price = new TextEditingController();
  TextEditingController _quantity = new TextEditingController();
  TextEditingController _discount = new TextEditingController();
  TextEditingController _total = new TextEditingController();
  TextEditingController _franklin = new TextEditingController();
  TextEditingController _points = new TextEditingController();

  ProductItem _selectedProductName; 
  
  List<ItemName> productItemNameList = new List<ItemName>();
  ItemName _selectItemName;
  DropdownButton itemNameDD;
  bool displayItemNameDD;
  String itemNameLoadingText;
  GlobalKey itemNameDDKey = new GlobalKey();

  List<ItemNumber> productItemNumberList = new List<ItemNumber>();
  ItemNumber _selectItemNumber;
  DropdownButton itemNumberDD;
  bool displayItemNumberDD;
  String itemNumberLoadingText;
  GlobalKey itemNumberDDKey = new GlobalKey();

  final GlobalKey<_ProductRowState> productRowStateKey = GlobalKey<_ProductRowState>();
  
  String accountName;
  DropDownField ddf;
  IconButton refreshButton;

  GlobalKey buttonKey = new GlobalKey();

  @override
  void initState() {
    super.initState();
    //_loadData();
    // mainBloc = MainBloc();
    _discount.addListener( calculateTotal );
    tabTitle = '  Product';
    
    displayItemNameDD = false;
    itemNameLoadingText = 'No item.';
    itemNumberLoadingText = 'No item.';
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  updateWidget(){
    setState(() {
      var indexOfMine = mainBloc.orderItems.indexWhere((product){
        if( product.index == count ){
          return true;
        } else {
          return false;
        }
      });
      
      if( indexOfMine >= 0 ) {
        print('->'+count.toString());
        print( jsonEncode(mainBloc.orderItems[indexOfMine]) );
        var _updateProduct = mainBloc.orderItems[indexOfMine];
        
        _franklin.text = franklinFormula(_updateProduct.productName, _updateProduct.bulkCount, _updateProduct.quantity, _updateProduct.singleFranklin, _updateProduct.bulkFranklin);
        
        _total.text = totalFormula(_updateProduct.productName, _updateProduct.bulkCount, _updateProduct.quantity, _updateProduct.singleItem, _updateProduct.bulkItem, _updateProduct.perItem, _updateProduct.cash, _updateProduct.discount);

        // mainBloc.setFranklinAndTotal(state, index, quantity, franklin, price, item)
        this.mainBloc.setFormFieldValue(
            this, 
            _updateProduct.index,
            _updateProduct.productId,  
            _updateProduct.productName, 
            _updateProduct.itemName, 
            _updateProduct.itemNumber, 
            _updateProduct.description, 
            _updateProduct.uom, 
            _updateProduct.cash, 
            _updateProduct.price, 
            _total.text, 
            _updateProduct.discount, 
            _updateProduct.quantity, 
            _updateProduct.franklinEnabled, 
            _franklin.text, 
            _updateProduct.points, 
            _updateProduct.bulkCount,
            _updateProduct.singleFranklin,
            _updateProduct.bulkFranklin,
            _updateProduct.perItem,
            _updateProduct.singleItem,
            _updateProduct.bulkItem
          );

      }
      
    });
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Remove Item"),
          content: new Text("Are you sure you wanted to remove the Product item?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Yes"),
              onPressed: () {
                setState(() {
                  isVisible = false;
                  if( this.saved ){
                    mainBloc.unsetProductItem(this,count);
                  }  
                });
                
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void calculateTotal(){
    int quantity = int.tryParse( _quantity.text );
    int bulkCount = int.tryParse(selectItem.bulkCount);
    double singleItem = double.tryParse( selectItem.singleItem );
    double bulkItem = double.tryParse( selectItem.bulkItem );
    double perItem = double.tryParse( selectItem.perItem );
    double price = double.tryParse(_price.text);
    double grandTotal;

    double discount = 0.0;
    if( _discount.text != null || _discount.text != "" ){
      discount = double.tryParse(_discount.text);
    }

    _total.text = totalFormula( selectItem.productName , bulkCount, quantity, singleItem, bulkItem, perItem, this.checkCash, discount).toString();
    
  }

  void calculateFranklin(){
    
    int quantity = int.tryParse( _quantity.text );
    int bulkCount = int.tryParse(selectItem.bulkCount);
    double singleFranklin = double.tryParse( selectItem.singleFranklin );
    double bulkFranklin = double.tryParse( selectItem.bulkFranklin);
    
    _franklin.text = franklinFormula(selectItem.productName, bulkCount, quantity, singleFranklin, bulkFranklin);
    this.franklin = _franklin.text;
  }

  franklinFormula( productName, bulkCount, quantity, singleFranklin, bulkFranklin ){

    quantity = int.tryParse( quantity.toString() );
    bulkCount = int.tryParse( bulkCount.toString() );
    singleFranklin = double.tryParse( singleFranklin.toString() );
    bulkFranklin = double.tryParse( bulkFranklin.toString() );

    num multiplier;
    bool isBulk = quantity >= bulkCount;
    double franklinResponse;
    
    if( productName == 'Adhesive' || productName == 'Underlay-Edm' || productName == 'Underlay-Depot' || productName == 'Underlay-Cgy' ) {

      multiplier = 0;
      if( quantity >= bulkCount && checkCash ) {
        multiplier = quantity / bulkCount;
        multiplier = multiplier.floor();
      } else {
        multiplier = 0;
      }
      // The value of bulk franklin has to be changed
      franklinResponse = multiplier * singleFranklin;
    
    } else {
      if( isBulk ) {
        franklinResponse = quantity * bulkFranklin;
      } else {
        franklinResponse = quantity * singleFranklin;
      }
      
    }

    return franklinResponse.toStringAsFixed(2);
  }

  totalFormula( productName, bulkCount, quantity, singleItem, bulkItem, perItem, isCash, discount ){

    quantity = int.tryParse(quantity.toString());
    bulkCount = int.tryParse(bulkCount.toString());
    singleItem = double.tryParse( singleItem.toString() );
    bulkItem = double.tryParse( bulkItem.toString() );
    perItem = double.tryParse( perItem.toString() );
    discount = double.tryParse( discount.toString() );

    bool isBulk;
    if( quantity >= bulkCount ){
      isBulk = true;
    } else {
      isBulk = false;
    }
    double totalResponse;
    if( productName == 'Adhesive' ) {
      if( isBulk ){
        totalResponse = ( quantity * singleItem ) - discount;
      } else {
        totalResponse = ( quantity * bulkItem ) - discount;
      }
      
    } else if( productName == 'Underlay-Edm' || productName == 'Underlay-Depot' || productName == 'Underlay-Cgy' ){
      bool checkCash;
      
      if( isCash.runtimeType == bool ){
        checkCash = isCash == true ? true: false;
      } else {
        checkCash = isCash == "true" ? true : false;
      }
      
      if( checkCash ){
        totalResponse = ( quantity * perItem * bulkItem ) - discount;
      } else {
        totalResponse = ( quantity * perItem * singleItem ) - discount;
      }

    } else {
      if ( isBulk ) {
        totalResponse = ( quantity * singleItem * perItem ) - discount;  
      } else {
        totalResponse = ( quantity * bulkItem * perItem ) - discount;
      }
      
    }
    return totalResponse.toStringAsFixed(2);  
    
  }
  

  void updateProductItem(){
    int checkQuantity = 0 ;
    
    if( selectItem == null ) return;
    
    int quantity = int.tryParse( _quantity.text );
    int bulkCount = int.tryParse(selectItem.bulkCount);
    double bulkFranklin = double.tryParse( selectItem.bulkFranklin);
    double singleFranklin = double.tryParse( selectItem.singleFranklin );
    double singleItem = double.tryParse( selectItem.singleItem );
    double bulkItem = double.tryParse( selectItem.bulkItem );
    double perItem = double.tryParse( selectItem.perItem );
    double discount = double.tryParse( _discount.text );
    double price = double.tryParse( selectItem.perItem );

    
    for( int i = 0; i < this.mainBloc.orderItems.length ; i++ ) {
    
      if( this.mainBloc.orderItems[i].itemName == selectItem.itemName && this.mainBloc.orderItems[i].index != count ) {
        checkQuantity += int.tryParse(this.mainBloc.orderItems[i].quantity);
      }
    }
    
    checkQuantity += quantity;

    _franklin.text = franklinFormula(selectItem.productName, bulkCount, quantity, singleFranklin, bulkFranklin);
    
    if( bulkCount <= checkQuantity ) {
      
      // _franklin.text = ( quantity * bulkFranklin ).toString();
      // _total.text = ( ( price * quantity * bulkItem ) - discount ).toStringAsFixed(2);

      _franklin.text = franklinFormula(selectItem.productName, bulkCount, quantity, singleFranklin, bulkFranklin);
      _total.text = totalFormula(selectItem.productName, bulkCount, quantity, singleItem, bulkItem, perItem, checkCash, discount).toString();
      
      for( int i = 0; i < this.mainBloc.orderItems.length ; i++ ) {
        if( this.mainBloc.orderItems[i].itemName == selectItem.itemName && this.mainBloc.orderItems[i].index != count ) {
          
          var itemTotal = totalFormula(this.mainBloc.orderItems[i].productName, this.mainBloc.orderItems[i].bulkCount, this.mainBloc.orderItems[i].quantity, this.mainBloc.orderItems[i].singleItem, this.mainBloc.orderItems[i].bulkItem, this.mainBloc.orderItems[i].perItem, this.mainBloc.orderItems[i].cash, this.mainBloc.orderItems[i].discount );

          var franklinTotal = franklinFormula(this.mainBloc.orderItems[i].productName, this.mainBloc.orderItems[i].bulkCount, this.mainBloc.orderItems[i].quantity, this.mainBloc.orderItems[i].singleFranklin, this.mainBloc.orderItems[i].bulkFranklin);

          this.mainBloc.setFormFieldValue(
            this, 
            this.mainBloc.orderItems[i].index,
            this.mainBloc.orderItems[i].productId,  
            this.mainBloc.orderItems[i].productName, 
            this.mainBloc.orderItems[i].itemName, 
            this.mainBloc.orderItems[i].itemNumber, 
            this.mainBloc.orderItems[i].description, 
            this.mainBloc.orderItems[i].uom, 
            this.mainBloc.orderItems[i].cash, 
            this.mainBloc.orderItems[i].price, 
            itemTotal, 
            this.mainBloc.orderItems[i].discount, 
            this.mainBloc.orderItems[i].quantity, 
            this.mainBloc.orderItems[i].franklinEnabled, 
            franklinTotal.toString(), 
            this.mainBloc.orderItems[i].points, 
            this.mainBloc.orderItems[i].bulkCount,
            this.mainBloc.orderItems[i].singleFranklin,
            this.mainBloc.orderItems[i].bulkFranklin,
            this.mainBloc.orderItems[i].perItem,
            this.mainBloc.orderItems[i].singleItem,
            this.mainBloc.orderItems[i].bulkItem
          );

        }
      }

    } else {
      for( int i = 0; i < this.mainBloc.orderItems.length ; i++ ) {
        if( this.mainBloc.orderItems[i].itemName == selectItem.itemName && this.mainBloc.orderItems[i].index != count ) {

          var itemTotal = totalFormula(this.mainBloc.orderItems[i].productName, this.mainBloc.orderItems[i].bulkCount, this.mainBloc.orderItems[i].quantity, this.mainBloc.orderItems[i].singleItem, this.mainBloc.orderItems[i].bulkItem, this.mainBloc.orderItems[i].perItem, this.mainBloc.orderItems[i].cash, this.mainBloc.orderItems[i].discount );

          var franklinTotal = franklinFormula(this.mainBloc.orderItems[i].productName, this.mainBloc.orderItems[i].bulkCount, this.mainBloc.orderItems[i].quantity, this.mainBloc.orderItems[i].singleFranklin, this.mainBloc.orderItems[i].bulkFranklin);
          
          this.mainBloc.setFormFieldValue(
            this, 
            this.mainBloc.orderItems[i].index,
            this.mainBloc.orderItems[i].productId,  
            this.mainBloc.orderItems[i].productName, 
            this.mainBloc.orderItems[i].itemName, 
            this.mainBloc.orderItems[i].itemNumber, 
            this.mainBloc.orderItems[i].description, 
            this.mainBloc.orderItems[i].uom, 
            this.mainBloc.orderItems[i].cash, 
            this.mainBloc.orderItems[i].price, 
            itemTotal, 
            this.mainBloc.orderItems[i].discount, 
            this.mainBloc.orderItems[i].quantity, 
            this.mainBloc.orderItems[i].franklinEnabled, 
            franklinTotal, 
            this.mainBloc.orderItems[i].points, 
            this.mainBloc.orderItems[i].bulkCount,
            this.mainBloc.orderItems[i].singleFranklin,
            this.mainBloc.orderItems[i].bulkFranklin,
            this.mainBloc.orderItems[i].perItem,
            this.mainBloc.orderItems[i].singleItem,
            this.mainBloc.orderItems[i].bulkItem
          );
        }
      }
    }
    
    
    
    this.mainBloc.setFormFieldValue(
      this, 
      count, 
      selectItem.id, 
      selectItem.productName, 
      selectItem.itemName, 
      selectItem.itemNumber, 
      _description.text, 
      _uom.text, 
      checkCash, 
      _price.text,
      _total.text,
      _discount.text, 
      _quantity.text,
      franklinEnabled ? '1' : '0', 
      franklinEnabled ? _franklin.text : '0', 
      _points.text,
      selectItem.bulkCount,
      selectItem.singleFranklin,
      selectItem.bulkFranklin,
      selectItem.perItem,
      selectItem.singleItem,
      selectItem.bulkItem
    );
    
    // double grandTotal = 0.0;
    // double grossTotal = 0.0;
    // double franklin = 0.0;
    // double points = 0.0;
    // for (var i = 0; i < mainBloc.orderItems.length ; i++) {
    //   grandTotal = grandTotal + double.tryParse(mainBloc.orderItems[i].total);
    //   grossTotal = grossTotal + double.tryParse(mainBloc.orderItems[i].total)  + double.tryParse(mainBloc.orderItems[i].discount ?? "0.0" );

    //   if( mainBloc.orderItems[i].points != "" ){
    //     points = points + double.tryParse( mainBloc.orderItems[i].points );
    //   }
      
    //   if( mainBloc.orderItems[i].franklinEnabled == "1" && 
    //   ( mainBloc.orderItems[i].franklin != "" ) ){
    //     franklin = franklin + double.tryParse( mainBloc.orderItems[i].franklin ?? "0.0" );
    //     franklin = franklinFormula(productName, bulkCount, quantity, singleFranklin, bulkFranklin)
    //   }
    // }
    // mainBloc.setGrandTotal( this, grandTotal, franklin, points, grossTotal );
  }

  generateProductItemNameDropdown( state ){
    _selectItemName = null;
    itemNameDD = DropdownButton(
      disabledHint: Text('No item.'),
      hint: Text('Select Product Item'),
      key: itemNameDDKey,
      value: _selectItemName,
      isDense: true,
      onChanged: ( newValue) async{
        
        await ItemNumberViewModel.loadItemNumber( _selectedProductName.productName , newValue.itemName ).then( (onValue){
          productItemNumberList.clear();
          productItemNumberList = ItemNumberViewModel.itemNumber;
          _selectItemNumber = null;
          setState((){
            _selectItemName = newValue;
            generateProductItemNumberDropdown();
          });
          
            
        }).then((onValue){
          //_selectItemNumber = productItemNumberList[0];
        });
        
      },
      isExpanded: true,
      items: productItemNameList.map(( value) {
          
          return new DropdownMenuItem(
            value: value,
            child: new Text(value.autocompleteterm, overflow: TextOverflow.ellipsis),
          );
        
        
      }).toList(),
    );
    
    _selectItemName = productItemNameList[0];

  }

  initGenerateProductItemNameDropdown(){
    itemNameDD = DropdownButton(
      disabledHint: Text('No item.'),
      hint: Text('Select Product Item'),
      key: itemNameDDKey,
      value: _selectItemName,
      isDense: true,
      isExpanded: true,
      onChanged: ( newValue) async{
        
        
        await ItemNumberViewModel.loadItemNumber( _selectedProductName.productName , newValue.itemName ).then((onValue){
          productItemNumberList.clear();
          productItemNumberList = ItemNumberViewModel.itemNumber;
          setState((){
            _selectItemName = newValue;
            _selectItemNumber = null;
            generateProductItemNumberDropdown();

          });
        }).then((onValue){
          setState(() {
            //_selectItemNumber = productItemNumberList[0];
          });
          
        });
      },
      items: productItemNameList.map(( value) {
          
          return new DropdownMenuItem(
            value: value,
            child: new Text(value.autocompleteterm, overflow: TextOverflow.ellipsis),
          );
      }).toList(),
    );
    
  }

  generateProductItemNumberDropdown( ){
    
    displayItemNumberDD = true;
    itemNumberLoadingText = "No item.";
    itemNumberDD = null;
    itemNumberDD = DropdownButton(
      isExpanded: true,
      disabledHint: Text('No item.'),
      hint: Text('Select Item Number'),
      key: itemNumberDDKey,
      value: _selectItemNumber,
      isDense: true,
      onChanged: ( newValue) async {
        selectItem = newValue;
        _selectItemNumber = newValue;
        
        //itemNumberTextField.textField.controller.text = _selectItemNumber.autocompleteterm;
        _description.text = selectItem.colorName;
        _uom.text = selectItem.itemType;
        _price.text = selectItem.singleItem;
        _quantity.text = "1";
        _discount.text = "0";
        this.productId = selectItem.id;
        tabTitle = '  '+ selectItem.productName +' : '+ _selectItemName.itemName +' : '+ selectItem.colorName ;

        if( selectItem.productName.indexOf('Underlay-Edm') >= 0 || 
          selectItem.productName.indexOf('Underlay-Depot') >= 0 || 
          selectItem.productName.indexOf('Underlay-Cgy') >= 0 ) {
            
          enableCash = true;
        }

        setState(() {
          calculateFranklin();
          calculateTotal();
          this.saved = true;
          updateProductItem();  
          
        });
      },
      items: productItemNumberList.map(( value) {
        return new DropdownMenuItem(
          value: value,
          child: new Text(value.autocompleteterm, overflow: TextOverflow.ellipsis),
        );
      }).toList(),
    );
    //setState(() {});
    
  }

  initGenerateProductItemNumberDropdown( ){
    
    displayItemNumberDD = true;
    itemNumberLoadingText = "No item.";
    itemNumberDD = DropdownButton(
      disabledHint: Text('No item.'),
      hint: Text('Select Item Number'),
      isExpanded: true,
      key: itemNumberDDKey,
      value: _selectItemNumber,
      isDense: true,
      onChanged: ( newValue) async {
        
        selectItem = newValue;
        _selectItemNumber = newValue;
        //itemNumberTextField.textField.controller.text = _selectItemNumber.autocompleteterm;
        _description.text = selectItem.colorName;
        _uom.text = selectItem.itemType;
        _price.text = selectItem.singleItem;
        _quantity.text = "1";
        _discount.text = "0";
        this.productId = selectItem.id;
        tabTitle = '  '+ selectItem.productName +' : '+ _selectItemName.itemName +' : '+ selectItem.colorName ;

        if( selectItem.productName.indexOf('Underlay-Edm') >= 0 || 
          selectItem.productName.indexOf('Underlay-Depot') >= 0 || 
          selectItem.productName.indexOf('Underlay-Cgy') >= 0 ) {
            
          enableCash = true;
        }
        
        setState(() {
          // calculateFranklin();
          // calculateTotal();
          this.saved = true;
          updateProductItem();  
          
        });

      },
      items: productItemNumberList.map(( value) {
        return new DropdownMenuItem(
          value: value,
          child: new Text(value.autocompleteterm, overflow: TextOverflow.ellipsis ,),
        );
      }).toList(),
    );
    //setState(() {});
  }

  var dealerName;
  @override
  Widget build(BuildContext context) {
    displayItemNameDD = true;
    displayItemNumberDD = true;
    initGenerateProductItemNameDropdown();
    initGenerateProductItemNumberDropdown();
    //generateProductItemNameDropdown();
    // TODO: implement build
    return Visibility(
      visible: isVisible??true,
      child: Card(
        child: ExpandablePanel(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
          header: Text( tabTitle, style: globals.inputTextBold, ),
          tapHeaderToExpand: true,
          hasIcon: true,
          expanded: Card(
            
            margin: const EdgeInsets.only(top: 5),
            child: Column(
              children: <Widget>[
                
                Row(
                  children: <Widget>[
                    Expanded(
                      child: new FormField(
                        builder: (FormFieldState state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                            
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Select Product Name',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Product Name',
                              labelStyle: globals.inputTextLight,
                            ),
                            isEmpty: _selectedProductName == ProductItem(),
                            child: new DropdownButtonHideUnderline(
                              child: new DropdownButton(
                                value: _selectedProductName,
                                isDense: true,
                                isExpanded: true,
                                onChanged: ( newValue) async{
                                  
                                  await ItemNameViewModel.loadItemName(newValue.productName).then((onValue){
                                    
                                    productItemNameList.clear();
                                    productItemNameList = ItemNameViewModel.items;
                                    generateProductItemNameDropdown( state );
                                    
                                    displayItemNameDD = true;
                                    _selectItemName = null;

                                    setState(() {
                                      _selectedProductName = newValue;
                                      state.didChange(newValue);
                                    });

                                  }).then((onValue){
                                    //_selectItemName = productItemNameList[0];
                                  });
                                  
                                },
                                items: productNameList.map(( value) {
                                  return new DropdownMenuItem(
                                    value: value,
                                    child: new Text(value.productName),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    displayItemNameDD == true ? Expanded(
                      child: new FormField(
                        builder: (FormFieldState state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Select Item Name',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Item Name',
                              labelStyle: globals.inputTextLight,
                            ),
                            isEmpty: _selectItemName == new ItemName(),
                            child: new DropdownButtonHideUnderline(
                              child: itemNameDD,
                            ),
                          );
                        },
                      ),
                    ) : 
                    Expanded(
                      child: Text(itemNameLoadingText, textAlign: TextAlign.center,),
                    ),
                    displayItemNumberDD == true ? Expanded(
                      child: new FormField(
                        builder: (FormFieldState state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                            
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Select Item Number',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Item Number',
                              labelStyle: globals.inputTextLight,
                            ),
                            isEmpty: _selectItemNumber == new ItemNumber(),
                            child: new DropdownButtonHideUnderline(
                              child: itemNumberDD,
                            ),
                          );
                        },
                      ),
                    ) : 
                    Expanded(
                      child: Text(itemNumberLoadingText, textAlign: TextAlign.center,),
                    )
                  ],
                ),
                // Row 2
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _description,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                            contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            focusedBorder:  OutlineInputBorder(
                              borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            //icon: Icon(Icons.lock,color: Colors.black38,),
                            hintText: 'Description',
                            hintStyle: globals.inputTextLight,
                            labelText: 'Description',
                            labelStyle: globals.inputTextLight,
                          ),
                          onSaved: (val) => this.description = val,
                          onChanged: ( value ){
                            this.description = value;
                          },
                          enabled: false
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _uom,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Unit Of Measure',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Unit Of Measure',
                              labelStyle: globals.inputTextLight,
                          ),
                          onChanged: ( value ){
                            this.uom = value;
                          },
                          enabled: false
                        ),
                      ),
                    ),
                    enableCash ? Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Checkbox( 
                              value: this.checkCash,
                              onChanged: (bool value) {
                                setState(() {
                                  this.checkCash = value;
                                  //calculateTotal();

                                  if( value == true && ( selectItem.productName == 'Underlay-Edm' || 
                                    selectItem.productName == "Underlay-Depot" || 
                                    selectItem.productName == "Underlay-Cgy" ) ) {
                                      _price.text = selectItem.bulkItem;
                                    
                                  } else if( value == false && ( selectItem.productName == 'Underlay-Edm' || 
                                    selectItem.productName == "Underlay-Depot" || 
                                    selectItem.productName == "Underlay-Cgy" ) ){
                                      _price.text = selectItem.singleItem; 
                                  }

                                  this.saved = true;
                                  updateProductItem();  
                                  
                                });
                                
                              },
                            ),
                            Text('Cash')
                          ],
                        ),
                      ),
                    ) : Text(''),
                  ],
                ),
                // Row 3
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _price,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Price',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Price',
                              labelStyle: globals.inputTextLight,
                          ),
                          enabled: false,
                          onSaved: (val) => this.price = val,
                          onChanged: ( value ){
                            this.price = value;
                            
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _quantity,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Quantity',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Quantity',
                              labelStyle: globals.inputTextLight,
                          ),
                          keyboardType: TextInputType.number,
                          // onSaved: (val) => this.quantity = val,
                          onChanged: ( value ){
                            print(value);
                            print(selectItem.bulkCount);
                            if( ( int.tryParse( value ) > int.tryParse(selectItem.bulkCount) ) && ( selectItem.productName == 'Underlay-Edm' || 
                              selectItem.productName == "Underlay-Depot" || 
                              selectItem.productName == "Underlay-Cgy" ) ) {
                                _price.text = selectItem.bulkItem;
                              
                            }
                            
                            setState(() {
                              this.quantity = value;
                              calculateFranklin();
                              calculateTotal();
                              this.saved = true;
                              updateProductItem();  
                            });
                          },
                          
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _discount,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Discount',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Discount',
                              labelStyle: globals.inputTextLight,
                          ),
                          keyboardType: TextInputType.number,
                          onSaved: (val){
                            calculateTotal();
                          },
                          
                          onChanged: ( value ){
                            setState(() {
                              this.discount = value;
                              calculateFranklin();
                              calculateTotal();
                              this.saved = true;
                              updateProductItem();  
                            });
                          },
                          
                        ),
                      ),
                    )
                  ],
                ),
                // Row 4
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _total,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Total',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Total',
                              labelStyle: globals.inputTextLight,
                          ),
                          enabled: false,
                          onSaved: (val) => this.total = val,
                          onChanged: ( value ){
                            this.total = value;
                            calculateFranklin();
                            calculateTotal();
                            updateProductItem(); 
                             
                          },

                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Checkbox( 
                              value: this.franklinEnabled??true,
                              onChanged: (value) {

                                setState(() {
                                  
                                  franklinEnabled = value;
                                  if( value ){
                                //    mainBloc.setGrandTotal(this, 1.2, 2.2, 3.3);
                                  } else {
                                  //  mainBloc.setGrandTotal(this, 0.0, 0.1, 0.2);
                                    _franklin.text = "";
                                  }
                                  calculateFranklin();
                                  calculateTotal();
                                  this.saved = true;
                                  updateProductItem();  
                                  
                                });

                              },
                            ),
                            Text('Franklin?')
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: franklinEnabled ? TextFormField( 
                          controller: _franklin,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Franklin',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Franklin',
                              labelStyle: globals.inputTextLight,
                          ),
                          onSaved: (val) => this.franklin = val,
                          onChanged: ( value ){
                            this.franklin = value;
                            calculateFranklin();
                            calculateTotal();
                            this.saved = true;
                            updateProductItem();  
                          },
                          enabled: false,
                          keyboardType: TextInputType.number,
                        ) : Text(''),
                      ),
                    ),
                    
                  ],
                ),
                // Row 5
                // Row 4
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: TextFormField( 
                          controller: _points,
                          style: globals.inputText,
                          decoration: InputDecoration(
                              
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              //icon: Icon(Icons.lock,color: Colors.black38,),
                              hintText: 'Promotions Points/UOM',
                              hintStyle: globals.inputTextLight,
                              labelText: 'Promotions Points/UOM',
                              labelStyle: globals.inputTextLight,
                          ),
                          onSaved: (val) => this.points = val,
                          onChanged: ( value ){
                            this.points = value;
                            setState(() {
                              calculateFranklin();
                              calculateTotal();
                              this.saved = true;
                              updateProductItem();  
                            });
                          },
                          keyboardType: TextInputType.number,

                        ),
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          this.saved ? 
                          IconButton( icon: Icon(Icons.check_circle), color: Colors.greenAccent, onPressed: (){},) : 
                          IconButton( icon: Icon( Icons.close), color: Colors.redAccent, onPressed: (){}, ),

                          IconButton(
                            icon: Icon(Icons.refresh),
                            color: Colors.grey,
                            onPressed: () {
                              
                              setState(() { 
                                if( selectItem == null ) return;
                                calculateFranklin();
                                calculateTotal();
                                this.saved = true;
                                updateProductItem();
                              });
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.delete),
                            color: Colors.redAccent,
                            onPressed: () {
                              
                              _showDialog();
                              
                            },
                          ),
                        ],
                      )
                      
                    ),
                    
                  ],
                )
              ],
            ),
          ),
          
          
        ),
      )
 
    );
  }
}
