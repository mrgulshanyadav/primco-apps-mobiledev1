import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:primcoca/AddOrderScreen.dart';

import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/HomeScreen.dart';
import 'package:primcoca/LoginSignupScreen.dart';
import 'package:localstorage/localstorage.dart';
import 'package:primcoca/SalesRepDashboardScreen.dart';


void main(){
  // debugPaintSizeEnabled = true;
  runApp(new MyApp());
} 

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    // checkPermission(context);
    // print( globals.userLocation[1] );

    return new MaterialApp(
        initialRoute: '/',
        debugShowCheckedModeBanner: false,
        routes: {
          // When we navigate to the "/" route, build the FirstScreen Widget
          '/home': (BuildContext context) => new HomeScreen(),
          '/addOrder': (BuildContext context) => new AddOrderScreen()
        },
        theme: new ThemeData(
          
            brightness: Brightness.light,
            primaryColor: globals.primaryColor,
            accentColor: globals.secondaryColor,
            
            // Define the default Font Family
            fontFamily: 'Poppins',
            // Define the default TextTheme. Use this to specify the default
            // text styling for headlines, titles, bodies of text, and more.
            textTheme: TextTheme(
              headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
              body1: TextStyle(fontSize: globals.pFontSize, fontFamily: 'Poppins'),
                
              display1: TextStyle(
                fontSize: 12.0,
                //decoration: TextDecoration.lineThrough,
                color: Colors.redAccent,
                fontWeight: FontWeight.w500,
                fontFamily: 'Poppins'
              ),

              display2: TextStyle(
                fontSize: 12.0,
                color: Colors.black45,
                decoration: TextDecoration.lineThrough,
                fontWeight: FontWeight.w500,
                fontFamily: 'Poppins'
              ),

              display3: TextStyle(
                fontSize: 12.0,
                color: Colors.white,
                fontWeight: FontWeight.w500,
                fontFamily: 'Poppins'
              ),

              display4: TextStyle(
                fontSize: globals.appFontSmallSize,
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontFamily: 'Poppins'
                
              ),

              title: TextStyle( 
                fontSize: 14.0, 
                fontStyle: FontStyle.normal, 
                color: globals.secondaryColor,
                fontFamily: 'Poppins'
              ),
              
            ),

        ),
        home: new MyHomePage(title: 'Primco'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  

  MyHomePage({Key key, this.title }) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();

  
}

class _MyHomePageState extends State<MyHomePage> {
  
  // final dbHelper = DatabaseHelper.instance;
  // final CreateJsonFile createJsonFile;
  _MyHomePageState();
  

  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  
  var platform = MethodChannel('crossingthestreams.io/resourceResolver');
  final LocalStorage userDataStorage = new LocalStorage('userData');  

  startTime() async {
    
    var _duration = new Duration(seconds: 4);
    return new Timer(_duration, navigationPage);
  }

  /*
  ConnectivityResult is an enum with the values as { wifi, mobile, none }.
  */
  @override
  void initState() { 
    super.initState();   
    startTime();
  }

  void navigationPage() {

    var userData = userDataStorage.getItem( 'userData' );
    if( userData == null ){
      print('empty');
      Navigator.pushReplacement( 
        context, 
        MaterialPageRoute( builder: (context) => LoginSignupScreen() )
      );
    } else {
      print('not empty');
      Navigator.pushReplacement(
        context, 
        MaterialPageRoute( builder: (context) => SalesRepDashboardScreen() )
      );
    }

    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    //checkPermission();

    return Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: globals.secondaryColor
        ),
        child: new Container(
          margin: new EdgeInsets.all(30.0), 
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              Container(
                child: Image.asset('images/logo.png'),
              ),
              
            ],
          ),
        ),
      );
  }

}
