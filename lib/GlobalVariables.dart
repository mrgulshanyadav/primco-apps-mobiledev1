library lojista.globals;
import 'package:flutter/material.dart';
// String baseURL = 'http://dev.rndexperts.in/orders.primco.ca/api/';
String baseURL = 'https://orders.primco.ca/api/';
// http://dev.rndexperts.in/primco/api/products/getproductname.json

String merchantListUrl = baseURL+'findAllMerchants';
String userLogin ="";

String currentStoreId;
// String currencySymbole ='\₹';
String currencySymbole ='Rs.';

String appTitle = "Primco";
// String razorPayAPIKey = "rzp_test_ImqExjUFTgbMWt";

// DATABASE CONFIG
// String currentMerchantId = '';
var userLocation = new List(2);
// List unitMarker = [ 'GMS', 'KG', 'ML', 'LTR', 'PCS' ];
// String deviceId;


// COLORS

// Color primaryColor = Colors.blueAccent;
// Color secondaryColor = Colors.red;

Color primaryColor = Color.fromRGBO(76, 82, 112, 1);
Color primaryColorOpac = Color.fromRGBO(76, 82, 112, 0.9);
// Color secondaryColor = Color.fromRGBO(246, 82, 160, 1);
Color secondaryColor = Color.fromRGBO(8, 84, 94, 1);
Color secondaryColor2 = Color.fromRGBO(255, 175, 0, 1);
Color thirdColor = Color.fromRGBO(105,239,225,1);
Color fourthColor = Color.fromRGBO(188,236,224,1);

double singleProductTitleSize = 20.0;
double appFontSize = 14.0;
double appFontSmallSize = 10.0;
const double infoTablePadding = 5.0;
const double sectionMargin = 10.0;
double h1FontSize = 35;
double h2FontSize = 22;
double h3FontSize = 19;
double h4FontSize = 15;
double pFontSize = 14;

Color darkBackground = Colors.white;

TextStyle infoTableContent = TextStyle(
  fontSize: appFontSmallSize,
  color: Colors.black,
  fontWeight: FontWeight.w300,
  
);

TextStyle bigButtonWhite = TextStyle(
  fontSize: appFontSize,
  color: Colors.white,
  fontWeight: FontWeight.w300,
  
);
TextStyle inputTextWhite = TextStyle(
  fontSize: appFontSize,
  color: Colors.white,
  fontWeight: FontWeight.w300,
  
);
TextStyle inputText = TextStyle(
  fontSize: appFontSize,
  color: Colors.black,
  fontWeight: FontWeight.w300,
  
);

TextStyle inputTextBold = TextStyle(
  fontSize: appFontSize,
  color: Colors.black,
  fontWeight: FontWeight.bold,
  
);
TextStyle inputTextLight = TextStyle(
  fontSize: appFontSize,
  color: Colors.black45,
  fontWeight: FontWeight.w300,
  
);
TextStyle bigButtonPrimary = TextStyle(
  fontSize: appFontSize,
  color: primaryColor,
  fontWeight: FontWeight.w300,
  
);

TextStyle contentSmall = TextStyle(
  fontSize: appFontSmallSize,
  color: Colors.black,
  fontWeight: FontWeight.w300,
  
);
TextStyle contentSmallPrimary = TextStyle(
  fontSize: appFontSmallSize,
  color: primaryColor,
  fontWeight: FontWeight.w300,
  
);

TextStyle contentSmallSecondary = TextStyle(
  fontSize: appFontSmallSize,
  color: secondaryColor,
  fontWeight: FontWeight.w300,
  
);

TextStyle pSecondary = TextStyle(
  fontSize: pFontSize,
  color: secondaryColor,
  fontWeight: FontWeight.w300,
  
);

TextStyle contentSmallWhite = TextStyle(
  fontSize: appFontSmallSize,
  color: Colors.white,
  fontWeight: FontWeight.w300,
  
);

TextStyle contentHeading = TextStyle(
  fontSize: singleProductTitleSize,
  color: primaryColor,
  fontWeight: FontWeight.w500,
);
TextStyle contentHeadingSmall = TextStyle(
  fontSize: appFontSize,
  color: primaryColor,
  fontWeight: FontWeight.w500,
  
);

TextStyle contentHeadingWhite = TextStyle(
  fontSize: singleProductTitleSize,
  color: Colors.white,
  fontWeight: FontWeight.w500,
);

TextStyle infoTableContentRed = TextStyle(
  fontSize: appFontSmallSize,
  color: Colors.redAccent,
  fontWeight: FontWeight.w300,
  decoration: TextDecoration.lineThrough
  
);

TextStyle infoTableHeading = TextStyle(
  fontSize: appFontSmallSize,
  color: Colors.black,
  fontWeight: FontWeight.bold,
  
);

TextStyle h1Primary = TextStyle(
  fontSize: h1FontSize,
  color: primaryColor,
  fontWeight: FontWeight.bold,
  
);

TextStyle h1White = TextStyle(
  fontSize: h1FontSize,
  color: Colors.white,
  fontWeight: FontWeight.bold,
  
);

TextStyle h1Secondary = TextStyle(
  fontSize: h1FontSize,
  color: secondaryColor,
  fontWeight: FontWeight.bold,
  
);

TextStyle h1Black = TextStyle(
  fontSize: h1FontSize,
  color: Colors.black,
  fontWeight: FontWeight.bold,
  
);

TextStyle h2Primary = TextStyle(
  fontSize: h2FontSize,
  color: primaryColor,
  fontWeight: FontWeight.bold,
  
);

TextStyle h3Primary = TextStyle(
  fontSize: h3FontSize,
  color: primaryColor,
  fontWeight: FontWeight.bold,
  
);

TextStyle h4Primary = TextStyle(
  fontSize: h4FontSize,
  color: primaryColor,
  fontWeight: FontWeight.bold,
  
);

TextStyle h2Secondary = TextStyle(
  fontSize: h2FontSize,
  color: secondaryColor,
  fontWeight: FontWeight.bold,
  
);
TextStyle h2White = TextStyle(
  fontSize: h2FontSize,
  color: Colors.white,
  fontWeight: FontWeight.bold,
  
);

TextStyle h2Black = TextStyle(
  fontSize: h2FontSize,
  color: Colors.black,
  fontWeight: FontWeight.bold,
  
);

TextStyle h_4White = TextStyle(
  fontSize: h4FontSize,
  color: Colors.white,
  
  
);

TextStyle pWhite = TextStyle(
  fontSize: pFontSize,
  color: Colors.white,
  // fontWeight: FontWeight.bold,
  
);

TextStyle pBlack = TextStyle(
  fontSize: pFontSize,
  color: Colors.black,
  // fontWeight: FontWeight.300,
  
);
