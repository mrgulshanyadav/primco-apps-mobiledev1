import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

import 'package:primcoca/EventDetailScreen.dart';
import 'package:primcoca/ExpenseAllowance/BillReimbursementForm.dart';
import 'package:primcoca/SalesRepDashboardScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class BillsListScreen extends StatefulWidget {
  final String authorizationHeader;
  BillsListScreen({ Key key, this.authorizationHeader }): super(key: key);


  @override
  _BillsListScreenState createState() => _BillsListScreenState(authorizationHeader);
}

class _BillsListScreenState extends State<BillsListScreen> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  final String authorizationHeader;

  _BillsListScreenState( this.authorizationHeader );

  LocalStorage userData = new LocalStorage('userData');

  String currentUserId, role;
  String comment;

  getSharedPreferencesData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      currentUserId = prefs.getString('currentUserId');
      role = prefs.getString('role');
    });
    print('currentUserId1: '+currentUserId);
    print('role: '+role.toString());
  }

  @override
  void initState() {

    comment = '';
    getSharedPreferencesData();

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Lists of Bills',),
        backgroundColor: globals.secondaryColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.pop(context);
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => SalesRepDashboardScreen()));
          },
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
//            Container(margin: EdgeInsets.only(top: 20),child: Center(child: Text('List of Bills', style: globals.h1Black, textAlign: TextAlign.center,))),
            Container(
              child: FutureBuilder(
                future: role!='admin'? _getListOfBills(): _getListOfBillsAdmin(),
                builder: (context,res){

                  var responsedata = res.data;
                  print('responseData22: '+responsedata.toString());

                  if(!res.hasData){
                    return Center(child: CircularProgressIndicator());
                  }
                  else{

                    BillList billList = BillList.fromJson(jsonDecode(responsedata));

                    return Expanded(
                      child: ListView.builder(
                          itemCount: billList.response.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (context, index){

                          String id = billList.response[index].id.toString()??'0';
                          String amount = billList.response[index].billingAmount.toString()??'0';
                          String date = billList.response[index].billingDate.toString()??'NA';
                          String type_of_expense = billList.response[index].billingTypeOfExpense.toString()??'NA';
                          String city_of_expense = billList.response[index].billingPlaceOfExpense.toString()??'NA';
                          String tax = billList.response[index].billingTax.toString()??'0';
                          String tax_amount = billList.response[index].billingTaxAmount.toString()??'0';
                          String total_amount = billList.response[index].billingTotalAmount.toString()??'0';
                          String status = billList.response[index].billingStatus.toString();
                          String billingImage = billList.response[index].billingImage;
                          List<Comment> comments = billList.response[index].comment;

                          TextStyle textStyle = TextStyle(color: Colors.blue);

                            return Container(
                              padding: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                              margin: EdgeInsets.only(top: 15, left: 7, right: 7),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 1,
                                  color: Colors.black54,
                                )
                              ),
                              child: Stack(
                                alignment: Alignment.topRight,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('ID: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('Amount: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('Date: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('Type of Expense: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('City of Expense: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('Tax: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('Tax Amount: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('Total Amount: ', style: textStyle,),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text('Status: ', style: textStyle,),
                                            ],
                                          ),
                                          Visibility(
                                            visible: billingImage.length>3? true : false,
                                            child: Divider(
                                              color: Colors.grey,
                                              thickness: 0.5,
                                            ),
                                          ),
                                          Visibility(
                                            visible: billingImage.length>3? true : false,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Text('Image: ', style: textStyle,),
                                              ],
                                            ),
                                          ),
                                          Visibility(
                                            visible: comments.length>0? true : false,
                                            child: Divider(
                                              color: Colors.grey,
                                              thickness: 0.5,
                                            ),
                                          ),
                                          Visibility(
                                            visible: comments.length>0? true : false,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Text('Comment: ', style: textStyle,),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(id),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(amount),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(date),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(type_of_expense),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(city_of_expense),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(tax),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(tax_amount),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(total_amount),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey,
                                            thickness: 0.5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(status)
                                            ],
                                          ),
                                          Visibility(
                                            visible: billingImage.length>3? true : false,
                                            child: Divider(
                                              color: Colors.grey,
                                              thickness: 0.5,
                                            ),
                                          ),
                                          Visibility(
                                            visible: billingImage.length>3? true : false,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                GestureDetector(
                                                  child: Icon(Icons.visibility,size: 19,),
                                                  onTap: () async {
                                                    await showDialog(
                                                        context: context,
                                                        builder: (_) => ImageDialog(imageUrl: billingImage,)
                                                    );
                                                  },
                                                )
                                              ],
                                            ),
                                          ),
                                          Visibility(
                                            visible: comments.length>0? true : false,
                                            child: Divider(
                                              color: Colors.grey,
                                              thickness: 0.5,
                                            ),
                                          ),
                                          Visibility(
                                            visible: comments.length>0? true : false,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                GestureDetector(
                                                  child: Icon(Icons.visibility, size: 19,),
                                                  onTap: () {
//                                                  await showDialog(context: context, builder: (_) => CommentsDialog(comments: comments,));
                                                    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>CommentsDialog(comments: comments,)));
                                                  },
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  role != 'admin' ? Column(
                                    children: <Widget>[
                                      status!='A' ?  status!= 'P' ? IconButton(icon: Icon(Icons.edit), onPressed: (){
                                        // edit bill

                                        Response response = billList.response[index];
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=> BillReimbursementForm(responseObject: response,)));

                                      },): Container() : Container(),

                                    ],
                                  ) :
                                  status== 'P' ? Column(
                                    children: <Widget>[
                                      FlatButton(child: Text('Approve'), onPressed: (){
                                        //  approve bill

                                        _editBill(id, 'A', '');
                                        Navigator.pop(context);

                                      },),
                                      FlatButton(child: Text('Decline'), onPressed: (){
                                        //  reject bill

                                        showDialog(
                                            context: context,
                                            child: new AlertDialog(
                                              content: Container(
                                                height: 97,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  children: <Widget>[
                                                    TextFormField(
                                                      style: TextStyle(color: Colors.black),
                                                      decoration: InputDecoration(

                                                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                                        border: OutlineInputBorder(
                                                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                                          borderRadius: new BorderRadius.circular(22.0),
                                                        ),
                                                        focusedBorder:  OutlineInputBorder(
                                                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                                          borderRadius: new BorderRadius.circular(22.0),
                                                        ),
                                                        hintStyle: globals.inputTextLight,
                                                        hintText: 'Comment',
                                                        //labelText: 'E-mail',
                                                        //fillColor: Colors.white,
                                                        labelStyle: TextStyle(color: Colors.white),

                                                      ),
                                                      keyboardType: TextInputType.multiline,

                                                      onChanged: (val){
                                                        setState(() {
                                                          comment = val;
                                                        });
                                                      },

                                                    ),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                      children: <Widget>[
                                                        FlatButton(child: Text('Cancel'), onPressed: (){
                                                          Navigator.pop(context);
                                                        },),
                                                        FlatButton(child: Text('Ok'), onPressed: (){
                                                          _editBill(id, 'D', comment);
                                                          Navigator.pop(context);
                                                        },),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                        );

                                      },),
                                      IconButton(icon: Icon(Icons.delete), onPressed: (){
                                        // delete bill

                                        _deleteBill(id);
                                        Navigator.pop(context);
                                        Future.delayed(Duration(seconds: 2), () {
                                          Navigator.of(context).push(
                                              new MaterialPageRoute(
                                                  builder: (context) =>
                                                      BillsListScreen()
                                              ));
                                        }
                                        );

                                      },),
                                    ],
                                  ) : Column(
                                    children: <Widget>[
                                      IconButton(icon: Icon(Icons.delete), onPressed: (){
                                        // delete bill

                                        _deleteBill(id);
                                        Navigator.pop(context);
                                        Future.delayed(Duration(seconds: 2), () {
                                          Navigator.of(context).push(
                                              new MaterialPageRoute(
                                                  builder: (context) =>
                                                      BillsListScreen()
                                              ));
                                          }
                                        );

                                      },),
                                    ],
                                  ),
                                ],
                              ),
                            );

                          }
                      ),
                    );
                  }

                },

              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<dynamic> _getListOfBills() async {

    final LocalStorage userDataStorage = new LocalStorage('userData');

    var authorizationHeader = await userDataStorage.getItem('authorizationHeader');
    var httpHeader = {
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
    };

    print('currentUserId2: '+currentUserId);

    var response = await http.post( globals.baseURL+'bill/billlist/'+currentUserId , headers: httpHeader).then((http.Response response) {

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        showInSnackBar("Please check form data.");
        return false;
      }else {
        return response.body;
      }

    });

    return response.toString();

  }

  Future<dynamic> _getListOfBillsAdmin() async {

    final LocalStorage userDataStorage = new LocalStorage('userData');

    var authorizationHeader = await userDataStorage.getItem('authorizationHeader');
    var httpHeader = {
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
    };

    print('currentUserId2: '+currentUserId);

    var response = await http.post( globals.baseURL+'bill/billlist/' , headers: httpHeader).then((http.Response response) {

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        showInSnackBar("Please check form data.");
        return false;
      }else {
        return response.body;
      }

    });

    return response.toString();

  }

  void _editBill(String id,String billing_status, String billing_comment) async {

    final LocalStorage userDataStorage = new LocalStorage('userData');

    var body = {
      'billing_comment': billing_comment,
      'billing_status': billing_status,
    };

    var authorizationHeader = await userDataStorage.getItem('authorizationHeader');
    var httpHeader = {
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
    };

    http.post( globals.baseURL+'bill/edit/'+id , body: body, headers: httpHeader).then((http.Response response) {

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        showInSnackBar("Please check form data.");
        print("Please check form data: reponse: "+response.statusCode.toString());
        return;
      }else{
        showInSnackBar("Updated.");
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BillsListScreen()));
      }

      var responseData = json.decode(response.body);
//      showInSnackBar(responseData['response']);
      print(responseData.toString());

    });

//    showInSnackBar( 'Please wait! Your form is being submitted..' );

  }

  void _deleteBill(String id) async {

    final LocalStorage userDataStorage = new LocalStorage('userData');

    var authorizationHeader = await userDataStorage.getItem('authorizationHeader');
    var httpHeader = {
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
    };

    http.post( globals.baseURL+'bill/delete/'+id , headers: httpHeader).then((http.Response response) {

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        showInSnackBar("Please check form data.");
        print("Please check form data: reponse: "+response.statusCode.toString());
        return;
      }else{
        showInSnackBar("Updated.");
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BillsListScreen()));
      }

      var responseData = json.decode(response.body);
//      showInSnackBar(responseData['response']);
      print(responseData.toString());

    });

//    showInSnackBar( 'Please wait! Your form is being submitted..' );

  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value)
    ));
  }

}

BillList billListFromJson(String str) => BillList.fromJson(json.decode(str));

String billListToJson(BillList data) => json.encode(data.toJson());

class BillList {
  List<Response> response;

  BillList({
    this.response,
  });

  factory BillList.fromJson(Map<String, dynamic> json) => BillList(
    response: List<Response>.from(json["response"].map((x) => Response.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "response": List<dynamic>.from(response.map((x) => x.toJson())),
  };
}

class Response {
  int id;
  String billingDate;
  String billingTypeOfExpense;
  String billingPlaceOfExpense;
  int billingAmount;
  String billingTax;
  int billingTaxAmount;
  int billingTotalAmount;
  String billingImage;
  String billingStatus;
  int billingUserid;
  String billingComment;
  DateTime created;
  List<Comment> comment;

  Response({
    this.id,
    this.billingDate,
    this.billingTypeOfExpense,
    this.billingPlaceOfExpense,
    this.billingAmount,
    this.billingTax,
    this.billingTaxAmount,
    this.billingTotalAmount,
    this.billingImage,
    this.billingStatus,
    this.billingUserid,
    this.billingComment,
    this.created,
    this.comment,
  });

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    id: json["id"],
    billingDate: json["billing_date"],
    billingTypeOfExpense: json["billing_type_of_expense"],
    billingPlaceOfExpense: json["billing_place_of_expense"],
    billingAmount: json["billing_amount"],
    billingTax: json["billing_tax"],
    billingTaxAmount: json["billing_tax_amount"],
    billingTotalAmount: json["billing_total_amount"],
    billingImage: json["billing_image"],
    billingStatus: json["billing_status"],
    billingUserid: json["billing_userid"],
    billingComment: json["billing_comment"] == null ? null : json["billing_comment"],
    created: DateTime.parse(json["created"]),
    comment: List<Comment>.from(json["comment"].map((x) => Comment.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "billing_date": billingDate,
    "billing_type_of_expense": billingTypeOfExpense,
    "billing_place_of_expense": billingPlaceOfExpense,
    "billing_amount": billingAmount,
    "billing_tax": billingTax,
    "billing_tax_amount": billingTaxAmount,
    "billing_total_amount": billingTotalAmount,
    "billing_image": billingImage,
    "billing_status": billingStatus,
    "billing_userid": billingUserid,
    "billing_comment": billingComment == null ? null : billingComment,
    "created": created.toIso8601String(),
    "comment": List<dynamic>.from(comment.map((x) => x.toJson())),
  };
}

class Comment {
  int id;
  String comment;
  int billid;
  DateTime created;

  Comment({
    this.id,
    this.comment,
    this.billid,
    this.created,
  });

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
    id: json["id"],
    comment: json["comment"],
    billid: json["billid"],
    created: DateTime.parse(json["created"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "comment": comment,
    "billid": billid,
    "created": created.toIso8601String(),
  };
}



class ImageDialog extends StatelessWidget {
  String imageUrl;
  ImageDialog({this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        width: MediaQuery.of(context).size.width-50,
        height: MediaQuery.of(context).size.height-50,
        child: Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Center(child: Image.network('http://orders.primco.ca/bill_image/'+imageUrl, fit: BoxFit.fill,)),
            ),
            IconButton(
              icon: Icon(Icons.close),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class CommentsDialog extends StatelessWidget {
  List<Comment> comments;
  CommentsDialog({this.comments});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Comments'),),
      body: SafeArea(
        child: ListView.builder(
            itemCount: comments.length,
            itemBuilder: (context,index){

              return Container(
                padding: EdgeInsets.all(15),
                margin: EdgeInsets.all(10),
                color: Colors.grey[300],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Date: '+comments[index].created.toString()),
                    Text('Comment: '+comments[index].comment),
                  ],
                ),
              );
            }
        ),
      ),
    );
  }
}