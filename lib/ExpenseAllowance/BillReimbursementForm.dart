import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:primcoca/ExpenseAllowance/BillsListScreen.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

import '../GlobalVariables.dart';

class BillReimbursementForm extends StatefulWidget {
  final Response responseObject;
  BillReimbursementForm({Key key, this.responseObject}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _BillReimbursementFormState();
}

class _BillReimbursementFormState extends State<BillReimbursementForm> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _totalTaxEditingController = new TextEditingController();

  List<String> typeOfExpenseList = ['Auto Allowance/Car Expenses', 'Airfare', 'Accommodation', 'Meals & Entertainment', 'Meal Allowance', 'Other Travel expenses', 'Miscellaneous (supplies, internet, etc.)'];
  List<String> typeOfTaxList = ['GST', 'PST'];

  String date_of_expense, place_of_expense, type_of_expense, type_of_tax;
  int amount, tax_amount, total_amount;

  File _image;
  String image_url;

  Future getImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
                height: _image!=null? 190 : 135,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        "Take a Picture",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(source: ImageSource.camera, maxHeight: 480, maxWidth: 640, imageQuality: 50);
                        setState(() {
                          _image = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    FlatButton(
                      child: Text(
                        "Pick from Gallery",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(
                            source: ImageSource.gallery, maxHeight: 480, maxWidth: 640, imageQuality: 50);
                        setState(() {
                          _image = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    _image != null
                        ? Divider(
                      thickness: 2,
                    ) : Container(),
                    _image != null
                        ? FlatButton(
                      child: Text(
                        "Remove Profile",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        setState(() {
                          _image = null;
                          Navigator.pop(context);
                        });
                      },
                    )
                        : Text("",),
                  ],
                ),
              ));
        });
  }

  DateTime _date = DateTime.now();

  Future<DateTime> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(context: context, initialDate: _date, firstDate: DateTime(1900), lastDate: DateTime(2100));

    if(picked!=null){
      setState(() {
        _date = picked;
        date_of_expense = DateFormat('yyyy-MM-dd').format(_date);
      });
    }

  }

  String currentUserId;
  bool isLoading;

  getSharedPreferencesData()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      currentUserId = prefs.getString('currentUserId');
    });
  }

  @override
  void initState() {

    getSharedPreferencesData();

    if(widget.responseObject!=null){
      date_of_expense = widget.responseObject.billingDate;
      type_of_expense = widget.responseObject.billingTypeOfExpense;
      place_of_expense = widget.responseObject.billingPlaceOfExpense;
      type_of_tax = widget.responseObject.billingTax;
      amount = widget.responseObject.billingAmount??0;
      tax_amount = widget.responseObject.billingTaxAmount??0;
      total_amount = widget.responseObject.billingTotalAmount??0;
      image_url = 'http://orders.primco.ca/bill_image/'+widget.responseObject.billingImage;
      _totalTaxEditingController.text = total_amount.toString();

      print('date_of_expense: '+date_of_expense);
      print('type_of_expense: '+type_of_expense);
      print('place_of_expense: '+place_of_expense);
      print('type_of_tax: '+type_of_tax);
      print('amount: '+amount.toString());
      print('tax_amount: '+tax_amount.toString());
      print('total_amount: '+total_amount.toString());
      print('image_url: '+image_url.toString());

    }else{
      amount = 0;
      tax_amount = 0;
      total_amount = 0;
      _totalTaxEditingController.text = '';
    }

    isLoading = false;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
        fontFamily: "Encode Sans", //my custom font
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: scaffoldKey,
//        backgroundColor: globals.primaryColorOpac,
        appBar: AppBar(
          title: Text('Bill Reimbursement Form',),
          backgroundColor: globals.secondaryColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ),
        body: SafeArea(
          child: Container(
            child: ListView(
              children: <Widget>[
//                const SizedBox(height: 20.0),
//                Center(child: Text('Bill Reimbursement Form', style: globals.h1Secondary, textAlign: TextAlign.center)),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      const SizedBox(height: 20.0),
                      GestureDetector(
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(top: 3, bottom: 3, left: 3, right: 15),
                            decoration: BoxDecoration(
                                borderRadius: new BorderRadius.circular(22.0),
                                border: Border.all(color: Colors.black54, style: BorderStyle.solid),
                            ),
                            child: Container(
                                padding: const EdgeInsets.symmetric( vertical: 13, horizontal: 20),
                                child: Text(date_of_expense!=null? date_of_expense:"Date of Expense",
                                  style: TextStyle(color: Colors.black),
                                )
                            ),
                        ),
                        onTap: (){
                          selectDate(context);
                        },
                      ),
                      const SizedBox(height: 20.0),
                      TextFormField(
                        initialValue: place_of_expense,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          focusedBorder:  OutlineInputBorder(
                            borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          hintText: 'City of expense',
                          labelStyle: TextStyle(color: Colors.black54),
                          hintStyle: globals.inputTextLight,
                        ),
                        keyboardType: TextInputType.text,
                        onChanged: (val){
                          setState(() {
                            place_of_expense = val;
                          });
                        },
                      ),
                      const SizedBox(height: 20.0),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(left: 3, right: 15),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(33.0)),
                              border: Border.all(color: Colors.black54, width: 1)
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: type_of_expense,
                              hint: Container(padding: EdgeInsets.only(left: 15),child: Text("Type of Expense", style: TextStyle(color: Colors.black), )),
                              icon: Icon(Icons.keyboard_arrow_down, color: Colors.black,),
                              iconSize: 24,
                              onChanged: (String newValue) {
                                setState(() {
                                  type_of_expense = newValue;
                                });
                              },
                              items: typeOfExpenseList.map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(padding: EdgeInsets.only(left: 15),child: Text(value, style: TextStyle(color: Colors.black),)),
                                );
                              }).toList(),

                            ),
                          )
                      ),
                      const SizedBox(height: 20.0),
                      TextFormField(
                        initialValue: widget.responseObject==null? '' : amount.toString(),
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(

                          contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          focusedBorder:  OutlineInputBorder(
                            borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          hintStyle: globals.inputTextLight,
                          hintText: 'Amount',
                          //labelText: 'E-mail',
                          //fillColor: Colors.white,
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                        keyboardType: TextInputType.number,

                        onChanged: (val){
                          setState(() {
                            if(val.isNotEmpty) {
                              amount = int.parse(val);
                              int total = amount + tax_amount;
                              _totalTaxEditingController.text = (total).toString();
                            }else {
                                amount = 0;
                                _totalTaxEditingController.text = tax_amount.toString();
                                if(amount==0 && tax_amount==0){
                                  _totalTaxEditingController.text = '0';
                                }
                            }
                          });
                        },

                      ),
                      const SizedBox(height: 20.0),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(left: 3, right: 15),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(33.0)),
                              border: Border.all(color: Colors.black54, width: 1)
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: type_of_tax,
                              hint: Container(padding: EdgeInsets.only(left: 15),child: Text("Type of Tax", style: TextStyle(color: Colors.black), )),
                              icon: Icon(Icons.keyboard_arrow_down, color: Colors.white,),
                              iconSize: 24,
                              onChanged: (String newValue) {
                                setState(() {
                                  type_of_tax = newValue;
                                });
                              },
                              items: typeOfTaxList.map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(padding: EdgeInsets.only(left: 15),child: Text(value, style: TextStyle(color: Colors.black), )),
                                );
                              }).toList(),

                            ),
                          )
                      ),
                      const SizedBox(height: 20.0),
                      TextFormField(
                        initialValue: widget.responseObject==null? '' : tax_amount.toString(),
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(

                          contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          focusedBorder:  OutlineInputBorder(
                            borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          hintStyle: globals.inputTextLight,
                          hintText: 'Tax Amount',
                          //labelText: 'E-mail',
                          //fillColor: Colors.white,
                          labelStyle: TextStyle(color: Colors.white),

                        ),
                        keyboardType: TextInputType.number,

                        onChanged: (val){
                          setState(() {
                            if(val.isNotEmpty) {
                              tax_amount = int.parse(val);
                              int total = amount + tax_amount;
                              _totalTaxEditingController.text = (total).toString();
                            }
                            else {
                              tax_amount = 0;
                                _totalTaxEditingController.text = amount.toString();

                                if(amount==0 && tax_amount==0){
                                  _totalTaxEditingController.text = '0';
                                }
                            }


                          });
                        },

                      ),
                      const SizedBox(height: 20.0),
                      FocusScope(
                        canRequestFocus: false,
                        child: TextFormField(
                         // initialValue: widget.responseObject==null? '' : total_amount.toString(),
                          style: TextStyle(color: Colors.black),
                          controller: _totalTaxEditingController,
                          decoration: InputDecoration(

                            contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            focusedBorder:  OutlineInputBorder(
                              borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            hintStyle: globals.inputTextLight,
                            hintText: 'Total Amount',
                            //labelText: 'E-mail',
                            //fillColor: Colors.white,
                            labelStyle: TextStyle(color: Colors.white),

                          ),
                          keyboardType: TextInputType.number,

                          onChanged: (val){
                            setState(() {
                              total_amount = int.parse(val);
                              total_amount = tax_amount+amount;
                            });
                          },

                        ),
                      ),
                      const SizedBox(height: 20.0),
                      Center(
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            border: Border.all(style: BorderStyle.solid, width: 1, color: Colors.black54),
                          ),
                          child:_image!=null? GestureDetector(onTap: ()=> getImage(context), child: Image.file(_image, fit: BoxFit.fill)) : image_url!=null? GestureDetector(onTap: ()=> getImage(context) ,child: Image.network(image_url, fit: BoxFit.fill,)) : Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: Text('Upload Photo', style: TextStyle(color: Colors.black54), textAlign: TextAlign.center,),
                                onPressed: (){
                                  getImage(context);
                                },
                              )
                            ],
                          ),

                        ),
                      ),
                      _image !=null? _image.lengthSync() > 2000000 ?
                      Text('Maximum image size should not be greater than 2mb', style: TextStyle(color: Colors.red),) : Container() : Container(),
                    ],
                  ),
                ),
                const SizedBox(height: 20.0),
                !isLoading ? Center(
                  child: ButtonTheme(
                    // minWidth: MediaQuery.of(context).size * .90,
                    minWidth: MediaQuery.of(context).size.width * .90,
                    height: 45.0,
                    child: RaisedButton(
                      //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                      child: Text( 'Save as Draft', style: globals.bigButtonWhite ),
                      color: globals.secondaryColor,
                      textColor: Colors.white,
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                      onPressed: (){
                        _submit('S');
//                        Navigator.pop(context);
                      },

                    ),
                  ),
                ) : Container(),
                const SizedBox(height: 20.0),
                !isLoading? Visibility(
                  visible: widget.responseObject==null? true: false,
                  child: Center(
                    child: ButtonTheme(
                      // minWidth: MediaQuery.of(context).size * .90,
                      minWidth: MediaQuery.of(context).size.width * .90,
                      height: 45.0,
                      child: RaisedButton(
                        //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                        child: Text( 'Submit for Approval', style: globals.bigButtonWhite ),
                        color: globals.secondaryColor,
                        textColor: Colors.white,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                        onPressed: (){
                          _submit('P');
//                          Navigator.pop(context);
                        },

                      ),
                    ),
                  ),
                ) : Container(),
                !isLoading? Visibility(
                  visible: widget.responseObject!=null? true: false,
                  child: Center(
                    child: ButtonTheme(
                      // minWidth: MediaQuery.of(context).size * .90,
                      minWidth: MediaQuery.of(context).size.width * .90,
                      height: 45.0,
                      child: RaisedButton(
                        //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                        child: Text( 'Update & Submit', style: globals.bigButtonWhite ),
                        color: globals.secondaryColor,
                        textColor: Colors.white,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                        onPressed: (){
                          _edit('P');
//                          Navigator.pop(context);
                        },

                      ),
                    ),
                  ),
                ) : Container(),
                isLoading ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(child: CircularProgressIndicator()),
                ) : Container(),
                const SizedBox(height: 20.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _submit(String billing_status) async{

    final LocalStorage userDataStorage = new LocalStorage('userData');
    print('billing_image: ' + 'data:image/png;base64,' + base64Encode(_image.readAsBytesSync()));

    var body = {
      'billing_date': date_of_expense,
      'billing_type_of_expense': type_of_expense,
      'billing_place_of_expense': place_of_expense,
      'billing_amount': amount.toString(),
      'billing_tax': type_of_tax,
      'billing_tax_amount': tax_amount.toString(),
      'billing_total_amount': _totalTaxEditingController.text,
      'billing_userid': currentUserId ,
      'billing_image': _image != null ? 'data:image/png;base64,' + base64Encode(_image.readAsBytesSync()) : '',
      'billing_status': billing_status,
    };

    var authorizationHeader = await userDataStorage.getItem('authorizationHeader');
    var httpHeader = {
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
    };

    http.post( globals.baseURL+'bill/add.json' , body: body, headers: httpHeader).then((http.Response response) {

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        print('status: '+statusCode.toString()); //
        var responseData = json.decode(response.body);
        try {
          showInSnackBar('status: '+statusCode.toString()+','+ responseData['message'].toString());
          print('responseData: ' + responseData['message'].toString());
        }catch(e){ print('e: '+e.toString()); }
        setState(() {
          isLoading = false;
        });

        return;
      }

      var responseData = json.decode(response.body);
      showInSnackBar(responseData['response']);
      Navigator.pop(context);
      print(responseData.toString());

      setState(() {
        isLoading = false;
      });

    });

    setState(() {
      isLoading = true;
    });

    if(isLoading)
      showInSnackBar( 'Please wait! Your form is being submitted..' );

  }

  void _edit(String billing_status) async {

    final LocalStorage userDataStorage = new LocalStorage('userData');

    var body = {
      'billing_date': date_of_expense.split(" ")[0],
      'billing_type_of_expense': type_of_expense,
      'billing_place_of_expense': place_of_expense,
      'billing_amount': amount.toString(),
      'billing_tax': type_of_tax,
      'billing_tax_amount': tax_amount.toString(),
      'billing_total_amount': _totalTaxEditingController.text,
      'billing_userid': currentUserId ,
      'billing_image': _image != null ? 'data:image/png;base64,' + base64Encode(_image.readAsBytesSync()) : '',
      'billing_status': billing_status,
    };

    var authorizationHeader = await userDataStorage.getItem('authorizationHeader');
    var httpHeader = {
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
    };

    http.post(globals.baseURL+'bill/edit/'+widget.responseObject.id.toString() , body: body, headers: httpHeader).then((http.Response response) {

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        print('status: '+statusCode.toString());
        var responseData = json.decode(response.body);
        try {
          showInSnackBar('status: '+statusCode.toString()+','+ responseData['message'].toString());
          print('responseData: ' + responseData['message'].toString());
        }catch(e){ print('e: '+e.toString()); }
        setState(() {
          isLoading = false;
        });

        return;
      }else{
        showInSnackBar("Updated.");
        Navigator.pop(context);
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BillsListScreen()));
        setState(() {
          isLoading = false;
        });

      }

      var responseData = json.decode(response.body);
      showInSnackBar(responseData['response']);
      Navigator.pop(context);
      print(responseData.toString());
      setState(() {
        isLoading = false;
      });

    });

    setState(() {
      isLoading = true;
    });

    if(isLoading)
      showInSnackBar( 'Please wait! Your form is being submitted..' );

  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value),
    ));
  }

}
