import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import "package:autocomplete_textfield/autocomplete_textfield.dart";
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'blocs/product_bloc.dart';
import 'package:primcoca/AddOrderScreenProductSection.dart';
import 'package:primcoca/customer_list.dart';

class AddOrderScreenSalesmenSection extends StatefulWidget {
  
  final MainBloc mainBloc;
  AddOrderScreenSalesmenSection({ Key key, this.mainBloc }) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _AddOrderScreenSalesmenSectionState( mainBloc );

}

class _AddOrderScreenSalesmenSectionState extends State<AddOrderScreenSalesmenSection> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  MainBloc mainBloc;
  _AddOrderScreenSalesmenSectionState( this.mainBloc );

  GlobalKey<AutoCompleteTextFieldState<Salesmen>> key = new GlobalKey();
  AutoCompleteTextField searchTextField;
  TextEditingController controller = new TextEditingController(); 

  TextEditingController email = new TextEditingController();
  bool dataLoaded = false;
  

  String dealerName = "";
  final format = DateFormat("yyyy-MM-dd");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadData();
    //mainBloc = MainBloc();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    //mainBloc.dispose();
    super.dispose();
  }

  void _loadData() async {
    await SalesmenViewModel.loadPlayers().then((onValue){
      setState(() {
        dataLoaded = true;  
      });
      
    });
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
      ));
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Add Order: Sales Section',),
        backgroundColor: globals.secondaryColor,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: (){
              setState(() {
                
              });
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: 
        dataLoaded == false ? Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(

                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Color.fromRGBO(255, 255, 255, 0.5),
                  child: Column(
                    children: <Widget>[
                      CircularProgressIndicator(),
                      SizedBox(
                        height: 30,
                      ),
                      Text('Loading Data')
                    ],
                  ),
                ),
              
              
            ]
          ),
        ) :
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Sales Section', style: globals.h1Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,

            ),
            Form(
              key: formKey,
              autovalidate: true,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: searchTextField = AutoCompleteTextField<Salesmen>(
                          //style: new TextStyle(color: Colors.black, fontSize: 16.0),
                          controller: controller,
                          decoration: new InputDecoration(
                            contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            focusedBorder:  OutlineInputBorder(
                              borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            //icon: Icon(Icons.lock,color: Colors.black38,),
                            hintText: 'Sales Representative Name',
                            hintStyle: globals.inputTextLight,
                            labelText: 'Sales Representative Name',
                            labelStyle: globals.inputTextLight,
                          ),
                          itemSubmitted: (item) {
                            
                            
                            setState(() {
                              email.text = item.repEmail;
                              mainBloc.setRepId( this, item.id );
                              searchTextField.textField.controller.text = item.autocompleteterm;
                            });
                            //print(mainBloc.address);
                            
                          },
                          clearOnSubmit: false,
                          key: key,
                          suggestions: SalesmenViewModel.salesmen,
                          itemBuilder: (context, item) {
                            //print(item.autocompleteterm);
                            var _name = item.autocompleteterm??'';
                            var _id = item.id??'';
                            var customerName = _name +':'+ _id;
                            return Text(
                                  '  '+customerName, 
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    height: 2.5
                                  ),
                                );
                          },
                          itemSorter: (a, b) {
                            return a.autocompleteterm.compareTo(b.autocompleteterm);
                          },
                          itemFilter: (item, query) {
                            return item.autocompleteterm
                                .toLowerCase()
                                .contains(query.toLowerCase());
                          }),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: email,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Sale Representative Email *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Sale Representative Email *',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setRepEmail(this, val);
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            initialValue: '',
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Event',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Event',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setSalesRepEvent(this, val);
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            initialValue: '',
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Sale Order #',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Sale Order #',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setSalesOrderId(this, val);
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            initialValue: '',
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Incentive acknowledgement',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Incentive acknowledgement',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setIncentiveAck(this, val);
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            initialValue: '',
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Purchaser\'s Name',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Purchaser\'s Name',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setPurchaserName(this, val);
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: DateTimeField(
                            format: format,
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2100));
                            },
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Sale Date',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Sale Date',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              mainBloc.setSalesDate( this, val );
                            }
                          ),
                        ),
                      ),
                      
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: ButtonTheme(
                          // minWidth: MediaQuery.of(context).size * .90,
                          minWidth: MediaQuery.of(context).size.width * .90,
                          height: 45.0,
                          child: RaisedButton( 
                            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
                            color: globals.secondaryColor,
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                            onPressed: (){
                              
                              if( searchTextField.textField.controller.text == "" ) {
                                
                                showInSnackBar("Can't proceed without any sales representative. Please select a sales representative from the list.");
                                
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    // return object of type Dialog
                                    return AlertDialog(
                                      title: new Text("Error"),
                                      content: new Text("Can't proceed without any sales representative. Please select a sales representative from the list."),
                                      actions: <Widget>[
                                        // usually buttons at the bottom of the dialog
                                        new FlatButton(
                                          child: new Text("Close"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              } else {
                                mainBloc.orderItems.clear();
                                Navigator.push(
                                  context, 
                                  MaterialPageRoute( builder: (context) => AddOrderScreenProductSection( mainBloc: mainBloc)  )
                                );
                              }
                              
                              
                            },

                          ),
                        ),
                      )
                    ],
                  )
                  
                ],
              ),
            ),
            
          ],
        )
      )
    );
  }
}

