import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:email_validator/email_validator.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/MeetingThankYouLoggedInScreen.dart';
import 'blocs/product_bloc.dart';


class SalesRepMeetingScreen extends StatefulWidget {
  final MainBloc mainBloc;
  SalesRepMeetingScreen({ Key key, this.mainBloc }): super(key: key);
  @override
  State<StatefulWidget> createState() => new _SalesRepMeetingScreenState();

}

class _SalesRepMeetingScreenState extends State<SalesRepMeetingScreen> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  int _selectedGender = 0;
  final LocalStorage userDataStorage = new LocalStorage('userData');
  bool hasError = true;

  TextEditingController _date = new TextEditingController();
  TextEditingController agenda = new TextEditingController();
  TextEditingController description = new TextEditingController();
  TextEditingController email = new TextEditingController();

  final format = DateFormat("yyyy-MM-dd kk:mm");

  //final MainBloc mainBloc;
  _SalesRepMeetingScreenState();

  var formFieldsColumn = List<Widget>();
  int count = 0;

  List<DropdownMenuItem<int>> genderList = [];

  List<String> _province = <String>['', 'Saskatoon', 'Alberta', 'Manitoba'];
  String _selectProvince = '';

  List<String> _salesMen = <String>[];
  String _selectedSalesmen = '';
  DropdownButton salesMenDD;

  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // mainBloc.setGrandTotal(this, 0.0, 0.0, 0.0, 0.0);
    
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message)
    ));
    
  }
  
  generateSalesmenDropdown(){
    
    setState(() {
      
      salesMenDD = null;
      _selectedSalesmen = '';
      salesMenDD = DropdownButton(
        value: _selectedSalesmen,
        isDense: true,
        onChanged: ( newValue) {
          setState(() {
            _selectedSalesmen = newValue;
          });
        },
        items: _salesMen.map((String value) {
          return new DropdownMenuItem(
            value: value,
            child: new Text(value),
          );
        }).toList(),
      );
    });
  }

  initSalesmenDropdown(){
    salesMenDD = DropdownButton(
      value: _selectedSalesmen,
      isDense: true,
      onChanged: ( newValue) {
        setState(() {
          _selectedSalesmen = newValue;
        });
      },
      items: _salesMen.map((String value) {
        return new DropdownMenuItem(
          value: value,
          child: new Text(value),
        );
      }).toList(),
    ); 
  }
  
  @override
  Widget build(BuildContext context) {
    initSalesmenDropdown();
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Schedule Meeting',),
        backgroundColor: globals.secondaryColor,
      ),
      key: scaffoldKey,
      persistentFooterButtons: <Widget>[
        ButtonTheme(
          // minWidth: MediaQuery.of(context).size * .90,
          minWidth: MediaQuery.of(context).size.width * .90,
          height: 45.0,
          child: RaisedButton( 
            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
            child: Text( 'Schedule', style: globals.bigButtonWhite ),
            color: globals.secondaryColor,
            textColor: Colors.white,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
            onPressed: (){

              if( hasError || ( agenda.text == "" || description.text == "" || email.text == "" || _date.text == "" ) ) {
                showInSnackBar('Please fill all details.');  
                return;
              }
              showInSnackBar('Sending request');
              var userData = userDataStorage.getItem('userData' );
              // userDataStorage.setItem('authorizationHeader',authorizationHeader );
              print(userData);
              var postData = { 
                'salesman_id': userData['id'].toString(),
                'agenda': agenda.text,
                'description': description.text,
                'date_time': _date.text,
                'email': email.text,
                'is_customer_meeting': '0'
              };
              
              http.post( globals.baseURL+'meetings/addmeeting.json', 
                body: postData
              ).then((response){
                final int statusCode = response.statusCode;
                debugPrint(response.body);
                if (statusCode < 200 || statusCode > 400 ) {
                  throw new Exception("Error while fetching data");
                }
                
                var responsData = json.decode(response.body);
                
                //print(responsData);
                if( responsData['status'] == 1 ) {
                  print(responsData);
                  print('success');
                  showInSnackBar('Meeting saved. Redirecting');
                  Navigator.pushReplacement(
                    context, 
                    MaterialPageRoute( builder: (context) => MeetingThankYouLoggedInScreen() )
                  );
                } else {
                  print(responsData);
                  showInSnackBar('Sorry! we were unable to schedule the meeting. Please try again later.');
                }
              });

            },

          ),
        )
      ],
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: Form(
          key: formKey,
          autovalidate: true,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                'Schedule Meeting', style: globals.h1Secondary, textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 40,
              ),
              
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: TextFormField( 
                      controller: agenda,
                      style: globals.inputText,
                      decoration: InputDecoration(
                        
                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        //icon: Icon(Icons.lock,color: Colors.black38,),
                        hintText: 'Agenda',
                        hintStyle: globals.inputTextLight,
                        labelText: 'Agenda',
                        labelStyle: globals.inputTextLight,
                      ),
                      validator: ( value ){
                        if (value.isEmpty) {
                          return 'Field is required.';
                        } 
                        // return '';
                      }
                    ),
                  ),
                  Expanded(
                    child: TextFormField( 
                      controller: description,
                      style: globals.inputText,
                      decoration: InputDecoration(
                          
                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        //icon: Icon(Icons.lock,color: Colors.black38,),
                        hintText: 'Description',
                        hintStyle: globals.inputTextLight,
                        labelText: 'Description',
                        labelStyle: globals.inputTextLight,
                      ),
                      validator: ( value ){
                        if (value.isEmpty) {
                          return 'Field is required.';
                        }
                        
                      }
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: TextFormField( 
                      controller: email,
                      style: globals.inputText,
                      decoration: InputDecoration(
                          
                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        //icon: Icon(Icons.lock,color: Colors.black38,),
                        hintText: 'Your Email',
                        hintStyle: globals.inputTextLight,
                        labelText: 'Your Email',
                        labelStyle: globals.inputTextLight,
                      ),
                      validator: ( value ){

                        if( !EmailValidator.validate(value, true) ){
                          hasError = true;
                          return 'Not a valid email.';

                        } else {
                          hasError = false;
                        }

                        if (value.isEmpty) {
                          return 'Field is required.';
                        }
                      }
                    ),
                  ),
                  Expanded(
                    child: DateTimeField(
                      controller: _date,
                      format: format,
                      onShowPicker: (context, currentValue) async {
                        final date = await showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100));
                        if (date != null) {
                          final time = await showTimePicker(
                            context: context,
                            initialTime:
                                TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                          );
                          return DateTimeField.combine(date, time);
                        } else {
                          return currentValue;
                        }
                      },
                      decoration: InputDecoration(
                          
                          contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          focusedBorder:  OutlineInputBorder(
                            borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          //icon: Icon(Icons.lock,color: Colors.black38,),
                          hintText: 'Submission Date',
                          hintStyle: globals.inputTextLight,
                          labelText: 'Submission Date',
                          labelStyle: globals.inputTextLight,
                      ),
                      onChanged: (val){
                        
                        //mainBloc.setSalesDate( this, val );
                      }
                    ),
                  ),
                  
                ],
              )
              
            ],
          )
        
        )
        
      )
    );
  }
}
