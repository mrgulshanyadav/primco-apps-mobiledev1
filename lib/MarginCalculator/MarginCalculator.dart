import 'dart:io';

import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:search_widget/search_widget.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import "package:autocomplete_textfield/autocomplete_textfield.dart";
import 'package:intl/intl.dart';

class MarginCalculator extends StatefulWidget {
  @override
  _MarginCalculatorState createState() => _MarginCalculatorState();
}

class _MarginCalculatorState extends State<MarginCalculator> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  String baseUrl;

  // Auto complete
  GlobalKey<AutoCompleteTextFieldState<Response>> key = new GlobalKey();
  AutoCompleteTextField searchTextField;
  String currentSearchText = "";
  TextEditingController controller = new TextEditingController();

  List<TextEditingController> _sellPriceController;
  List<TextEditingController> _rebateController;
  List<TextEditingController> _grossMarginController;

  Map<String,String> slabMap;
  List<Map<String,String>> slabList;
  String currency;

  TextEditingController _searchTextController;
  TextEditingController _conversionRateController;

  // for search widget
  List<Response> familiesResponseList;
  Response _selectedItem;

  // for search items
  double mill_cost;
  double conversion_rate;
  double new_mill_cost;
  double brokerage_multiplier;
  double brokerage;
  double freight;
  double ad_fund_percentage;
  double ad_fund;
  double landed_cost_per_unit;
  double load;
  double load_percentage;
  double marginlic;
  double sell_price_level1;
  double rebate_level1;
  double gross_margin_level1;
  double sell_price_level2;
  double rebate_level2;
  double gross_margin_level2;
  double sell_price_level3;
  double rebate_level3;
  double gross_margin_level3;

  double sell_price, rebate, margin_lic, brokragepersantage, brokrage, gross_margin_level;

  // new
  double _sellPrice, _rebate, _grossMargin, _conversionRate;

  List<String> currencyList = ['Default','Dollar'];

  bool isItemSelected;

  @override
  void initState() {
    super.initState();

    _searchTextController = new TextEditingController();
    _conversionRateController = new TextEditingController();

    currency = 'Default';

    // create new slab list
    slabList = new List<Map<String,String>>();
    // create new slab map
    slabMap = new Map();

    // used to create unique controllers for each slab items in list
    _sellPriceController = List<TextEditingController>();
    _rebateController = List<TextEditingController>();
    _grossMarginController = List<TextEditingController>();

    _sellPrice = 0.0;
    _grossMargin = 0.0;
    _rebate = 0.0;
    _conversionRate = 0.0;

    // used for storing search list items from api
    familiesResponseList = new List();

    isItemSelected = false;

  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text('Margin Calculator'),
          backgroundColor: Color.fromRGBO(8, 84, 94, 1),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(bottom: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        width: screenWidth/2,
                        padding: EdgeInsets.all(5),
                        child: FutureBuilder(
                          future: _getListOfFamilies(),
                          builder: (context, snapshot){

                            if(!snapshot.hasData){
                              return FocusScope(canRequestFocus: false, child: TextFormField(
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(22.0),
                                  ),
                                  focusedBorder:  OutlineInputBorder(
                                    borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                    borderRadius: new BorderRadius.circular(22.0),
                                  ),
                                  hintText: 'Loading..',
                                  labelStyle: TextStyle(color: Colors.black54),
                                  hintStyle: globals.inputTextLight,
                                ),
                                keyboardType: TextInputType.text,
                                onChanged: (val){
                                  setState(() {

                                  });
                                },
                              ),);
                            }
                            else{

                              GetFamiliesResponse getFamiliesResponse = GetFamiliesResponse.fromJson(jsonDecode(snapshot.data));

                              familiesResponseList = getFamiliesResponse.response;

                              print('familiesResponseList: '+familiesResponseList.toString());

                              return Container(
                                child: searchTextField = AutoCompleteTextField<Response>(
                                  //style: new TextStyle(color: Colors.black, fontSize: 16.0),
                                    controller: controller,
                                    decoration: new InputDecoration(
                                      contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                      border: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                        borderRadius: new BorderRadius.circular(22.0),
                                      ),
                                      focusedBorder:  OutlineInputBorder(
                                        borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                        borderRadius: new BorderRadius.circular(22.0),
                                      ),
                                      //icon: Icon(Icons.lock,color: Colors.black38,),
                                      hintText: 'Search Product Name or Family or Mill',
                                      hintStyle: globals.inputTextLight,
                                      labelText: 'Search Product Name or Family or Mill',
                                      labelStyle: globals.inputTextLight,
                                    ),
                                    itemSubmitted: (item) {
                                      searchTextField.textField.controller.text = item.name.toString();

                                      setState(() {
                                        _selectedItem = item;

                                        isItemSelected = true;

                                        _searchTextController.text = item.name;
                                        _conversionRateController.text = item.conversionRate.toString();
                                        _conversionRate = item.conversionRate;
                                        conversion_rate = item.conversionRate;

                                        sell_price = item.sellPriceLevel1;
                                        rebate = item.rebateLevel1;

                                        marginlic = item.millCost / _conversionRate;

                                        brokragepersantage = item.brokerageMultiplier;

                                        brokrage = brokragepersantage * marginlic;

                                        freight = item.freight;

                                        ad_fund = marginlic * brokrage;

                                        landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;

                                        load  =  landed_cost_per_unit * item.loadPercentage;

                                        margin_lic = landed_cost_per_unit +  load;

                                        gross_margin_level = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                        print('sell_price: '+sell_price.toString());
                                        print('rebate: '+rebate.toString());
                                        print('gross_margin_level: '+gross_margin_level.toString());

                                        print('sell_price1: '+item.sellPriceLevel1.toString());
                                        print('rebate1: '+item.rebateLevel1.toString());
                                        print('gross_margin_level1: '+item.grossMarginLevel1.toString());

                                        print('sell_price2: '+item.sellPriceLevel2.toString());
                                        print('rebate2: '+item.rebateLevel2.toString());
                                        print('gross_margin_level2: '+item.grossMarginLevel2.toString());

                                        print('sell_price3: '+item.sellPriceLevel3.toString());
                                        print('rebate3: '+item.rebateLevel3.toString());
                                        print('gross_margin_level3: '+item.grossMarginLevel3.toString());


                                        // create new list
                                        slabList = new List();

                                        // create slab 1
                                        slabMap = new Map();

                                        // add data to slab 1
                                        slabMap.putIfAbsent('slab_type', () => 'Slab 1');
                                        slabMap.putIfAbsent('sell_price', () => item.sellPriceLevel1.toString());
                                        slabMap.putIfAbsent('rebate', () => item.rebateLevel1.toString());
                                        slabMap.putIfAbsent('gross_margin', () => item.grossMarginLevel1.toString());
                                        slabMap.putIfAbsent('focusable', () => 'no');
                                        slabList.add(slabMap);

                                        // initialize controllers of slab 1
                                        _sellPriceController.add(TextEditingController());
                                        _rebateController.add(TextEditingController());
                                        _grossMarginController.add(TextEditingController());

                                        _sellPriceController[0].text = item.sellPriceLevel1.toString();
                                        _rebateController[0].text = item.rebateLevel1.toString();
                                        _grossMarginController[0].text = item.grossMarginLevel1.toString();

                                        // create slab 2
                                        slabMap = new Map();

                                        // add data to slab 2
                                        slabMap.putIfAbsent('slab_type', () => 'Slab 2');
                                        slabMap.putIfAbsent('sell_price', () => item.sellPriceLevel2.toString());
                                        slabMap.putIfAbsent('rebate', () => item.sellPriceLevel2.toString());
                                        slabMap.putIfAbsent('gross_margin', () => item.grossMarginLevel2.toString());
                                        slabMap.putIfAbsent('focusable', () => 'no');
                                        slabList.add(slabMap);

                                        // initialize controllers of slab 2
                                        _sellPriceController.add(TextEditingController());
                                        _rebateController.add(TextEditingController());
                                        _grossMarginController.add(TextEditingController());

                                        _sellPriceController[1].text = item.sellPriceLevel2.toString();
                                        _rebateController[1].text = item.rebateLevel2.toString();
                                        _grossMarginController[1].text = item.grossMarginLevel2.toString();

                                        // create slab 3
                                        slabMap = new Map();

                                        // add data to slab 3
                                        slabMap.putIfAbsent('slab_type', () => 'Slab 3');
                                        slabMap.putIfAbsent('sell_price', () => item.sellPriceLevel3.toString());
                                        slabMap.putIfAbsent('rebate', () => item.rebateLevel3.toString());
                                        slabMap.putIfAbsent('gross_margin', () => item.grossMarginLevel3.toString());
                                        slabMap.putIfAbsent('focusable', () => 'no');
                                        slabList.add(slabMap);

                                        // initialize controllers of slab 3
                                        _sellPriceController.add(TextEditingController());
                                        _rebateController.add(TextEditingController());
                                        _grossMarginController.add(TextEditingController());

                                        _sellPriceController[2].text = item.sellPriceLevel3.toString();
                                        _rebateController[2].text = item.rebateLevel3.toString();
                                        _grossMarginController[2].text = item.grossMarginLevel3.toString();


                                      });
                                      print('selected item: '+item.name);


                                    },
                                    clearOnSubmit: false,
                                    key: key,
                                    suggestions: familiesResponseList,
                                    itemBuilder: (context, item) {
                                      return Container(
                                        padding: const EdgeInsets.all(12),
                                        child: GestureDetector(
                                          child: Text(
                                            item.name.toString()??''+ ' ('+item.productCode.toString()??''+')',
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      );
                                    },
                                    itemSorter: (a, b) {
                                      return a.family.toString().compareTo(b.family.toString());
                                    },
                                    itemFilter: (item, query) {
                                      return (item.productCode).toString().toLowerCase().contains(query.toLowerCase()) ||
                                          (item.mill).toString().toLowerCase().contains(query.toLowerCase()) ||
                                          (item.name).toString().toLowerCase().contains(query.toLowerCase());
                                    }
                                 ),
                              );
                            }

                          },
                        ),
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width/2,
                          padding: EdgeInsets.only(left: 3, right: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(33.0)),
                              border: Border.all(color: Colors.black54, width: 1)
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: currency,
                              hint: Container(padding: EdgeInsets.only(left: 15),child: Text("Currency", style: TextStyle(color: Colors.black), )),
                              icon: Icon(Icons.keyboard_arrow_down, color: Colors.black,),
                              iconSize: 24,
                              onChanged: (String newValue) {
                                setState(() {
                                  currency = newValue;
                                  if(currency.toLowerCase()=='default'){
                                    _conversionRate = _selectedItem.conversionRate;
                                    _conversionRateController.text = conversion_rate.toString();

                                    sell_price = _selectedItem.sellPriceLevel1;
                                    rebate = _selectedItem.rebateLevel1;
                                    marginlic = _selectedItem.millCost / _conversionRate;
                                    brokragepersantage = _selectedItem.brokerageMultiplier;
                                    brokrage = brokragepersantage * marginlic;
                                    freight = _selectedItem.freight;
                                    ad_fund = marginlic * brokrage;
                                    landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                    load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                    margin_lic = landed_cost_per_unit +  load;
                                    gross_margin_level1 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                    sell_price = _selectedItem.sellPriceLevel2;
                                    rebate = _selectedItem.rebateLevel2;
                                    marginlic = _selectedItem.millCost / _conversionRate;
                                    brokragepersantage = _selectedItem.brokerageMultiplier;
                                    brokrage = brokragepersantage * marginlic;
                                    freight = _selectedItem.freight;
                                    ad_fund = marginlic * brokrage;
                                    landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                    load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                    margin_lic = landed_cost_per_unit +  load;
                                    gross_margin_level2 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                    sell_price = _selectedItem.sellPriceLevel3;
                                    rebate = _selectedItem.rebateLevel3;
                                    marginlic = _selectedItem.millCost / _conversionRate;
                                    brokragepersantage = _selectedItem.brokerageMultiplier;
                                    brokrage = brokragepersantage * marginlic;
                                    freight = _selectedItem.freight;
                                    ad_fund = marginlic * brokrage;
                                    landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                    load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                    margin_lic = landed_cost_per_unit +  load;
                                    gross_margin_level3 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;


                                    _sellPriceController[0].text = _selectedItem.sellPriceLevel1.toString();
                                    _rebateController[0].text = _selectedItem.rebateLevel1.toString();
                                    _grossMarginController[0].text = gross_margin_level1.toString();

                                    _sellPriceController[1].text = _selectedItem.sellPriceLevel2.toString();
                                    _rebateController[1].text = _selectedItem.rebateLevel2.toString();
                                    _grossMarginController[1].text = gross_margin_level2.toString();

                                    _sellPriceController[2].text = _selectedItem.sellPriceLevel3.toString();
                                    _rebateController[2].text = _selectedItem.rebateLevel3.toString();
                                    _grossMarginController[2].text = gross_margin_level3.toString();

                                    for(int index=3;index<slabList.length;index++){
                                      calculateGrossMargin(index, _selectedItem, double.parse(_sellPriceController[index].text) , double.parse(_rebateController[index].text));
                                    }

                                  }

                                });
                              },
                              items: currencyList.map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(padding: EdgeInsets.only(left: 15),child: Text(value, style: TextStyle(color: Colors.black),)),
                                );
                              }).toList(),

                            ),
                          )
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width: screenWidth/2,
                        padding: EdgeInsets.all(5),
                        child: FocusScope(
                          canRequestFocus: currency.toLowerCase()=='default'? false: true,
                          child: TextFormField(
                            controller: _conversionRateController,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                borderRadius: new BorderRadius.circular(22.0),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                              hintStyle: globals.inputTextLight,
                              labelText: 'Conversion Rate'
                            ),
                            keyboardType: TextInputType.number,
                            onChanged: (val){
                              setState(() {
                                _conversionRate = double.parse(val);

                                sell_price = _selectedItem.sellPriceLevel1;
                                rebate = _selectedItem.rebateLevel1;
                                marginlic = _selectedItem.millCost / _conversionRate;
                                brokragepersantage = _selectedItem.brokerageMultiplier;
                                brokrage = brokragepersantage * marginlic;
                                freight = _selectedItem.freight;
                                ad_fund = marginlic * brokrage;
                                landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                margin_lic = landed_cost_per_unit +  load;
                                gross_margin_level1 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                sell_price = _selectedItem.sellPriceLevel2;
                                rebate = _selectedItem.rebateLevel2;
                                marginlic = _selectedItem.millCost / _conversionRate;
                                brokragepersantage = _selectedItem.brokerageMultiplier;
                                brokrage = brokragepersantage * marginlic;
                                freight = _selectedItem.freight;
                                ad_fund = marginlic * brokrage;
                                landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                margin_lic = landed_cost_per_unit +  load;
                                gross_margin_level2 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                sell_price = _selectedItem.sellPriceLevel3;
                                rebate = _selectedItem.rebateLevel3;
                                marginlic = _selectedItem.millCost / _conversionRate;
                                brokragepersantage = _selectedItem.brokerageMultiplier;
                                brokrage = brokragepersantage * marginlic;
                                freight = _selectedItem.freight;
                                ad_fund = marginlic * brokrage;
                                landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                margin_lic = landed_cost_per_unit +  load;
                                gross_margin_level3 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                _sellPriceController[0].text = _selectedItem.sellPriceLevel1.toString();
                                _rebateController[0].text = _selectedItem.rebateLevel1.toString();
                                _grossMarginController[0].text = gross_margin_level1.toString();

                                _sellPriceController[1].text = _selectedItem.sellPriceLevel2.toString();
                                _rebateController[1].text = _selectedItem.rebateLevel2.toString();
                                _grossMarginController[1].text = gross_margin_level2.toString();

                                _sellPriceController[2].text = _selectedItem.sellPriceLevel3.toString();
                                _rebateController[2].text = _selectedItem.rebateLevel3.toString();
                                _grossMarginController[2].text = gross_margin_level3.toString();

                                for(int index=3;index<slabList.length;index++){
                                  calculateGrossMargin(index, _selectedItem, double.parse(_sellPriceController[index].text) , double.parse(_rebateController[index].text));
                                }

                              });
                            },
                          ),
                        ),
                      ),
                      Container(
                        width: screenWidth/2,
                        padding: EdgeInsets.all(5),
                        child: RaisedButton(
                          color: Colors.blue,
                          padding: EdgeInsets.all(15),
                          child: Text('RESET', style: TextStyle(color: Colors.white),),
                          onPressed: (){
                            setState(() {
                              _conversionRate = 0;
                              _conversionRateController.text = '';
                              _searchTextController.text = '';
                              _selectedItem = null;
                              slabMap.clear();
                              slabList.clear();
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(5),
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: slabList.length,
                  itemBuilder: (context, index){

                    return Card(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(5),
                              child: Text('Slab Type: '+ 'Slab '+(index+1).toString(), style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  width: screenWidth/4+20,
                                  padding: EdgeInsets.all(5),
                                  child: FocusScope(
                                    canRequestFocus: slabList[index]['focusable']=='yes'? true : false,
                                    child: TextFormField(
                                      controller: _sellPriceController[index],
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                          borderRadius: new BorderRadius.circular(22.0),
                                        ),
                                        focusedBorder:  OutlineInputBorder(
                                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                          borderRadius: new BorderRadius.circular(22.0),
                                        ),
                                        labelText: 'Sell Price',
                                        labelStyle: TextStyle(color: Colors.black54),
                                        hintStyle: globals.inputTextLight,
                                      ),
                                      keyboardType: TextInputType.number,
                                      onChanged: (val){
                                        setState(() {
                                          _sellPrice = double.parse(val);

                                          sell_price = _selectedItem.sellPriceLevel1;
                                          rebate = _selectedItem.rebateLevel1;
                                          marginlic = _selectedItem.millCost / _conversionRate;
                                          brokragepersantage = _selectedItem.brokerageMultiplier;
                                          brokrage = brokragepersantage * marginlic;
                                          freight = _selectedItem.freight;
                                          ad_fund = marginlic * brokrage;
                                          landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                          load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                          margin_lic = landed_cost_per_unit +  load;
                                          gross_margin_level1 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                          sell_price = _selectedItem.sellPriceLevel2;
                                          rebate = _selectedItem.rebateLevel2;
                                          marginlic = _selectedItem.millCost / _conversionRate;
                                          brokragepersantage = _selectedItem.brokerageMultiplier;
                                          brokrage = brokragepersantage * marginlic;
                                          freight = _selectedItem.freight;
                                          ad_fund = marginlic * brokrage;
                                          landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                          load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                          margin_lic = landed_cost_per_unit +  load;
                                          gross_margin_level2 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                          sell_price = _selectedItem.sellPriceLevel3;
                                          rebate = _selectedItem.rebateLevel3;
                                          marginlic = _selectedItem.millCost / _conversionRate;
                                          brokragepersantage = _selectedItem.brokerageMultiplier;
                                          brokrage = brokragepersantage * marginlic;
                                          freight = _selectedItem.freight;
                                          ad_fund = marginlic * brokrage;
                                          landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                          load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                          margin_lic = landed_cost_per_unit +  load;
                                          gross_margin_level3 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;


                                          _sellPriceController[0].text = _selectedItem.sellPriceLevel1.toString();
                                          _rebateController[0].text = _selectedItem.rebateLevel1.toString();
                                          _grossMarginController[0].text = gross_margin_level1.toString();

                                          _sellPriceController[1].text = _selectedItem.sellPriceLevel2.toString();
                                          _rebateController[1].text = _selectedItem.rebateLevel2.toString();
                                          _grossMarginController[1].text = gross_margin_level2.toString();

                                          _sellPriceController[2].text = _selectedItem.sellPriceLevel3.toString();
                                          _rebateController[2].text = _selectedItem.rebateLevel3.toString();
                                          _grossMarginController[2].text = gross_margin_level3.toString();

//                                          for(int i=3;i<slabList.length;i++){
                                            calculateGrossMargin(index, _selectedItem, double.parse(_sellPriceController[index].text) , double.parse(_rebateController[index].text));
//                                          }

                                        });
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                  width: screenWidth/4+20,
                                  padding: EdgeInsets.all(5),
                                  child: FocusScope(
                                    canRequestFocus: slabList[index]['focusable']=='yes'? true : false,
                                    child: TextFormField(
                                      controller: _rebateController[index],
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                          borderRadius: new BorderRadius.circular(22.0),
                                        ),
                                        focusedBorder:  OutlineInputBorder(
                                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                          borderRadius: new BorderRadius.circular(22.0),
                                        ),
                                        labelText: 'Rebate',
                                        labelStyle: TextStyle(color: Colors.black54),
                                        hintStyle: globals.inputTextLight,
                                      ),
                                      keyboardType: TextInputType.number,
                                      onChanged: (val){
                                        setState(() {
                                          _rebate = double.parse(val);

                                          sell_price = _selectedItem.sellPriceLevel1;
                                          rebate = _selectedItem.rebateLevel1;
                                          marginlic = _selectedItem.millCost / _conversionRate;
                                          brokragepersantage = _selectedItem.brokerageMultiplier;
                                          brokrage = brokragepersantage * marginlic;
                                          freight = _selectedItem.freight;
                                          ad_fund = marginlic * brokrage;
                                          landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                          load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                          margin_lic = landed_cost_per_unit +  load;
                                          gross_margin_level1 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                          sell_price = _selectedItem.sellPriceLevel2;
                                          rebate = _selectedItem.rebateLevel2;
                                          marginlic = _selectedItem.millCost / _conversionRate;
                                          brokragepersantage = _selectedItem.brokerageMultiplier;
                                          brokrage = brokragepersantage * marginlic;
                                          freight = _selectedItem.freight;
                                          ad_fund = marginlic * brokrage;
                                          landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                          load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                          margin_lic = landed_cost_per_unit +  load;
                                          gross_margin_level2 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

                                          sell_price = _selectedItem.sellPriceLevel3;
                                          rebate = _selectedItem.rebateLevel3;
                                          marginlic = _selectedItem.millCost / _conversionRate;
                                          brokragepersantage = _selectedItem.brokerageMultiplier;
                                          brokrage = brokragepersantage * marginlic;
                                          freight = _selectedItem.freight;
                                          ad_fund = marginlic * brokrage;
                                          landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;
                                          load  =  landed_cost_per_unit * _selectedItem.loadPercentage;
                                          margin_lic = landed_cost_per_unit +  load;
                                          gross_margin_level3 = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;


                                          _sellPriceController[0].text = _selectedItem.sellPriceLevel1.toString();
                                          _rebateController[0].text = _selectedItem.rebateLevel1.toString();
                                          _grossMarginController[0].text = gross_margin_level1.toString();

                                          _sellPriceController[1].text = _selectedItem.sellPriceLevel2.toString();
                                          _rebateController[1].text = _selectedItem.rebateLevel2.toString();
                                          _grossMarginController[1].text = gross_margin_level2.toString();

                                          _sellPriceController[2].text = _selectedItem.sellPriceLevel3.toString();
                                          _rebateController[2].text = _selectedItem.rebateLevel3.toString();
                                          _grossMarginController[2].text = gross_margin_level3.toString();

//                                          for(int i=3;i<slabList.length;i++){
                                          calculateGrossMargin(index, _selectedItem, _sellPrice, _rebate);
//                                          }

                                        });
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                  width: screenWidth/4+20,
                                  padding: EdgeInsets.all(5),
                                  child: FocusScope(
                                    canRequestFocus: false,
                                    child: TextFormField(
                                      controller: _grossMarginController[index],
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                          borderRadius: new BorderRadius.circular(22.0),
                                        ),
                                        focusedBorder:  OutlineInputBorder(
                                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                          borderRadius: new BorderRadius.circular(22.0),
                                        ),
                                        labelText: 'Gross Margin',
                                        labelStyle: TextStyle(color: Colors.black54),
                                        hintStyle: globals.inputTextLight,
                                      ),
                                      keyboardType: TextInputType.number,
                                      onChanged: (val){
                                        setState(() {

                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: (){
            if(slabList.length<10) {
              setState(() {
                slabMap = new Map();

                slabMap.putIfAbsent('slab_type', () => 'Slab 1');
                slabMap.putIfAbsent('sell_price', () => '0');
                slabMap.putIfAbsent('rebate', () => '0');
                slabMap.putIfAbsent('gross_margin', () => '0');
                slabMap.putIfAbsent('focusable', () => 'yes');
                slabList.add(slabMap);

                _sellPriceController.add(TextEditingController(text: '0.0'));
                _rebateController.add(TextEditingController(text: '0.0'));
                _grossMarginController.add(TextEditingController(text: '0'));

              });
            }
          },
        ),
      ),
    );

  }

  calculateGrossMargin(int index, Response item, double sell_price, double rebate){

    setState(() {
      marginlic = item.millCost / double.parse(_conversionRateController.text);

      brokragepersantage = item.brokerageMultiplier;

      brokrage = brokragepersantage * marginlic;

      freight = item.freight;

      ad_fund = marginlic * brokrage;

      landed_cost_per_unit = marginlic + brokrage + freight + ad_fund;

      load  =  landed_cost_per_unit * item.loadPercentage;

      margin_lic = landed_cost_per_unit +  load;

      gross_margin_level = (( (sell_price - margin_lic ) + rebate )/sell_price)*100;

      _grossMarginController[index].text = gross_margin_level.toString();

    });

    print('sell_price: '+sell_price.toString());
    print('rebate: '+rebate.toString());
    print('gross_margin_level: '+gross_margin_level.toString());
  }

  Future<dynamic> _getListOfFamilies() async {

    final LocalStorage userDataStorage = new LocalStorage('userData');

    var authorizationHeader = await userDataStorage.getItem('authorizationHeader');
    var httpHeader = {
      HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader
    };

    var response = await http.post( globals.baseURL+'families/findproductbyname/' , headers: httpHeader).then((http.Response response) {

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        showInSnackBar("Please check form data.");
        return false;
      }else {
        return response.body;
      }

    });

    return response.toString();

  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
    ));

  }


}


GetFamiliesResponse getFamiliesResponseFromJson(String str) => GetFamiliesResponse.fromJson(json.decode(str));

String getFamiliesResponseToJson(GetFamiliesResponse data) => json.encode(data.toJson());

class GetFamiliesResponse {
  List<Response> response;

  GetFamiliesResponse({
    this.response,
  });

  factory GetFamiliesResponse.fromJson(Map<String, dynamic> json) => GetFamiliesResponse(
    response: List<Response>.from(json["response"].map((x) => Response.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "response": List<dynamic>.from(response.map((x) => x.toJson())),
  };
}

class Response {
  int id;
  Family family;
  String name;
  String productCode;
  String roleCut;
  double millCost;
  UsdCad usdCad;
  double conversionRate;
  double newMillCost;
  double brokerageMultiplier;
  double brokerage;
  Units units;
  double freight;
  double adFundPercentage;
  double adFund;
  double landedCostPerUnit;
  double load;
  double loadPercentage;
  double marginLic;
  double sellPriceLevel1;
  double rebateLevel1;
  String grossMarginLevel1;
  double sellPriceLevel2;
  double rebateLevel2;
  String grossMarginLevel2;
  double sellPriceLevel3;
  double rebateLevel3;
  String grossMarginLevel3;
  String mill;
  String vendorId;
  double sqftCtn;
  double wgtLbsCtn;
  double ctnPallet;
  double sqftPallet;
  int palletContainer20;
  double sqftContainer20;
  int sqftRoll;
  dynamic sqydRoll;
  dynamic pcesCtn;
  dynamic rollsPallet;
  dynamic palletContainer40;
  dynamic tabsStrip;
  dynamic packsPallet;
  dynamic ibsContainer;
  dynamic m2Ctn;
  dynamic lfCtn;
  dynamic qtyCtn;
  DateTime created;

  Response({
    this.id,
    this.family,
    this.name,
    this.productCode,
    this.roleCut,
    this.millCost,
    this.usdCad,
    this.conversionRate,
    this.newMillCost,
    this.brokerageMultiplier,
    this.brokerage,
    this.units,
    this.freight,
    this.adFundPercentage,
    this.adFund,
    this.landedCostPerUnit,
    this.load,
    this.loadPercentage,
    this.marginLic,
    this.sellPriceLevel1,
    this.rebateLevel1,
    this.grossMarginLevel1,
    this.sellPriceLevel2,
    this.rebateLevel2,
    this.grossMarginLevel2,
    this.sellPriceLevel3,
    this.rebateLevel3,
    this.grossMarginLevel3,
    this.mill,
    this.vendorId,
    this.sqftCtn,
    this.wgtLbsCtn,
    this.ctnPallet,
    this.sqftPallet,
    this.palletContainer20,
    this.sqftContainer20,
    this.sqftRoll,
    this.sqydRoll,
    this.pcesCtn,
    this.rollsPallet,
    this.palletContainer40,
    this.tabsStrip,
    this.packsPallet,
    this.ibsContainer,
    this.m2Ctn,
    this.lfCtn,
    this.qtyCtn,
    this.created,
  });

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    id: json["id"],
    family: familyValues.map[json["family"]],
    name: json["name"],
    productCode: json["product_code"] == null ? null : json["product_code"],
    roleCut: json["role_cut"] == null ? null : json["role_cut"],
    millCost: json["mill_cost"] == null ? null : json["mill_cost"].toDouble(),
    usdCad: usdCadValues.map[json["usd_cad"]],
    conversionRate: json["conversion_rate"] == null ? null : json["conversion_rate"].toDouble(),
    newMillCost: json["new_mill_cost"].toDouble(),
    brokerageMultiplier: json["brokerage_multiplier"].toDouble(),
    brokerage: json["brokerage"].toDouble(),
    units: unitsValues.map[json["units"]],
    freight: json["freight"]==null? null : json["freight"].toDouble(),
    adFundPercentage: json["ad_fund_percentage"].toDouble(),
    adFund: json["ad_fund"].toDouble(),
    landedCostPerUnit: json["landed_cost_per_unit"].toDouble(),
    load: json["_load"].toDouble(),
    loadPercentage: json["load_percentage"].toDouble(),
    marginLic: json["margin_lic"].toDouble(),
    sellPriceLevel1: json["sell_price_level1"] == null ? null : json["sell_price_level1"].toDouble(),
    rebateLevel1: json["rebate_level1"] == null ? null : json["rebate_level1"].toDouble(),
    grossMarginLevel1: json["gross_margin_level1"] == null ? null : json["gross_margin_level1"],
    sellPriceLevel2: json["sell_price_level2"] == null ? null : json["sell_price_level2"].toDouble(),
    rebateLevel2: json["rebate_level2"] == null ? null : json["rebate_level2"].toDouble(),
    grossMarginLevel2: json["gross_margin_level2"] == null ? null : json["gross_margin_level2"],
    sellPriceLevel3: json["sell_price_level3"] == null ? null : json["sell_price_level3"].toDouble(),
    rebateLevel3: json["rebate_level3"] == null ? null : json["rebate_level3"].toDouble(),
    grossMarginLevel3: json["gross_margin_level3"] == null ? null : json["gross_margin_level3"],
    mill: json["mill"],
    vendorId: json["vendorID"] == null ? null : json["vendorID"],
    sqftCtn: json["sqft_ctn"] == null ? null : json["sqft_ctn"].toDouble(),
    wgtLbsCtn: json["wgt_lbs_ctn"] == null ? null : json["wgt_lbs_ctn"].toDouble(),
    ctnPallet: json["ctn_pallet"] == null ? null : json["ctn_pallet"].toDouble(),
    sqftPallet: json["sqft_pallet"] == null ? null : json["sqft_pallet"].toDouble(),
    palletContainer20: json["pallet_container_20"] == null ? null : json["pallet_container_20"],
    sqftContainer20: json["sqft_container_20"] == null ? null : json["sqft_container_20"].toDouble(),
    sqftRoll: json["sqft_roll"] == null ? null : json["sqft_roll"],
    sqydRoll: json["sqyd_roll"],
    pcesCtn: json["pces_ctn"],
    rollsPallet: json["rolls_pallet"],
    palletContainer40: json["pallet_container_40"],
    tabsStrip: json["tabs_strip"],
    packsPallet: json["packs_pallet"],
    ibsContainer: json["ibs_container"],
    m2Ctn: json["m2_ctn"],
    lfCtn: json["lf_ctn"],
    qtyCtn: json["qty_ctn"],
    created: DateTime.parse(json["created"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "family": familyValues.reverse[family],
    "name": name,
    "product_code": productCode == null ? null : productCode,
    "role_cut": roleCut == null ? null : roleCut,
    "mill_cost": millCost == null ? null : millCost,
    "usd_cad": usdCadValues.reverse[usdCad],
    "conversion_rate": conversionRate == null ? null : conversionRate,
    "new_mill_cost": newMillCost,
    "brokerage_multiplier": brokerageMultiplier,
    "brokerage": brokerage,
    "units": unitsValues.reverse[units],
    "freight": freight,
    "ad_fund_percentage": adFundPercentage,
    "ad_fund": adFund,
    "landed_cost_per_unit": landedCostPerUnit,
    "_load": load,
    "load_percentage": loadPercentage,
    "margin_lic": marginLic,
    "sell_price_level1": sellPriceLevel1 == null ? null : sellPriceLevel1,
    "rebate_level1": rebateLevel1 == null ? null : rebateLevel1,
    "gross_margin_level1": grossMarginLevel1 == null ? null : grossMarginLevel1,
    "sell_price_level2": sellPriceLevel2 == null ? null : sellPriceLevel2,
    "rebate_level2": rebateLevel2 == null ? null : rebateLevel2,
    "gross_margin_level2": grossMarginLevel2 == null ? null : grossMarginLevel2,
    "sell_price_level3": sellPriceLevel3 == null ? null : sellPriceLevel3,
    "rebate_level3": rebateLevel3 == null ? null : rebateLevel3,
    "gross_margin_level3": grossMarginLevel3 == null ? null : grossMarginLevel3,
    "mill": mill,
    "vendorID": vendorId == null ? null : vendorId,
    "sqft_ctn": sqftCtn == null ? null : sqftCtn,
    "wgt_lbs_ctn": wgtLbsCtn == null ? null : wgtLbsCtn,
    "ctn_pallet": ctnPallet == null ? null : ctnPallet,
    "sqft_pallet": sqftPallet == null ? null : sqftPallet,
    "pallet_container_20": palletContainer20 == null ? null : palletContainer20,
    "sqft_container_20": sqftContainer20 == null ? null : sqftContainer20,
    "sqft_roll": sqftRoll == null ? null : sqftRoll,
    "sqyd_roll": sqydRoll,
    "pces_ctn": pcesCtn,
    "rolls_pallet": rollsPallet,
    "pallet_container_40": palletContainer40,
    "tabs_strip": tabsStrip,
    "packs_pallet": packsPallet,
    "ibs_container": ibsContainer,
    "m2_ctn": m2Ctn,
    "lf_ctn": lfCtn,
    "qty_ctn": qtyCtn,
    "created": created.toIso8601String(),
  };
}

enum Family { CARPET, LUXURY_VINYL, MOLDINGS, ADHESIVES, UNDERLAY, ACCESSORIES, LAMINATE, LVS, RESILIENT, WOOD, LVT, FAMILY_ADHESIVES, ENGINEERED_WOOD, MOULDINGS, RUBBER, RR, LV, MAPLE_LEAF_LAMISOL, EMPTY, CONCRETE_TILES, EMMA, TILES, PROCEDO_FLOORING, VINYL, MANNINGTON_RESILIENT_DROPS_OFF_GOODS, MOULIDINGS, MOLDINGS_REDUCER, MOLDINGS_STR_NOSE, MOLDINGS_T_MOLD, MOLDINGS_THRESHOLD, MOLDINGS_QUARTER_RND, MOLDINGS_WALL_BASE }

final familyValues = EnumValues({
  "Accessories": Family.ACCESSORIES,
  "Adhesives ": Family.ADHESIVES,
  "Carpet": Family.CARPET,
  "Concrete Tiles": Family.CONCRETE_TILES,
  "Emma": Family.EMMA,
  "": Family.EMPTY,
  "Engineered Wood": Family.ENGINEERED_WOOD,
  "Adhesives": Family.FAMILY_ADHESIVES,
  "Laminate": Family.LAMINATE,
  "Luxury Vinyl": Family.LUXURY_VINYL,
  "LV": Family.LV,
  "LVS": Family.LVS,
  "LVT": Family.LVT,
  "MANNINGTON RESILIENT DROPS & OFF GOODS": Family.MANNINGTON_RESILIENT_DROPS_OFF_GOODS,
  "MAPLE LEAF LAMISOL": Family.MAPLE_LEAF_LAMISOL,
  "Moldings": Family.MOLDINGS,
  "Moldings - Quarter RND": Family.MOLDINGS_QUARTER_RND,
  "Moldings - Reducer": Family.MOLDINGS_REDUCER,
  "Moldings - STR NOSE": Family.MOLDINGS_STR_NOSE,
  "Moldings - Threshold": Family.MOLDINGS_THRESHOLD,
  "Moldings - T Mold": Family.MOLDINGS_T_MOLD,
  "Moldings - Wall Base": Family.MOLDINGS_WALL_BASE,
  "Mouldings": Family.MOULDINGS,
  "Moulidings": Family.MOULIDINGS,
  "PROCEDO FLOORING": Family.PROCEDO_FLOORING,
  "Resilient": Family.RESILIENT,
  "RR": Family.RR,
  "Rubber": Family.RUBBER,
  "Tiles": Family.TILES,
  "Underlay": Family.UNDERLAY,
  " VINYL": Family.VINYL,
  "Wood": Family.WOOD
});

enum Units { SF, SY, EACH, ROLL, CUTS, UNITS_CUTS, CU_TS, SPOOL, UNITS_EACH, UNITS_ROLL, STRIP, CTN, LF, EMPTY, PER_CTN, PURPLE_EACH, EA }

final unitsValues = EnumValues({
  "CTN": Units.CTN,
  "cuts": Units.CUTS,
  "CUTs": Units.CU_TS,
  "EA": Units.EA,
  "EACH": Units.EACH,
  "": Units.EMPTY,
  "LF": Units.LF,
  "PER CTN": Units.PER_CTN,
  "Each": Units.PURPLE_EACH,
  "ROLL": Units.ROLL,
  "SF": Units.SF,
  "SPOOL": Units.SPOOL,
  "STRIP": Units.STRIP,
  "SY": Units.SY,
  "Cuts": Units.UNITS_CUTS,
  "each": Units.UNITS_EACH,
  "Roll": Units.UNITS_ROLL
});

enum UsdCad { CAD, USD_GENERAL, USD_MANNINGTON, USD_OTHER, USD_NEW }

final usdCadValues = EnumValues({
  "cad": UsdCad.CAD,
  "usd_general": UsdCad.USD_GENERAL,
  "usd_mannington": UsdCad.USD_MANNINGTON,
  "usd_new": UsdCad.USD_NEW,
  "usd_other": UsdCad.USD_OTHER
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
