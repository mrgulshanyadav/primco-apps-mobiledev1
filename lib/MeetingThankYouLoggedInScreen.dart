import 'package:flutter/material.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/SalesRepDashboardScreen.dart';

class MeetingThankYouLoggedInScreen extends StatefulWidget{
  // final String pdfLink;
  // final String qrCodeLink;

  MeetingThankYouLoggedInScreen({ Key key }): super(key: key);

  @override
  State<StatefulWidget> createState() => new _MeetingThankYouLoggedInScreenState();

}

class _MeetingThankYouLoggedInScreenState extends State<MeetingThankYouLoggedInScreen>{
  // final String pdfLink;
  // final String qrCodeLink;

  _MeetingThankYouLoggedInScreenState();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      
      backgroundColor: globals.secondaryColor,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              Text(
                'Thank you for being with Primco.',
                style: globals.h1White,
                textAlign: TextAlign.center,
              ),
              RaisedButton(
                child: Text('Back to Home'),
                color: Colors.white,
                textColor: globals.secondaryColor,
                onPressed: () {
                  Navigator.pushReplacement(context, 
                    MaterialPageRoute( builder: (context) => SalesRepDashboardScreen() )
                  );
                }
              ),
              
              
              
            ],
          )  
        )
        
      ),
    );
  }
}