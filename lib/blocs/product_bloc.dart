import './bloc_setting.dart';
// import '../customer_list.dart';

class MainBloc extends BloCSetting {
  
  List orderItems = new List();
  
  // Customer data
  String customerId;
  String poNumber;
  String shipTo;
  String additionalComments;
  String shipVia;
  DateTime shipDate;
  String address;
  String city;
  String province;
  String postalCode;
  String sin;
  bool isTest = false;

  //CANCForm1Data _cancForm1Data;

  double grandTotal;
  double grossTotal;
  double points;
  double franklin;

  // Salesrep data
  String salesRepId;
  String salesRepEmail;
  String salesRepEvent;
  String salesOrderId;
  String incentiveAck;
  String purchserName;
  DateTime saleDate;

  setGrandTotal(state, grandTotal, franklin, points, grossTotal ){
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.grandTotal = grandTotal;
    mainBloc.franklin = franklin;
    mainBloc.points = points;
    mainBloc.grossTotal = grossTotal;
  }
  

  setRepId(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.salesRepId = value;
  }

  setOrderTestStatus(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.isTest= value;
  }

  setRepEmail(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.salesRepId = value;
  }

  setSalesRepEvent(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.salesRepEvent = value;
  }

  setSalesOrderId(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.salesOrderId = value;
  }

  setIncentiveAck(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.incentiveAck = value;
  }

  setPurchaserName(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.purchserName= value;
  }

  setSalesDate(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.saleDate = value;
  }

  setAddress(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.address = value;
  }
  setCity(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.city = value;
  }
  setProvince(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.province = value;
  }
  setPostalCode(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.postalCode = value;
  }
  setSin(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.sin = value;
  }

  setCustomerId(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.customerId = value;
  }
  setPoNumber(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.poNumber = value;
  }
  setShipTo(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.shipTo = value;
  }
  setShipVia(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.shipVia = value;
  }
  setShipDate(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.shipDate = value;
  }
  setAdditionalComments(state, value) {
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );
    mainBloc.additionalComments = value;
  }

  setFranklinAndTotal( state, index, quantity, franklin, price, item ){

    rebuildWidgets(
      setStates: (){},
      states: [state]
    );

    var indexOfMine = mainBloc.orderItems.indexWhere((product){
      if( product.index == index ){
        return true;
      } else {
        return false;
      }
    });
     
    mainBloc.orderItems[indexOfMine] = new Product( 
      index: index, 
      franklin: ( double.tryParse(quantity) * double.tryParse(franklin) ).toString() ,
      price: ( double.tryParse(quantity) * double.tryParse(price) * double.tryParse(item) ).toString(),
      // quantity: quantity,
      // total: ''

    ); 
  }
  
  setFormFieldValue( state, 
    index, 
    productId, 
    productName, 
    itemName, 
    itemNumber, 
    description, 
    uom, 
    cash, 
    price,
    total,
    discount, 
    quantity,
    franklinEnabled, 
    franklin, 
    points,
    bulkCount,
    singleFranklin,
    bulkFranklin,
    perItem,
    singleItem,
    bulkItem
  ){
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );

    var indexOfMine = mainBloc.orderItems.indexWhere((product){
      if( product.index == index ){
        return true;
      } else {
        return false;
      }
    });
    
    if( indexOfMine < 0 ) {
      mainBloc.orderItems.add( new Product( 
          index: index, 
          productId: productId, 
          productName: productName, 
          itemName: itemName,
          itemNumber: itemNumber,
          cash: cash,
          description: description,
          discount: discount,
          franklin: franklin,
          franklinEnabled: franklinEnabled,
          points: points,
          price: price,
          quantity: quantity,
          total: total,
          uom: uom,
          bulkCount: bulkCount,
          singleFranklin: singleFranklin,
          bulkFranklin: bulkFranklin,
          perItem: perItem,
          singleItem: singleItem,
          bulkItem: bulkItem


        ) 
      );
      
    } else {
      
      mainBloc.orderItems[indexOfMine] = new Product( 
          index: index, 
          productId: productId, 
          productName: productName, 
          itemName: itemName,
          itemNumber: itemNumber,
          cash: cash,
          description: description,
          discount: discount,
          franklin: franklin,
          franklinEnabled: franklinEnabled,
          points: points,
          price: price,
          quantity: quantity,
          total: total,
          uom: uom,
          bulkCount: bulkCount,
          singleFranklin: singleFranklin,
          bulkFranklin: bulkFranklin,
          perItem: perItem,
          singleItem: singleItem,
          bulkItem: bulkItem

        ) ; 
    } 
  }

  unsetProductItem(state, index){
    rebuildWidgets(
      setStates: (){},
      states: [state]
    );

    var indexOfMine = mainBloc.orderItems.removeWhere((product){
      if( product.index == index ){
        return true;
      } else {
        return false;
      }
    });

  }
}
MainBloc mainBloc; 

class Product{
  int index;
  String productName, 
  productId, 
  itemName, 
  itemNumber, 
  description, 
  uom, 
  price,
  total,
  discount, 
  quantity, 
  franklinEnabled,
  franklin, 
  points,
  bulkCount,
  singleFranklin,
  bulkFranklin,
  perItem,
  singleItem,
  bulkItem;
  bool cash;

  Product({
    this.index,
    this.productId,
    this.productName,
    this.itemName,
    this.itemNumber,
    this.description,
    this.uom,
    this.cash,
    this.price,
    this.total,
    this.discount,
    this.franklinEnabled,
    this.franklin,
    this.points,
    this.quantity,
    this.bulkCount,
    this.singleFranklin,
    this.bulkFranklin,
    this.perItem,
    this.singleItem,
    this.bulkItem
  });

  Map<String, dynamic> toJson() =>
    {
      'index': index,
      'product_id': productId,
      'productName': productName,
      'itemName': itemName,
      'description': description,
      'uom': uom,
      'cash': cash,
      'price': price,
      'total': total,
      'discount': discount,
      'franklin_enabled': franklinEnabled,
      'incentives': franklin,
      'promo': points,
      'quantity': quantity,
      'bulkCount': bulkCount,
      'singleFranklin': singleFranklin,
      'bulkFranklin': bulkFranklin,
      'perItem': perItem,
      'singleItem': singleItem,
      'bulkItem':bulkItem
    };

  
}
