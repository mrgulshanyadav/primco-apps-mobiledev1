import 'package:flutter/material.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/ExpenseAllowance/BillReimbursementForm.dart';

class HomeScreen extends StatefulWidget {
  
  @override
  State<StatefulWidget> createState() => new _HomeScreenState();

}

class _HomeScreenState extends State<HomeScreen> {
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: globals.primaryColorOpac,
        body: SafeArea(
          child: Container(
            child: Center(
              child: ButtonTheme(
                // minWidth: MediaQuery.of(context).size * .90,
                minWidth: MediaQuery.of(context).size.width * .90,
                height: 65.0,
                child: RaisedButton(
                  //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                  child: Text( 'Expense Allowance', style: globals.bigButtonWhite ),
                  color: globals.secondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                  onPressed: (){

                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute( builder: (context) => BillReimbursementForm())// LoginSignupScreen() )
                    );

                  },

                ),
              ),
            ),
          ),
        ),
      );
  }
}

