import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:primcoca/EventDetailScreen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;

class OrdersListScreen extends StatefulWidget{
  final String authorizationHeader;
  OrdersListScreen({ Key key, this.authorizationHeader }): super(key: key);

  @override
  State<StatefulWidget> createState() => new _OrdersListScreenState( authorizationHeader );

}

class _OrdersListScreenState extends State<OrdersListScreen>{
  // final String pdfLink;
  // final String qrCodeLink;

  final String authorizationHeader;

  _OrdersListScreenState( this.authorizationHeader );
  List<Widget> children;
  
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   // getHeader();
  }

  // Future getHeader() async{
  //   authorizationHeader = await userDataStorage.getItem('authorizationHeader');
  // }

  @override
  Widget build(BuildContext context) {
    
    // TODO: implement build
    return Scaffold(
      // backgroundColor: globals.secondaryColor,
      appBar: AppBar(
        title: Text('Orders List',),
        backgroundColor: globals.secondaryColor,
      ),  
      body: SingleChildScrollView(

        child: Padding(
          padding: const EdgeInsets.all(10),
          child: FutureBuilder<http.Response>(
            future: http.post( 
              'http://dev.rndexperts.in/orders.primco.ca/api/orders/getordersbysalesman.json',
              body: { 'id': '40' },
              headers: {
                HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader,
                HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
              }
            ),
            builder: (context, snapshot){

              if( snapshot.hasData ) {
                // print(snapshot.data.body);
                var ordersData = jsonDecode(snapshot.data.body);
                List<Orders> orderList = new List<Orders>();
                
                if( ordersData['status'] == 1 ) {

                  for( int i = 0; i < ordersData['orders'].length; i++ ){

                    List<OrderItems> orderItems = new List<OrderItems>();    
                    
                    for (int j = 0; j < ordersData['orders'][i]['order_items'].length; j++) {
                      var orderSubItem = ordersData['orders'][i]['order_items'][j];   
                      orderItems.add( 
                        new OrderItems( 
                          orderSubItem['id'].toString(), 
                          orderSubItem['incentives'].toString(), 
                          orderSubItem['price'].toString(), 
                          orderSubItem['product_id'].toString(), 
                          orderSubItem['promo'].toString(), 
                          orderSubItem['quantity'].toString(), 
                          orderSubItem['total'].toString()
                        ) 
                      );
                    }
                    var orderListData = ordersData['orders'][i];
                    orderList.add(
                      new Orders(
                        orderListData['id'].toString(),
                        orderListData['customer_id'].toString(),
                        orderListData['grand_total'].toString(),
                        orderListData['salesman_id'].toString(),
                        orderItems
                      )
                    );

                  }

                  print( orderList );

                  return Text('finding');

                } else {
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'No orders found.'
                      ),
                    ),
                  );
                }
                
              } else {
                if( snapshot.hasError ){
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'Error loading data. '+ snapshot.error.toString()
                      ),
                    ),
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator()
                  );
                  
                }
              }

            }
          )

          // child: Column(
          //   // mainAxisAlignment: MainAxisAlignment.start,
          //   children: <Widget>[
              
          //     GestureDetector(
          //       onTap:(){
          //         // Navigator.push(
          //         //   context, 
          //         //   MaterialPageRoute( builder: (context) => AddOrderScreen() ) 
          //         // );
          //       },
          //       child: Card(
          //         child: Padding(
          //           padding: const EdgeInsets.all(10),
          //           child: Row(
          //             children: <Widget>[
          //               Icon(
          //                 Icons.add_shopping_cart
          //               ),
          //               SizedBox(
          //                 width: 20
          //               ),
          //               Text(
          //                 'Add Order', style: globals.h4Primary,
          //               )

          //             ],
          //           ),
          //         ),
          //       ),
          //     ),
              
          //   ],
          // )  
        )
        
      ),
    );
  }
}





class Orders{
  String id;
  String customerId;
  String salesmanId;
  String grandTotal;
  List<OrderItems> orderItems;

  Orders(
      this.id,
      this.customerId,
      this.grandTotal,
      this.salesmanId,
      this.orderItems
      );

}

class OrderItems{
  String id;
  String productId;
  String price;
  String quantity;
  String incentives;
  String total;
  String promo;

  OrderItems(
      this.id,
      this.incentives,
      this.price,
      this.productId,
      this.promo,
      this.quantity,
      this.total
      );

}
