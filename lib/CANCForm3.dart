import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:ui' as ui;
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:localstorage/localstorage.dart';

import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/ThankYouScreen.dart';
import 'package:primcoca/customer_list.dart';

class CANCForm3 extends StatefulWidget {
  final String contactId;
  CANCForm3({ Key key, this.contactId }) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _CANCForm3State(contactId);

}

class _CANCForm3State extends State<CANCForm3> {
  
  final contactId;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final LocalStorage userDataStorage = new LocalStorage('userData');
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _sign = GlobalKey<SignatureState>();
  final format = DateFormat("yyyy-MM-dd");

  _CANCForm3State( this.contactId );
  
  TextEditingController submissionDate = new TextEditingController(); 
  TextEditingController customerNumber = new TextEditingController();
  TextEditingController companyName = new TextEditingController();
  TextEditingController adminName = new TextEditingController();
  TextEditingController adminEmail = new TextEditingController();
  TextEditingController _date = new TextEditingController();
  TextEditingController _title = new TextEditingController();
  TextEditingController _printName = new TextEditingController();
  
  bool isPartOfGroup = false;
  
  bool accountDetails = false;
  bool invoices = false;
  bool priceList = false;
  bool quotes = false;
  bool purchase = false;
  bool price = false;

  bool stdAccountDetails = false;
  bool stdPlacedOrders = false;
  bool stdInvoicedOrder = false;
  bool stdCreditMemo = false;
  bool stdPrices = false;
  bool stdPriceList = false;

  var standarUserWidget = List<Widget>();
  List standarUserList = new List();
  int count = 0;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadData();
   // mainBloc = MainBloc();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    //mainBloc.dispose();
    super.dispose();
  }

  void _loadData() async {
    await SalesmenViewModel.loadPlayers();
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
      ));
    
  }

  saveData() async{
    
    final sign = _sign.currentState;
    final image = await sign.getData();
    var data = await image.toByteData(format: ui.ImageByteFormat.png);
    final encodedSignature = base64.encode(data.buffer.asUint8List());

    var postData = {
      'credit_application_id': contactId,
      'submission_date': submissionDate.text,
      'customer_number': customerNumber.text,
      'company_name': companyName.text,
      'admin_can_see_account_details': accountDetails ? '1' : '0',
      'admin_can_see_invoices': invoices ? '1': '0',
      'admin_can_see_price_list': priceList ? '1': '0',
      'admin_can_see_quotes': quotes ? '1': '0',
      'admin_can_see_purchases': purchase ? '1': '0',
      'admin_can_see_prices': price ? '1': '0',
      'standar_user_contact': jsonEncode( standarUserList ),
      'first__last_name': adminName.text,
      'user_email': adminEmail.text,
      'can_see_account_details': stdAccountDetails ? '1': '0',
      'can_see_placed_order': stdPlacedOrders ? '1': '0',
      'can_see_invoiced_order': stdInvoicedOrder? '1': '0',
      'can_see_credit_memos': stdCreditMemo ? '1': '0',
      'can_see_prices': stdPrices ? '1': '0',
      'can_see_price_list': stdPriceList ? '1': '0',
      'print_name': _printName.text,
      '_date': _date.text,
      '_title': _title.text,
      'anti_spam_consent': '1',
      'signature': encodedSignature
    };

    print(postData);

    showInSnackBar('Saving data...');

    // return;
    // http://dev.rndexperts.in/primco/api/online-user-registration/addonlineuser.json
    var authorizationHeader = userDataStorage.getItem('authorizationHeader');
    http.post( globals.baseURL+'online-user-registration/addonlineuser.json', 
      body: postData, 
      headers: {
        HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader,
        HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
      }
    ).then((http.Response response) {
      final int statusCode = response.statusCode;
      debugPrint(response.body);
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      
      var responsData = json.decode(response.body);
      
      print(responsData);
      if( responsData['status'] == 1 ) {
        showInSnackBar('Data saved. Redirecting...');
        Navigator.pushReplacement(context, 
          MaterialPageRoute( builder: (context) => 
          
            ThankYouScreen( 
              qrCodeLink: responsData['response']['qrcode_link'], 
              pdfLink: responsData['response']['pdf_link'] 
            ) 
          )
        );
      } else {
        showInSnackBar('Error while saving data!');
        print('falied');
      
        // showInSnackBar('Failed to save data.'+ responseData['errors'] );
      }
      
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Credit Application For New Customer',),
        backgroundColor: globals.secondaryColor,
      ),
      
      persistentFooterButtons: <Widget>[
        ButtonTheme(
          // minWidth: MediaQuery.of(context).size * .90,
          minWidth: MediaQuery.of(context).size.width * .90,
          height: 45.0,
          child: RaisedButton( 
            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
            color: globals.secondaryColor,
            textColor: Colors.white,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
            onPressed: (){
              saveData();
            },

          ),
        )
      ],
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Company Information', style: globals.h1Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,

            ),
            Form(
              key: formKey,
              autovalidate: true,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  // Legal name, trade name
                  Row(
                    children: <Widget>[
                      
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: DateTimeField(
                            controller: submissionDate,
                            format: format,
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2100));
                            },
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Submission Date',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Submission Date',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              //mainBloc.setSalesDate( this, val );
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: customerNumber,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Customer Number',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Customer Number',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyName,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Company Name',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Company Name',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: <Widget>[
                      Text('Administrator Will See:')
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: accountDetails??false,
                              onChanged: (val){
                                setState(() {
                                  accountDetails = val;
                                });
                              }
                            ),
                            Text('Account Details, Including Credit Details')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: invoices??false,
                              onChanged: (val){
                                setState(() {
                                  invoices = val;
                                });
                              }
                            ),
                            Text('Invoices(Open/Closed/Credit Memos)')
                          ],
                        ),
                      ),
                      
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: priceList??false,
                              onChanged: (val){
                                setState(() {
                                  priceList = val;
                                });
                              }
                            ),
                            Text('Pricelists')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: quotes??false,
                              onChanged: (val){
                                setState(() {
                                  quotes = val;
                                });
                              }
                            ),
                            Text('Quotes')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: purchase??false,
                              onChanged: (val){
                                setState(() {
                                  purchase = val;
                                });

                              }
                            ),
                            Text('Purchases')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: price??false,
                              onChanged: (val){
                                setState(() {
                                  price = val;
                                });

                              }
                            ),
                            Text('Prices')
                          ],
                        ),
                      ),
                      
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text('Administrator'),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: adminName,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Name',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Name',
                                labelStyle: globals.inputTextLight,
                            ),
                            // onSaved: (val) => this._name = val,
                            onChanged: ( value ){
                              // this.description = value;
                            },
                            // enabled: false
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: adminEmail,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Email',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Email',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: ( value ){
                              // this.uom = value;
                            },
                            // enabled: false
                          ),
                        ),
                      ),
                      
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text('Standard User'),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: standarUserWidget,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Tap button to add.',
                      ),
                      RaisedButton(
                        color: globals.secondaryColor,
                        textColor: Colors.white,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                        onPressed: (){
                          setState(() {
                            count++;
                            standarUserWidget.add( new ContactList( count: count, title: "User", customerContactList: standarUserList ) );  
                          });
                        },
                        child: Text('Add More Invoice Contact'),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: <Widget>[
                      Text('Standard User Options:')
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: stdAccountDetails,
                              onChanged: (val){
                                setState(() {
                                  stdAccountDetails = val;
                                });

                              }
                            ),
                            Text('Can See Account Details')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: stdPlacedOrders,
                              onChanged: (val){
                                setState(() {
                                  stdPlacedOrders = val;
                                });

                              }
                            ),
                            Text('Can See Placed Order')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: stdInvoicedOrder,
                              onChanged: (val){
                                setState(() {
                                  stdInvoicedOrder = val;
                                });

                              }
                            ),
                            Text('Can See Invoiced Order')
                          ],
                        ),
                      ),
                      
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: stdCreditMemo,
                              onChanged: (val){
                                setState(() {
                                  stdCreditMemo = val;
                                });

                              }
                            ),
                            Text('Can See Credit Memos')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: stdPrices,
                              onChanged: (val){
                                setState(() {
                                  stdPrices = val;
                                });

                              }
                            ),
                            Text('Can See Prices')
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: stdPriceList,
                              onChanged: (val){
                                setState(() {
                                  stdPriceList = val;
                                });

                              }
                            ),
                            Text('Can See Price List')
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: DateTimeField(
                            controller: _date,
                            format: format,
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2100));
                            },
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Date',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Date',
                                labelStyle: globals.inputTextLight,
                            ),
                            onChanged: (val){
                              //mainBloc.setSalesDate( this, val );
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: _title,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Title',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Title',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: _printName,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Print Name',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Print Name',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    height: 200,
                    width: MediaQuery.of(context).size.width * .90,
                    child: Container(
                      
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.black,
                          width: 1,
                        ),
                      ),
                      child: Signature(

                        color: Colors.black,// Color of the drawing path
                        strokeWidth: 2.0,
                        backgroundPainter: null, // Additional custom painter to draw stuff like watermark 
                        onSign: null, // Callback called on user pan drawing
                        key: _sign,
                        // key that allow you to provide a GlobalKey that'll let you retrieve the image once user has signed
                      ),
                    )
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: ButtonTheme(
                          // minWidth: MediaQuery.of(context).size * .90,
                          minWidth: MediaQuery.of(context).size.width * .90,
                          height: 45.0,
                          child: RaisedButton( 
                            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
                            color: globals.secondaryColor,
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                            onPressed: (){
                              saveData();
                            },

                          ),
                        ),
                      )
                    ],
                  )
                  
                ],
              ),
            ),
            
          ],
        )
      )
    );
  }
}

