import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:email_validator/email_validator.dart';
import 'package:localstorage/localstorage.dart';

import 'package:primcoca/CANCFormContactList.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/customer_list.dart';

class CANCForm1 extends StatefulWidget {
  
  CANCForm1({ Key key }) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _CANCForm1State();

}

class _CANCForm1State extends State<CANCForm1> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final LocalStorage userDataStorage = new LocalStorage('userData');

  _CANCForm1State();
  
  TextEditingController companyLegalName = new TextEditingController(); 
  TextEditingController companyTradeName = new TextEditingController();
  TextEditingController companyStreetAddress = new TextEditingController();
  TextEditingController companyPhone = new TextEditingController();
  TextEditingController companyFax = new TextEditingController();
  TextEditingController companyCity = new TextEditingController();
  TextEditingController companyProvince = new TextEditingController();
  TextEditingController companyPostalCode = new TextEditingController();
  TextEditingController companyEmailAddress = new TextEditingController();

  TextEditingController ownerName = new TextEditingController();
  TextEditingController ownerHomeAddress = new TextEditingController();
  TextEditingController ownerHomePhone = new TextEditingController();
  TextEditingController ownerCellPhone  = new TextEditingController();

  TextEditingController managerName = new TextEditingController();
  TextEditingController managerPhone = new TextEditingController();
  TextEditingController financialInstitutionIncludingFinancialAddress = new TextEditingController();
  TextEditingController bankAccountInformation = new TextEditingController();
  TextEditingController managerCity = new TextEditingController();
  TextEditingController managerProvince = new TextEditingController();
  TextEditingController managerPosalCode = new TextEditingController();
  TextEditingController managerPhone2 = new TextEditingController();
  TextEditingController managerGST = new TextEditingController();
  TextEditingController provincialSalesTax = new TextEditingController();
  TextEditingController amountOfCreditRequired = new TextEditingController();
  TextEditingController expectedSalesVolumePerYear = new TextEditingController();
  TextEditingController ficalYearEnd = new TextEditingController();
  TextEditingController isPartOfBuyGroup = new TextEditingController();
  TextEditingController buyGroupName = new TextEditingController();
  
  bool isPartOfGroup = false;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadData();
   // mainBloc = MainBloc();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    //mainBloc.dispose();
    super.dispose();
  }

  void _loadData() async {
    await SalesmenViewModel.loadPlayers();
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
      ));
    
  }

  saveData() async{
    
    var postData = {
      'company_legal_name': companyLegalName.text??'',
      'company_trade_name': companyTradeName.text??'',
      'street_address': companyStreetAddress.text??'',
      'phone': companyPhone.text??'',
      'fax': companyFax.text??'',
      'city': companyCity.text??'',
      'province': companyProvince.text??'',
      'postal_code': companyPostalCode.text??'',
      'email_address': companyEmailAddress.text??'',
      'owner_name': ownerName.text??'',
      'owner_home_address': ownerHomeAddress.text??'',
      'owner_home_phone': ownerHomePhone.text??'',
      'owner_cell_phone': ownerCellPhone.text??'',
      'manager_name': managerName.text??'',
      'manager_phone': managerPhone.text??'',
      'manager_finacial_address': financialInstitutionIncludingFinancialAddress.text??'',
      'manager_bank_account': bankAccountInformation.text??'',
      'manager_city': managerCity.text??'',
      'manager_province': managerProvince.text??'',
      'manager_postal_code': managerPosalCode.text??'',
      'manager_phone_2': managerPhone2.text??'',
      'manager_gst': managerGST.text??'',
      'province_sales_tax': provincialSalesTax.text??'',
      'amount_credit_required': amountOfCreditRequired.text??'',
      'yearly_sales_volume': expectedSalesVolumePerYear.text??'',
      'fiscal_year_end': ficalYearEnd.text,
      'is_group': isPartOfGroup?"1":"0",
      'group_name': buyGroupName.text??''
    };

    print(postData.toString());

    showInSnackBar('Saving data...');

    // http://dev.rndexperts.in/primco/api/credit-applications/addcustomer.json

    var authorizationHeader = userDataStorage.getItem('authorizationHeader');
    http.post( globals.baseURL+'credit-applications/addcustomer.json', 
      body: postData, 
      headers: {
        HttpHeaders.authorizationHeader: 'Basic ' + authorizationHeader,
        HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
      }
    ).then((http.Response response) {
      final int statusCode = response.statusCode;
      debugPrint(response.body);
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      
      var responsData = json.decode(response.body);
      
      //print(responsData);
      if( responsData['status'] == 1 ) {
        showInSnackBar('Data saved. Redirecting...');
        print(responsData);
        Navigator.pushReplacement(context, 
          MaterialPageRoute( builder: (context) => 
            CANCFormContactList( contactId: responsData['creditApplication']['id'].toString() ) ,
          )
        );
      } else {
        showInSnackBar('Error while saving data!');
        print('falied');
      
        // showInSnackBar('Failed to save data.'+ responseData['errors'] );
      }
      
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Credit Application For New Customer',),
        backgroundColor: globals.secondaryColor,
      ),
      
      persistentFooterButtons: <Widget>[
        ButtonTheme(
          // minWidth: MediaQuery.of(context).size * .90,
          minWidth: MediaQuery.of(context).size.width * .90,
          height: 45.0,
          child: RaisedButton( 
            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
            color: globals.secondaryColor,
            textColor: Colors.white,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
            onPressed: (){
              saveData();
            },

          ),
        )
      ],
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Company Information', style: globals.h1Secondary, textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,

            ),
            Form(
              key: formKey,
              autovalidate: true,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  // Legal name, trade name
                  Row(
                    children: <Widget>[
                      
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyLegalName,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Company Legal Name *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Company Legal Name *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyTradeName,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Company Trade Name *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Company Trade Name *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Street address, phone
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyStreetAddress,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Street Address *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Street Address *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyPhone ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Phone # *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Phone # *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Fax, City
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyFax ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Fax #',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Fax #',
                                labelStyle: globals.inputTextLight,
                            )
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyCity ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'City *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'City *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Porvince, Postal code
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyProvince ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Province *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Province *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyPostalCode ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Postal Code *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Postal Code *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Email
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: companyEmailAddress ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Email Address *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Email Address *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if( !EmailValidator.validate(value, true) ){
                                return 'Not a valid email.';

                              }
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Owner Information', style: globals.h1Secondary, textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Owner name, Owner home address
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: ownerName ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Owners Name *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Owners Name *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: ownerHomeAddress,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Owners Home Address *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Owners Home Address *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Owner home phone, owner cell phone
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: ownerHomePhone,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Owners Home Phone #',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Owners Home Phone #',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: ownerCellPhone ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Owners Cell Phone #',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Owners Cell Phone #',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Management Information', style: globals.h1Secondary, textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: managerName,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Manager Name *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Manager Name *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: managerPhone ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Manager Phone #',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Manager Phone #',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: financialInstitutionIncludingFinancialAddress ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Finacial Institution Including Finacial Address',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Finacial Institution Including Finacial Address',
                                labelStyle: globals.inputTextLight,
                            )
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: bankAccountInformation,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Bank Account Information(please provide void cheque):',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Bank Account Information(please provide void cheque):',
                                labelStyle: globals.inputTextLight,
                            )
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: managerCity ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'City',
                                hintStyle: globals.inputTextLight,
                                labelText: 'City',
                                labelStyle: globals.inputTextLight,
                            )
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: managerProvince ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Province',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Province',
                                labelStyle: globals.inputTextLight,
                            )
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: managerPosalCode,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Postal Code',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Postal Code',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: managerPhone2 ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Phone',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Phone',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: managerGST ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'GST/HST Registration #*',
                                hintStyle: globals.inputTextLight,
                                labelText: 'GST/HST Registration #*',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: provincialSalesTax ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Provincial Sales Tax # *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Provincial Sales Tax # *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: amountOfCreditRequired ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Amount of Credit Required *',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Amount of Credit Required *',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: expectedSalesVolumePerYear ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Expected Sales Volume Per Year',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Expected Sales Volume Per Year',
                                labelStyle: globals.inputTextLight,
                            )
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: TextFormField( 
                            controller: ficalYearEnd ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Fiscal Year End',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Fiscal Year End',
                                labelStyle: globals.inputTextLight,
                            )
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Row(
                            children: <Widget>[
                              Checkbox( 
                                value: this.isPartOfGroup??true,
                                onChanged: (bool value) {
                                  setState(() {
                                    isPartOfGroup = value;
                                    
                                    if( value ){
                                      //_cancForm1Data.isPartOfBuyGroup = "1";
                                    } else {
                                      //_cancForm1Data.isPartOfBuyGroup= "0";
                                    }
                                  });
                                  
                                },
                              ),
                              Text("Are you part \nof a Buy Group?")
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: isPartOfGroup ? TextFormField( 
                            controller: buyGroupName ,
                            style: globals.inputText,
                            decoration: InputDecoration(
                                
                                contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                focusedBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                                  borderRadius: new BorderRadius.circular(22.0),
                                ),
                                //icon: Icon(Icons.lock,color: Colors.black38,),
                                hintText: 'Buy Group Name',
                                hintStyle: globals.inputTextLight,
                                labelText: 'Buy Group Name',
                                labelStyle: globals.inputTextLight,
                            ),
                            validator: ( value ){
                              if (value.isEmpty) {
                                return 'Field is required.';
                              }
                            }
                          ) : 
                          Text(''),
                        ),
                      ),
                      
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: ButtonTheme(
                          // minWidth: MediaQuery.of(context).size * .90,
                          minWidth: MediaQuery.of(context).size.width * .90,
                          height: 45.0,
                          child: RaisedButton( 
                            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
                            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
                            color: globals.secondaryColor,
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
                            onPressed: (){
                              saveData();
                            },

                          ),
                        ),
                      )
                    ],
                  )
                  
                ],
              ),
            ),
            
          ],
        )
      )
    );
  }
}

