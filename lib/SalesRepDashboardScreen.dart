import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:flutter/foundation.dart';
import 'package:primcoca/AddOrderScreen.dart';
import 'package:primcoca/ExpenseAllowance/BillsListScreen.dart';
import 'package:primcoca/CANCForm1.dart';
import 'package:primcoca/EventListScreen.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/LoginSignupScreen.dart';
import 'package:primcoca/MarginCalculator/MarginCalculator.dart';
import 'package:primcoca/OrdersListScreen.dart';
import 'package:primcoca/SalesRepMeetingScreen.dart';
import 'blocs/product_bloc.dart';
import 'package:localstorage/localstorage.dart';
import './ExpenseAllowance/BillReimbursementForm.dart';


class SalesRepDashboardScreen extends StatefulWidget {
  final MainBloc mainBloc;
  SalesRepDashboardScreen({ Key key, this.mainBloc }): super(key: key);
  @override
  State<StatefulWidget> createState() => new _SalesRepDashboardScreenState();

}

class _SalesRepDashboardScreenState extends State<SalesRepDashboardScreen> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  int _selectedGender = 0;
  final LocalStorage userDataStorage = new LocalStorage('userData');

  TextEditingController _date = new TextEditingController();
  TextEditingController agenda = new TextEditingController();
  TextEditingController description = new TextEditingController();
  TextEditingController email = new TextEditingController();

  final format = DateFormat("yyyy-MM-dd kk:mm");

  //final MainBloc mainBloc;
  _SalesRepDashboardScreenState();

  var formFieldsColumn = List<Widget>();
  int count = 0;

  List<DropdownMenuItem<int>> genderList = [];

  List<String> _province = <String>['', 'Saskatoon', 'Alberta', 'Manitoba'];
  String _selectProvince = '';

  List<String> _salesMen = <String>[];
  String _selectedSalesmen = '';
  DropdownButton salesMenDD;

  var authorizationHeader;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // mainBloc.setGrandTotal(this, 0.0, 0.0, 0.0, 0.0);
    getHeader();
  }

  Future getHeader() async{
    authorizationHeader = await userDataStorage.getItem('authorizationHeader');
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
      ));
  }
  
  @override
  Widget build(BuildContext context) {
    
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard',),
        backgroundColor: globals.secondaryColor,
        leading: null,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.power_settings_new),
            onPressed: (){
              userDataStorage.deleteItem('userData');
              Navigator.pushReplacement(
                context,
                MaterialPageRoute( builder:(context) => LoginSignupScreen() )
              );
            },
          )
        ],
      ),
      key: scaffoldKey,
      
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap:(){
                Navigator.push(
                  context, 
                  MaterialPageRoute( builder: (context) => AddOrderScreen() ) 
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.add_shopping_cart
                      ),
                      SizedBox(
                        width: 20
                      ),
                      Text(
                        'Add Order', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap:(){
                Navigator.push(
                  context, 
                  MaterialPageRoute( builder: (context) => OrdersListScreen( authorizationHeader: authorizationHeader, ) ) 
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.list
                      ),
                      SizedBox(
                        width: 20
                      ),
                      Text(
                        'List Orders', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap:(){
                Navigator.push(
                  context, 
                  MaterialPageRoute( builder: (context) => CANCForm1() ) 
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.face
                      ),
                      SizedBox(
                        width: 20
                      ),
                      Text(
                        'Add Customer Credit Application', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap:(){
                Navigator.push(
                  context, 
                  MaterialPageRoute( builder: (context) => SalesRepMeetingScreen() ) 
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.calendar_today
                      ),
                      SizedBox(
                        width: 20
                      ),
                      Text(
                        'Schedule Meeting', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap:(){
                Navigator.push(
                  context, 
                  MaterialPageRoute( builder: (context) => EventListScreen() )
                  // CANCForm3()
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.flag
                      ),
                      SizedBox(
                        width: 20
                      ),
                      Text(
                        'Events', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap:(){
                Navigator.push(
                    context,
                    MaterialPageRoute( builder: (context) => BillReimbursementForm() )
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                          Icons.add_a_photo
                      ),
                      SizedBox(
                          width: 20
                      ),
                      Text(
                        'Add Expense Allowance', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap:(){
                Navigator.push(
                    context,
                    MaterialPageRoute( builder: (context) => BillsListScreen() )
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                          Icons.list
                      ),
                      SizedBox(
                          width: 20
                      ),
                      Text(
                        'List of Bills', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap:(){
                Navigator.push(
                    context,
                    MaterialPageRoute( builder: (context) => MarginCalculator() )
                );
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                          Icons.attach_money
                      ),
                      SizedBox(
                          width: 20
                      ),
                      Text(
                        'Margin Calculator', style: globals.h4Primary,
                      )

                    ],
                  ),
                ),
              ),
            ),

          ],
        )
        
      )
    );
  }
}
