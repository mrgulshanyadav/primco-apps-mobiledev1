import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:primcoca/GlobalVariables.dart' as globals;
import 'package:primcoca/MeetingThankYouScreen.dart';
import 'package:primcoca/customer_list.dart';
import 'blocs/product_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SalesmanData{
  int id;
  String name, email, photo, phone;

  SalesmanData({
    this.id,
    this.name,
    this.email,
    this.photo,
    this.phone
  });
}

class ClientMeetingScreen extends StatefulWidget {
  final MainBloc mainBloc;
  ClientMeetingScreen({ Key key, this.mainBloc }): super(key: key);
  @override
  State<StatefulWidget> createState() => new _ClientMeetingScreenState();

}

class _ClientMeetingScreenState extends State<ClientMeetingScreen> {
  
  _ClientMeetingScreenState();
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  
  TextEditingController _date = new TextEditingController();
  TextEditingController agenda = new TextEditingController();
  TextEditingController description = new TextEditingController();
  TextEditingController email = new TextEditingController();

  final format = DateFormat("yyyy-MM-dd kk:mm");
  var formFieldsColumn = List<Widget>();
  int count = 0;

  List<DropdownMenuItem<int>> genderList = [];
  List<String> _province = <String>[ '','SK', 'AB', 'BC', 'MB'];
  String _selectProvince = '';
  List<SalesmanData> _salesMen = List<SalesmanData>();
  String baseUrl;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // mainBloc.setGrandTotal(this, 0.0, 0.0, 0.0, 0.0);
    
  }

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message)
      ));
    
  }
  
  
  int selectSalesRepId = null;
  void _handleSalesRepChange(int value) {
    setState(() {
      selectSalesRepId = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Schedule Meeting',),
        backgroundColor: globals.secondaryColor,
      ),
      key: scaffoldKey,
      persistentFooterButtons: <Widget>[
        ButtonTheme(
          // minWidth: MediaQuery.of(context).size * .90,
          minWidth: MediaQuery.of(context).size.width * .90,
          height: 45.0,
          child: RaisedButton( 
            //padding: EdgeInsets.symmetric( vertical: 10.0, horizontal: 35.0 ),
            child: Text( 'Save & Next', style: globals.bigButtonWhite ),
            color: globals.secondaryColor,
            textColor: Colors.white,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(33.0)),
            onPressed: (){

              if( agenda.text == "" ) {
                showInSnackBar("Agenda is required.");  
                return;
              } else if( description.text == "" ){
                showInSnackBar("Description is required.");  
                return;
              } else if( _date.text == "" ) {
                showInSnackBar("Date is required.");  
                return;
              } else if( email.text == "" ){
                showInSnackBar("Email is required.");  
                return;
              }
              
              showInSnackBar('Sending request');
              
              var postData = { 
                'salesman_id': selectSalesRepId.toString(),
                'agenda': agenda.text.toString(),
                'description': description.text.toString(),
                'date_time': _date.text.toString(),
                'email': email.text.toString(),
                'is_customer_meeting': '1'
              };
              print(postData);
              http.post( globals.baseURL+'meetings/addmeeting.json', 
                body: postData
              ).then((response){
                final int statusCode = response.statusCode;
                debugPrint(response.body);
                if (statusCode < 200 || statusCode > 400 ) {
                  throw new Exception("Error while fetching data");
                }
                
                var responsData = json.decode(response.body);
                
                //print(responsData);
                if( responsData['status'] == 1 ) {
                  print(responsData);
                  print('success');
                  showInSnackBar('Meeting saved. Redirecting');
                  Navigator.pushReplacement(
                    context, 
                    MaterialPageRoute( builder: (context) => MeetingThankYouScreen() )
                  );
                } else {
                  print(responsData);
                  showInSnackBar('Sorry! we were unable to schedule the meeting. Please try again later.');
                }
              });

            },

          ),
        )
      ],
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                'Schedule Meeting', style: globals.h1Secondary, textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                children: <Widget>[
                  
                  Expanded(
                    child: new FormField(
                      builder: (FormFieldState state) {
                        return InputDecorator(
                          
                          decoration: InputDecoration(
                            
                            contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            focusedBorder:  OutlineInputBorder(
                              borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                              borderRadius: new BorderRadius.circular(22.0),
                            ),
                            //icon: Icon(Icons.lock,color: Colors.black38,),
                            hintText: 'Select Province',
                            hintStyle: globals.inputTextLight,
                            labelText: 'Province',
                            labelStyle: globals.inputTextLight,
                          ),

                          isEmpty: _selectProvince == '',
                          child: new DropdownButtonHideUnderline(
                            child: new DropdownButton(
                              value: _selectProvince,
                              isExpanded: true,
                              disabledHint: Text('No Province.'),
                              hint: Text('Select Province Item'),
                              isDense: true,
                              onChanged: (String newValue) {
                                setState(() {
                                  // _selectedSalesmen = null;
                                  _selectProvince = newValue;
                                  state.didChange(newValue);
                                  //salesMenDD = null;
                                  http.post( globals.baseURL+'products/getallsalesmen.json', body: { 'province': _selectProvince } )
                                  .then((response){
                                    final int statusCode = response.statusCode;
                                    debugPrint(response.body);
                                    if (statusCode < 200 || statusCode > 400 ) {
                                      throw new Exception("Error while fetching data");
                                    }
                                    
                                    var responsData = json.decode(response.body);
                                    
                                    //print(responsData);
                                    if( responsData['status'] == 1 ) {
                                      
                                      _salesMen.clear();
                                      baseUrl = responsData['base_url'];
                                      for (var i = 0; i < responsData['json_records'].length; i++){ 
                                        // _salesMen.add(responsData['json_records'][i]['id'].toString()+': '+responsData['json_records'][i]['name']);
                                        _salesMen.add( new SalesmanData( 
                                          id: responsData['json_records'][i]['id'],
                                          name: responsData['json_records'][i]['name'],
                                          email: responsData['json_records'][i]['email'],
                                          phone: responsData['json_records'][i]['phone'],
                                          photo: responsData['json_records'][i]['photo'],
                                        ) );
                                      }
                                    } else {
                                      showInSnackBar('Failed to load data.' );
                                    }
                                  });


                                });
                              },
                              items: _province.map((String value) {
                                return new DropdownMenuItem(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                      
                ],
              ),
              
              _salesMen.length > 0 
              ? Container(
                child: GridView.count(
                  shrinkWrap: true,
                  crossAxisCount: 2, 
                  childAspectRatio: 0.9,
                  physics: NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(5),
                  children: _salesMen.map((row){
                    return Card(
                      
                      child: Column (
                        children: <Widget>[
                          row.photo.toString() != "null" ? 
                          Image( 
                            image: NetworkImage( baseUrl.toString() + row.photo.toString() ),
                            fit: BoxFit.cover,
                            height: 150,
                            width: 250, 
                          ):
                          Container(
                            height: 150,
                            width: 250,
                            child: Text('No Image'),
                            alignment: Alignment(0.0, 0.0),
                          ),
                          SizedBox(
                            height: 5
                          ),
                          Row(
                            children: <Widget>[
                              new Radio(
                                value: row.id,
                                groupValue: selectSalesRepId,
                                onChanged: _handleSalesRepChange,
                              ),
                              Text(
                                row.name,
                                style: globals.h4Primary,
                              ),
                            ],
                          ),
                          
                          
                        ]
                      )
                      
                    );
                  }).toList()
                )
              ) 
              : Padding(
                child: Text('Select a Provice.', style: globals.h4Primary ,) ,
                padding: const EdgeInsets.all(10),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: TextFormField( 
                      controller: agenda,
                      style: globals.inputText,
                      decoration: InputDecoration(
                        
                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        //icon: Icon(Icons.lock,color: Colors.black38,),
                        hintText: 'Agenda',
                        hintStyle: globals.inputTextLight,
                        labelText: 'Agenda',
                        labelStyle: globals.inputTextLight,
                      ),
                      validator: ( value ){
                        if (value.isEmpty) {
                          return 'Field is required.';
                        }
                      }
                    ),
                  ),
                  Expanded(
                    child: TextFormField( 
                      controller: description,
                      style: globals.inputText,
                      decoration: InputDecoration(
                          
                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        //icon: Icon(Icons.lock,color: Colors.black38,),
                        hintText: 'Description',
                        hintStyle: globals.inputTextLight,
                        labelText: 'Description',
                        labelStyle: globals.inputTextLight,
                      ),
                      validator: ( value ){
                        if (value.isEmpty) {
                          return 'Field is required.';
                        }
                      }
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: TextFormField( 
                      controller: email,
                      style: globals.inputText,
                      decoration: InputDecoration(
                          
                        contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        focusedBorder:  OutlineInputBorder(
                          borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                          borderRadius: new BorderRadius.circular(22.0),
                        ),
                        //icon: Icon(Icons.lock,color: Colors.black38,),
                        hintText: 'Your Email',
                        hintStyle: globals.inputTextLight,
                        labelText: 'Your Email',
                        labelStyle: globals.inputTextLight,
                      ),
                      validator: ( value ){
                        if (value.isEmpty) {
                          return 'Field is required.';
                        }
                      }
                    ),
                  ),
                  Expanded(
                    child: DateTimeField(
                      controller: _date,
                      format: format,
                      onShowPicker: (context, currentValue) async {
                        final date = await showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100));
                        if (date != null) {
                          final time = await showTimePicker(
                            context: context,
                            initialTime:
                                TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                          );
                          return DateTimeField.combine(date, time);
                        } else {
                          return currentValue;
                        }
                      },
                      decoration: InputDecoration(
                          
                          contentPadding: const EdgeInsets.symmetric( vertical: 10, horizontal: 20),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.green,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          focusedBorder:  OutlineInputBorder(
                            borderSide: BorderSide(color: globals.primaryColor,style: BorderStyle.solid),
                            borderRadius: new BorderRadius.circular(22.0),
                          ),
                          //icon: Icon(Icons.lock,color: Colors.black38,),
                          hintText: 'Meeting Date and Time',
                          hintStyle: globals.inputTextLight,
                          labelText: 'Meeting Date and Time',
                          labelStyle: globals.inputTextLight,
                      ),
                      onChanged: (val){
                        //mainBloc.setSalesDate( this, val );
                      }
                    ),
                  ),
                  
                ],
              )
              
            ],
          )
        
        )
        
      )
    );
  }
}
