// import 'dart:io';

// import 'package:path/path.dart';
// import 'package:sqflite/sqflite.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:primcoca/GlobalVariables.dart' as globals;

// class DatabaseHelper {
  
//   static final _databaseName = "locallalaji.db";
//   static final _databaseVersion = 1;
  
//   static final userId = '_id';
//   static final fullName = 'full_name';
//   static final userEmail = 'email';

//   // make this a singleton class
//   DatabaseHelper._privateConstructor();
//   static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

//   // only have a single app-wide reference to the database
//   static Database _database;
//   Future<Database> get database async {
//     // if (_database != null) return _database;
//     // lazily instantiate the db the first time it is accessed
//     _database = await _initDatabase();
    
//     return _database;
//   }
  
//   // this opens the database (and creates it if it doesn't exist)
//   _initDatabase() async {
//     Directory documentsDirectory = await getApplicationDocumentsDirectory();
//     String path = join(documentsDirectory.path, _databaseName);
//     print('InitDB');
//     return await openDatabase(path,
//         version: _databaseVersion,
//         onCreate: _onCreate);
//   }

//   // SQL code to create the database table
//   Future _onCreate(Database db, int version) async {
//     print('_on__Create');
//     String usersTable = globals.usersTable;
//     String wishlistTable = globals.wishlistTable;
//     String cartTable = globals.cartTable;
//     String optionsTable = globals.optionsTable;
//     String orderTable = globals.orderTable;
//     String orderItemsTable = globals.orderItemsTable;
//     print('_onCreate');

//     await db.execute('''
//           CREATE TABLE IF NOT EXISTS $usersTable (
//             _id INTEGER PRIMARY KEY,
//             full_name TEXT NOT NULL,
//             first_name TEXT NOT NULL,
//             last_name TEXT NOT NULL,
//             email TEXT NOT NULL
//           )
//           ''');
//     await db.execute('''  
//           CREATE TABLE IF NOT EXISTS $wishlistTable (
//             _id INTEGER PRIMARY KEY,
//             product_id TEXT NOT NULL,
//             merchant_id TEXT NOT NULL,
//             title TEXT NOT NULL,
//             price TEXT NOT NULL,
//             sale_price TEXT NOT NULL,
//             image TEXT NOT NULL
//           )
//           ''');

//     await db.execute('''      
//           CREATE TABLE IF NOT EXISTS $optionsTable (
//             _id INTEGER PRIMARY KEY,
//             key TEXT NOT NULL,
//             value TEXT NOT NULL
//           )
//           ''');

//     await db.execute('''
//           CREATE TABLE IF NOT EXISTS $cartTable (
//             _id INTEGER PRIMARY KEY,
//             product_id TEXT NOT NULL,
//             merchant_id TEXT NOT NULL,
//             main_product_id TEXT NOT NULL,
//             title TEXT NOT NULL,
//             price TEXT NOT NULL,
//             sale_price TEXT NOT NULL,
//             image TEXT NOT NULL,
//             selectedVariation TEXT NOT NULL,
//             quantity TEXT NOT NULL,
//             amount TEXT NOT NULL
//           )
//           ''');

//     await db.execute('''
//           CREATE TABLE IF NOT EXISTS $wishlistTable (
//             _id INTEGER PRIMARY KEY,
//             product_id TEXT NOT NULL,
//             merchant_id TEXT NOT NULL,
//             image TEXT NOT NULL,
//             title TEXT NOT NULL,
//             price TEXT NOT NULL,
//             sale_price TEXT NOT NULL
//           )
//           ''');
    
//     await db.execute('''
//           CREATE TABLE IF NOT EXISTS $orderTable (
//             _id INTEGER PRIMARY KEY,
//             order_id TEXT NOT NULL,
//             merchant_id TEXT NOT NULL,
//             address_id TEXT NOT NULL,
//             couponCode TEXT NOT NULL,
//             couponDiscount TEXT NOT NULL,
//             igst TEXT NOT NULL,
//             cgst TEXT NOT NULL,
//             sgst TEXT NOT NULL,
//             shippingCharges TEXT NOT NULL,
//             deliveyTime TEXT NOT NULL,
//             subTotal TEXT NOT NULL,
//             total TEXT NOT NULL,
//             paymentMethod TEXT NOT NULL,
//             dateCreated TEXT NOT NULL,
//             status TEXT NOT NULL
//           )
//           ''');
//       await db.execute('''
//           CREATE TABLE IF NOT EXISTS $orderItemsTable (
//             _id INTEGER PRIMARY KEY,
//             _orderTable_id INTEGER,
//             product_id TEXT NOT NULL,
//             main_product_id TEXT NOT NULL,
//             title TEXT NOT NULL,
//             price TEXT NOT NULL,
//             sale_price TEXT NOT NULL,
//             image TEXT NOT NULL,
//             selectedVariation TEXT NOT NULL,
//             quantity TEXT NOT NULL,
//             amount TEXT NOT NULL
//           )
//           ''');
    

        
//   }
  
//   // Helper methods

//   // Inserts a row in the database where each key in the Map is a column name
//   // and the value is the column value. The return value is the id of the
//   // inserted row.
//   Future<List<Map<String, dynamic>>> getMetaValue(String table, String metaKey ) async {
//     Database db = await instance.database;
//     // return db.rawQuery('select tables');
//     return db.query(globals.optionsTable, where: 'key = "$metaKey"', columns: ['value'] );
//     //return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table WHERE `$columnName`="$value"'));
//   }

//   Future<int> addMetaValue(String table, String metaKey, String metaValue ) async {
//     Database db = await instance.database;
//     Map<String, dynamic> row ={ 'key': metaKey, 'value': metaValue };
//     return await db.insert(table, row);
//   }

//   Future<int> updateMetaValue( String table, String metaKey, String metaValue) async {
//     Database db = await instance.database;

//     //  = getMetaValue(globals.optionsTable, metaKey );
//     final checkFlag = await instance.getMetaValue(globals.optionsTable, metaKey);
//     if( checkFlag.isEmpty ){
//       return await instance.addMetaValue(globals.optionsTable, metaKey, metaValue);
//     } else {
//       return await db.update(table, { 'value': metaValue }, where: 'key = ?', whereArgs: [metaKey]);  
//     }
    
//   }

//   // All of the rows are returned as a list of maps, where each map is 
//   // a key-value list of columns.
//   Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
//     Database db = await instance.database;
//     return await db.query(table);
//   }

//   // All of the methods (insert, query, update, delete) can also be done using
//   // raw SQL commands. This method uses a raw query to give the row count.
//   Future<int> queryRowCount(String table ) async {
//     Database db = await instance.database;
//     return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table'));
//   }

//   Future<List<Map<String, dynamic>>> rawQuery(String query ) async {
//     Database db = await instance.database;
    
//     return await db.rawQuery( query );
//   }  

//   Future<int> queryRowCountWhere(String table, String columnName, String value ) async {
//     Database db = await instance.database;
//     return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table WHERE $columnName = "$value" ' ));
//     //return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table WHERE `$columnName`="$value"'));
//   }

//   Future<List<Map<String, dynamic>>> queryRowWhere(String table, String columnName, String value ) async {
//     Database db = await instance.database;
//     return db.query(table, where: '$columnName = ?', whereArgs: [value]);
//     //return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table WHERE `$columnName`="$value"'));
//   }

//   Future<List<Map<String, dynamic>>> queryLastRow(String table, String columnName, String value, List<String> columns ) async {
//     Database db = await instance.database;
//     if( columns.length == 0 ) columns = ['*'];
//     return db.query(table, where: '$columnName=$value', offset: 0, limit: 1, orderBy: '_id', columns: columns );
//     //return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table WHERE `$columnName`="$value"'));
//   }
//   Future<List<Map<String, dynamic>>> queryLast(String table ) async {
//     Database db = await instance.database;
//     //if( columns.length == 0 ) columns = ['*'];
//     return db.query(table,offset: 0, limit: 1, orderBy: '_id DESC' );
//     //return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table WHERE `$columnName`="$value"'));
//   }

//   Future<int> insert( String table, Map<String, dynamic> row) async {
//     Database db = await instance.database;
//     return await db.insert(table, row);
//   }

//   // We are assuming here that the id column in the map is set. The other 
//   // column values will be used to update the row.
//   Future<int> update( String table, Map<String, dynamic> row, String columnName, String value) async {
//     Database db = await instance.database;
//     final checkFlag = await instance.queryRowCountWhere(table, columnName, value);
//     print('checkflag'+checkFlag.toString());
//     print(columnName+"-"+value +"->"+table);
//     if( checkFlag == 0 ) {
//       return await instance.insert(table , row);
//     }
//     return await db.update(table, row, where: '$columnName = ?', whereArgs: [value]);
//   }

//   // Deletes the row specified by the id. The number of affected rows is 
//   // returned. This should be 1 as long as the row exists.
//   Future<int> delete( String table, String columnName, var value ) async {
//     Database db = await instance.database;
//     return await db.delete(table, where: '$columnName = $value' );
//   }
// }